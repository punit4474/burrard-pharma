<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'google' => [
        'client_id' => '382065269237-amuoer7nhrrgqjrc921os78d9a50em7g.apps.googleusercontent.com',
        'client_secret' => '_Z6-JBntarjlm32n8tvcSCnC',
        'redirect' => 'https://burrardpharma.com/auth/google/callback',
    ],

    'facebook' => [
        'client_id' => '579405276086098',
        'client_secret' => '05d3871576098b4935d178a29c084375',
        'redirect' => 'https://burrardpharma.com/auth/facebook/callback',
    ],
    'linkedin' => [
        'client_id' => '78bwt5jzt136gz',         // Your LinkedIn Client ID
        'client_secret' => 'sGrbOurdWBGI5rMq', // Your LinkedIn Client Secret
        'redirect' => 'https://burrardpharma.com/auth/linkedin/callback',       // Your LinkedIn Callback URL
    ],
    'stripe' => [
        'secret' => 'sk_test_51HBe6ZJdvXf76erCTpKneLMJECQ86aHYcAo3xYm8JzR2GNsLP9mbivVwrZL1WsePkD3g46DA6QVRTnHO4LuQngUa00SyzJaBms',
    ],


];
