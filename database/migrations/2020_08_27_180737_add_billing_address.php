<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBillingAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            Schema::table('orders', function (Blueprint $table) {
                $table->text('city')->nullable();
                $table->text('country')->nullable();
                $table->text('postalcode')->nullable();
                $table->text('phone_number')->nullable();
            });
            Schema::table('users', function (Blueprint $table) {
                $table->text('city')->nullable();
                $table->text('country')->nullable();
                $table->text('postalcode')->nullable();
//                $table->text('phone_number')->nullable();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn(['city','country','postalcode','phone_number']);
            });
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn(['city','country','postalcode','phone_number']);
            });
        });
    }
}
