<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::group(['namespace' => 'Front'], function () {
    Route::get('login',function (){

        if(Auth::check() && Auth::user()->hasRole('user')){
            return redirect('user-profile');
        } else {
            return view('Front.Auth.login');
        }

    })->name('user_login');


    // Route::get('/login','AuthController@login');
    Route::post('/login','AuthController@dologin');
    Route::post('/register','AuthController@register');
    Route::get('/logout','AuthController@logout');
    Route::post('/forgot-password','AuthController@forgotPassword');
    Route::post('/change-password','AuthController@changePassword');
    Route::get('auth/google','AuthController@redirectToGoogle');
    Route::get('auth/google/callback','AuthController@handleGoogleCallback');

    Route::get('auth/linkedin','AuthController@redirectToFacebook');
    Route::get('auth/linkedin/callback','AuthController@handleFacebookCallback');


    Route::get('/','FrontController@index');
    Route::get('/service','FrontController@service');
    Route::get('/company','FrontController@company');
    Route::get('/user-profile','FrontController@userProfile')->middleware(['auth','verified']);
    Route::post('/update-profile','FrontController@updateProfile')->middleware(['auth','verified']);
    Route::get('/pricing','FrontController@pricing');
    Route::get('/generator','FrontController@generator');
    Route::get('/generator-ajax','FrontController@generatorAjax');
    Route::POST('/subscription','FrontController@subscription');
    Route::post('/contact-us','FrontController@storeContact');
    Route::post('/news-letter','FrontController@notification');

    Route::post('/checkout','FrontController@generatorStore')->middleware(['auth','verified']);
    Route::post('/payment','FrontController@paymentStore')->name('stripe.post')->middleware(['auth','verified']);

    Route::get('/cancel', 'FrontController@cancel')->name('payment.cancel');
    Route::get('/payment/success', 'FrontController@success')->name('payment.success');

    Route::get('payment-sucess',function(){
        return view('Front.success');
    });

    Route::get('payment-failed',function(){
        return view('Front.failed');
    });

    Route::get('privacy-policy',function(){
        return view('Front.privacy');
    });
    Route::get('terms-and-conditions',function(){
        return view('Front.terms');
    });

    Route::get('email/verify',function(){
        return view('auth.verify');
    })->name('verification.notice');
    /**
     * Blog Section
     */
    Route::get('press','FrontController@blog');
    Route::get('press/{slug}','FrontController@blogDetails');
    Route::get('medium-blog','FrontController@mediumBlog');
});


Route::get('/home', 'HomeController@index')->name('home');

//Route::get('/verify/{token}', 'VerifyController@VerifyEmail')->name('verify');

Route::group(['prefix'=>'burrard-admin','namespace'=>'Admin'],function(){

    Route::get('login',function (){

        if(Auth::check() && Auth::user()->hasRole('admin')){
            return redirect('burrard-admin/');
        } else {
            return view('Admin.Auth.login');
        }

    });
//    Route::get('login','AuthController@login');
    Route::post('login','AuthController@doLogin');

    Route::group(['middleware' => 'AdminAuth'], function () {
        Route::get('/','DashboardController@index');
        Route::get('logout','AuthController@logout');

        Route::resource('user', 'UserController');
        Route::get('user/{id}/destroy', 'UserController@destroy');
        Route::get('user/{id}/status', 'UserController@statusChange');

        Route::resource('report', 'ReportController');
        Route::get('report/{id}/destroy', 'ReportController@destroy');
        Route::get('report/{id}/status', 'ReportController@statusChange');
        Route::get('reports/bulk', 'ReportController@bulk');
        Route::post('reports/bulk', 'ReportController@storeBulk');

        Route::resource('payment', 'PaymentController');
        Route::get('payment/{id}/destroy', 'PaymentController@destroy');

        Route::resource('order', 'OrderController');
        Route::get('order/{id}/destroy', 'OrderController@destroy');

        Route::resource('contact-us', 'ContactController');
        Route::get('contact-us/{id}/destroy', 'ContactController@destroy');

        Route::resource('subscription', 'SubscriptionController');

        Route::resource('blog', 'BlogController');
        Route::get('blog/{id}/destroy', 'BlogController@destroy');
        Route::get('blog/{id}/status', 'BlogController@statusChange');

        Route::resource('news-letter','NewsletterController');
        Route::get('news-letters/export','NewsletterController@export');
    });
});
