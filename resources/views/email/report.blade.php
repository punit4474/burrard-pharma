<!DOCTYPE html>
<html>
<head>
    <title>Report Generate</title>
    <style type="text/css">
       .checkout-drug-card {
            width: 100%;
            height: auto;
            display: flex;
            justify-content: space-between;
            flex-flow: row wrap;
            position: relative;
            cursor: pointer;
            margin-bottom: 2rem;
            background-color: #FDFBF3;
            border: 1px solid #DEDCD5;
            border-radius: 1.875rem;
            transition: 0.3s;
        }
        .drug-name {
            width: 100%;
            margin: 0 2rem;
            border-bottom: 1px solid #DEDCD5;
        }
        .drug-info-left {
            width: 50%;
            margin: 0 0 2.5rem 0;
            border-right: 1px solid #DEDCD5;
        }

        .drug-info-right {
            width: 50%;
            margin: 0 0 2.5rem 0;
        }
    </style>
</head>
<body>
<div>
    <div style="background:#ffffff;padding:15px">
        <center>
            <div style="width:100%;max-width:500px;background:#ffffff;height:80px">
                <br>
                <center>

                    <a href="#">
                        <img
                            alt=""
                            src="https://burrardpharma.com/storage/app/public/Frontassets/email_logo.png"

                            style="display:block;font-family:Helvetica,Arial,sans-serif;color:#666666;font-size:16px;height: 100%;width: 100%;"
                            border="0" class="CToWUd">
                    </a>
                </center>
            </div>
            <br>

            <div style="width:100%;max-width:580px;background:#ffffff;height:auto;padding:10px 0 10px 0">
                <hr style="border:dashed 1px #e1e1e1;max-width:100%">

                <div style="width:100%;max-width:580px;background:#ffffff;height:auto;padding:0px 0 0px 0">
                    <div style="font-family:'Lato',Helvetica,Arial,sans-serif;display:inline-block;margin:0px 0px 0 0;max-width:100%;width:100%;margin-right:0px">

                        <div style="width:100%;max-width:580px;background:#ffffff;height:auto;padding:20px 0 0px 0">
                            <div bgcolor="#f8f4e8" align="left"
                                 style="padding:0px 0% 0px 0%;font-size:22px;line-height:25px;font-family:'Lato',Helvetica,Arial,sans-serif;color:#000000;font-weight:700"
                                 class="m_-7788511936867687679padding-copy">Dear Admin,
                            </div>


                            <div style="width:100%;max-width:580px;background:#ffffff;height:auto;padding:15px 0 0px 0">
                                <div bgcolor="#f8f4e8" align="left"
                                     style="padding:0px 0% 0px 0%;font-size:16px;line-height:25px;font-family:'Lato',Helvetica,Arial,sans-serif;color:#6c6e6e;font-weight:500"
                                     class="m_-7788511936867687679padding-copy">
                                    New Report Request Received, <br>
                                    Name : {{$name}} <br>
                                    Email :<b> {{$email}}</b><br>
                                    @foreach ($report_data as $row)
                                        <label class="checkout-drug-card">
                                            <div class="drug-name">
                                                <h1>{{$row->name}}</h1>
                                            </div>
                                            <div class="drug-info-left">
                                                <div id="dosage">{{$row->dosage_form}}</div>
                                                <div id="strengh">{{$row->strengh}}</div>
                                            </div>
                                            <div class="drug-info-right">
                                                <div id="route">{{$row->route_of_admin}}</div>
                                                <div id="code">{{$row->atc_code}}</div>
                                            </div>
                                        </label>
                                    @endforeach

                                    <br/>
                                    <br/>
                                    Admin<br/>
                                    Burrard Pharma

                                </div>




                                <div style="width:100%;max-width:580px;background:#ffffff;height:auto;padding:0px 0 10px 0">
                                    <hr style="border:dashed 1px #e1e1e1;max-width:100%">
                                </div>

                                <div style="display:inline-block;text-align:center;color:#777777;margin:0px auto 26px auto;font-family:'Lato',Helvetica,Arial,sans-serif"
                                     align="center">

                                    <div style="font-size:12px;color:#777777;margin:12px auto 30px auto;font-family:'Lato',Helvetica,Arial,sans-serif">
                                        <p style="font-size: 11px"><b>Note</b>:- Do not reply to this notification message,this message was auto-generated by the sender's security system.</p>

                                        <p style="line-height:1;font-size:12px;margin:0 20px 30px 20px;padding:0 0 0 0;color:#777777;font-family:'Lato',Helvetica,Arial,sans-serif">
                                            Burrard Pharma | All Rights Reserved.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </center>
    </div>

</div>
</body>
</html>
