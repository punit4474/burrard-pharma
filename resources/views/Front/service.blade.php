@extends('layouts.front')
@section('front_title')
    Service
@endsection
@section('meta')
    <link rel="canonical" href="http://www.burrardpharma.com/"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Title"/>
    <meta property="og:description" content="Description"/>
    <meta property="og:url" content="http://www.burrardpharma.com/"/>
    <meta property="og:site_name" content="Burrard Pharma"/>
    <meta name="description" content="Description"/>
    <meta name="twitter:title" content="Title">
    <meta name="twitter:description" content="Description">
@endsection
@section('front_css')
@endsection
@section('front_content')

<div id="full-wrapper">
    <div class="full-container">
        <img class="services-banner" src="{{URL::to('storage/app/public/Frontassets/media/services-banner.jpeg')}}">
        <div class="full-content-box">
            <div class="content-box-text">
                <h1>Solution to preclinical and commercial drug products.</h1>
            </div>
        </div>
        <a class="dark-bg-btn" href="#services-wrapper">
            <div class="btn-icon">
                <img src="{{URL::to('storage/app/public/Frontassets/media/down-white.svg')}}">
            </div>
        </a>
    </div>
</div>

<div id="services-wrapper">
    <div class="services-container">
        <div class="services-title">
            <h1>Burrard Pharma provides manufacturing, drug development and drug formulation from clinical research to commercial marketing</h1>
        </div>
        <div id="drug-formulation">
            <div class="sticky-content">
                <div class="sticky-card">
                    <div class="sticky-icon">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/formulation.svg')}}">
                    </div>
                    <div class="sticky-title">
                        <h1>Drug Formulation</h1>
                    </div>
                </div>
            </div>
            <div class="sticky-content">
                <video class="normal-media"
                    poster=""
                    src="{{URL::to('storage/app/public/Frontassets/media/formulation.mp4')}}"
                    type="video/mp4"
                    autoplay loop playsinline muted
                    data-object-fit="cover">
                </video>
                <div class="sticky-text">
                    <p>Our team of biopharmaceutical professionals work diligently to gain the thorough understanding of different compounds, to help you effectively develop quality drug formulation and development.</p>
                    <p>Our core services include process development, formulation development, preclinical (toxicology) and batch production.</p>
                    <p>1. Master formulation you need to produce a a drug either in clinical size or commercial in one place.</p>
                    <p>2.You will have full access to our knowledge, manufacturing secrets, formulation, master formula, products specifications, studies and more from a single dashboard.</p>
                    <p>So successful in fact, that our customers have made over $500 million dollars on the platform to date!
                        Master formulation you need to produce a a drug either in clinical size or commercial in one place.
                        You will have full access to our knowledge, manufacturing secrets, formulation, master formula, products specifications, studies and more from a single dashboard.</p>
                </div>
                <div class="sticky-text">
                    <h2>Write up and review drug summary</h2>
                    <h3>Introduction</h3>
                </div>
                <div class="sticky-text">
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Basic information on the chemical</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Mechanism of action</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Pharmacology class</p>
                    </div>
                </div>
                <div class="sticky-text">
                    <h3>Non-clinical overview</h3>
                </div>
                <div class="sticky-text">
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Pharmacology</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Pharmacokinetics</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Toxicology</p>
                    </div>
                </div>
                <div class="sticky-text">
                    <h3>Clinical overview</h3>
                </div>
                <div class="sticky-text">
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Overview of biopharmaceutics</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Clinical pharmacology</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Efficacy and safety</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Benefits and risks</p>
                    </div>
                </div>
                <div class="sticky-text">
                    <h2>Determine information on quality</h2>
                    <h3>Drug substance</h3>
                </div>
                <div class="sticky-text">
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Nomenclature</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Structure</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Physicochemical properties</p>
                    </div>
                </div>
                <div class="sticky-text">
                    <h3>Manufacture information</h3>
                </div>
                <div class="sticky-text">
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Batch formula</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Description of manufacture process and controls</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Controls of critical steps and intermediates</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Product testing, validation and evaluation</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Manufacture process development</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Determine impurities</p>
                    </div>
                </div>
                <div class="sticky-text">
                    <h3>Pharmaceutical development</h3>
                </div>
                <div class="sticky-text">
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Components of drug product</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Microbiological attributes</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Compatibility</p>
                    </div>
                </div>
                <div class="sticky-text">
                    <h3>Control of drug substance</h3>
                </div>
                <div class="sticky-text">
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Specification of drug substance</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Analytical procedures</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Batch analyses</p>
                    </div>
                </div>

                <div class="sticky-text">
                    <h3>Reference standards or materials</h3>
                    <h3>Container closure system</h3>
                    <h3>Stability data</h3>
                    <h3>Control of excipients</h3>
                </div>
            </div>
        </div>

        <div id="know-how">
            <div class="sticky-content">
                <div class="sticky-card">
                    <div class="sticky-icon">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/development.svg')}}">
                    </div>
                    <div class="sticky-title">
                        <h1>Know How</h1>
                    </div>
                </div>
            </div>
            <div class="sticky-content">
                <video class="normal-media"
                    poster=""
                    src="{{URL::to('storage/app/public/Frontassets/media/development.mp4')}}"
                    type="video/mp4"
                    autoplay loop playsinline muted
                    data-object-fit="cover">
                </video>
                <div class="sticky-text">
                    <p>Burrard Pharma comprehensive drug development services are designed to reduce your costs and time required, helping you achieve your drug discovery goals. Our creative scientists have accumulated decades of industry-specific experience. This enables our firm to create molecules to the specifications you need efficiently. As well, our drug development experts will optimize your processes and scale up the operations by your requirements for drug manufacturing.</p>

                    <p>Our team of biopharmaceutical professionals work diligently to gain the thorough understanding of different compounds, to help you effectively develop quality drug formulation and development.</p>

                    <p>Our core services include process development, formulation development, preclinical (toxicology) and batch productions.</p>
                </div>
                <div class="sticky-text">
                    <h2>Our drug development team has extensive expertise in the following</h2>
                </div>
                <div class="sticky-text">
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Stability studies</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Lyophilization – Cytotoxic formulation</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Terminal sterilisation</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Potent Formulation</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Highly potent product formulation</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Peptides/Proteins/Enzyme</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Monoclonal antibodies (MAB)</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Nanoparticles</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Inactivated &amp; attenuated live vaccines</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Dry Powder</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Filling of both liquid and dry powder</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Sterile crystallization</p>
                    </div>

                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Formulation of disperse systems</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Filling and lyophilisation of colloidal drug carriers, such as liposomes, nanoparticles, microemulsions and micelles</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Technology portfolios such as homogenization, diafiltration, organic solvent distillation, liposome loading and sterile filtration</p>
                    </div>
                </div>
            </div>
        </div>


        <div id="drug-manufacturing">
            <div class="sticky-content">
                <div class="sticky-card">
                    <div class="sticky-icon">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/manufacturing.svg')}}">
                    </div>
                    <div class="sticky-title">
                        <h1>Drug Manufacturing</h1>
                    </div>
                </div>
            </div>
            <div class="sticky-content">
                <video class="normal-media"
                    poster=""
                    src="{{URL::to('storage/app/public/Frontassets/media/manufacturing.mp4')}}"
                    type="video/mp4"
                    autoplay loop playsinline muted
                    data-object-fit="cover">
                </video>
                <div class="sticky-text">
                    <p>With over a decade of international experience, Burrard Pharmaceuticals is well positioned to navigate through any complex drug manufacturing projects.</p>

                    <p>We are experts in producing traditionally complex products such as cytotoxic’s, highly potent compounds, biologics, lyophilized products and Antibody-Drug Conjugates. Often, parenteral manufacturing is a very complicated process, and we are here to help you simplify it and ensure the excellence in development.</p>
                </div>
                <div class="sticky-text">
                    <h2>Burrard Pharmaceutical’s manufacturing services include</h2>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Vial filling- Aseptic</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Solid-Semi Solid- Liquid</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Packaging – Emulsion – Sterile</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Stability storage</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Lyophilization – Cytotoxic</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Terminal sterilisation</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Potent Formulation</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>cGMP production of complex compounds</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Highly potent products</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Cytotoxics</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Peptides/Proteins/Enzyme</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Monoclonal antibodies (MAB)</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Nano particles</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Inactivated & attenuated live vaccines</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Dry Powder</p>
                    </div>
                </div>





                <div class="sticky-text">
                    <h2>Cyotoxic Molecules</h2>
                </div>
                <div class="sticky-text">
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Filling of both liquid and dry powder</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Clinical and commercial lyophilized products</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Sterile crystallization</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Formulation of disperse systems as well as filling and lyophilisation of colloidal drug carriers, such as liposomes, nanoparticles, microemulsions and micelles</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Technology portfolio such as homogenization, diafiltration, organic solvent distillation, liposome loading and sterile filtration</p>
                    </div>
                </div>




                <div class="sticky-text">
                    <h2>Support services</h2>
                </div>
                <div class="sticky-text">
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Technology transfer</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Stability storage</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Lyophilization cycle development</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Analytics</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Engineering</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Microbiological services</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Packaging and kitting</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Regulatory submission and support</p>
                    </div>
                </div>




                <div class="sticky-text">
                    <h2>Burrard Pharmaceuticals has proven experience especially with the following molecule classes</h2>
                </div>
                <div class="sticky-text">
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Small molecules</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Biologics</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Liposomal systems</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Antibody-drug conjugates</p>
                    </div>
                </div>







                <div class="sticky-text">
                    <h2>Biologics</h2>
                    <h3>By utilizing innovative filling line tech, our service is designed to protect the integrity of biologics and to optimize the yield of costly production of active pharmaceutical ingredients.</h3>
                </div>
                <div class="sticky-text">
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Non-destructive testing and weight checking</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Dual filling</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Cold chain filling</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Process design with high yield optimization</p>
                    </div>
                </div>
                <div class="sticky-text">
                    <h3>With our wealth of experience with biologic products, Burrard Pharmaceuticals is ready to provide our skills.</h3>
                </div>
                <div class="sticky-text">
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Biomolecule formulation</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Process design and development</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Solutions, suspensions as well as development of lyophilized forms</p>
                    </div>
                </div>










                <div class="sticky-text">
                    <h2>Lyophilized Vials</h2>
                    <h3>Burrard has extensive experience with freeze drying. Our solution offers individualized filling suites with lyophilization capacities.</h3>
                </div>
                <div class="sticky-text">
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Sizes: 2mL to 10mL</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>All primary packaging options such as stoppers, vials, and seals</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Multiple filling</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Leading technology in lyophilization with automated loading/unloading system</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Meet FDA, EU and JP regulatory standards</p>
                    </div>
                    <h2>We can handle the following molecules</h2>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Cytotoxic’s</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Highly potent molecules</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Small molecules</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Biologics</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Antibody-drug conjugates</p>
                    </div>
                </div>












                <div class="sticky-text">
                    <h2>Antibiotics</h2>
                    <h3>Burrard Pharmaceuticals offers quality manufacturing of antibiotics.</h3>
                    <h2>Vaccines</h2>
                    <h3>Burrard Pharmaceuticals sees the growing trend of vaccines forming an important portion of your business model.</h3>
                </div>
                <div class="sticky-text">
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Pandemic vaccines</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Conjugates</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Adjuvants</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Preventive vaccines</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Seasonal vaccines</p>
                    </div>
                </div>








            </div>
        </div>



        <div id="technology-transfer">
            <div class="sticky-content">
                <div class="sticky-card">
                    <div class="sticky-icon">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/transfer.svg')}}">
                    </div>
                    <div class="sticky-title">
                        <h1>Technology Transter</h1>
                    </div>
                </div>
            </div>
            <div class="sticky-content">
                <video class="normal-media"
                    poster=""
                    src="{{URL::to('storage/app/public/Frontassets/media/transfer.mp4')}}"
                    type="video/mp4"
                    autoplay loop playsinline muted
                    data-object-fit="cover">
                </video>
                <div class="sticky-text">
                    <p>Transfer scientific findings from one organization to another often take the research further to develop something new or better. Successful product innovations have often begun through the usage of technology transfer.</p>

                </div>
                <div class="sticky-text">
                    <h2>Some of the technology transfer we have worked with include the below:</h2>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Large Volume Parenterals (LVP)</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Small Volume Parenterals (SVP)</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Pre-filled Syringes</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>IV products</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Blood bags</p>
                    </div>

                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Dialysis concentrate solutions and powders</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Plasma Fractionation</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Biotechnological Plants</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Multipurpose Pharmaceutical Plants</p>
                    </div>
                </div>

                <div class="sticky-text">
                    <h2>Pharmaceutical Technologies</h2>
                    <h3>We are capable of producing intravenous (IV) solutions in both small (SVP) and large (LVP) volumes in standard or modular configurations.</h3>

                </div>
                <div class="sticky-text">
                    <h2>Available IV products include:</h2>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>PVC and PP bags</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Plastic and glass bottles</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Bottles with Blow Fill Seal technology (BFS)</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Bottle with Stretch Blow Molding Technology (SBM)</p>
                    </div>

                </div>

                <div class="sticky-text">
                    <h2>Solutions include:</h2>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Water</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Sodium Chloride</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Glucose</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Dextrose</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Mannitol</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Amino acids</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Plasma expanders</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Heparin</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Ciprofloxacin</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Metronidazole</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Paracetamol</p>
                    </div>
                </div>
                <div class="sticky-text">
                    <h2>Dialysis</h2>
                    <h3>Facilities for producing hemodialysis and peritoneal dialysis products.</h3>

                </div>
                <div class="sticky-text">
                    <h2>Available formats include:</h2>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Canisters</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Bags</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Cartridges</p>
                    </div>
                </div>
                <div class="sticky-text">
                    <h2>Available products include:</h2>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Diacetate bags</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Priming bags</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Multi-chamber bags</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Continuous Ambulatory Peritoneal Dialysis bags</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Bicarbonate cartridges</p>
                    </div>
                </div>
                <div class="sticky-text">
                    <h2>Blood Bags</h2>
                    <h3>We can design facilities that produce blood bags with anticoagulant solutions. These plants can be for full auto assembly of bags or through the semi-auto assembly..</h3>

                </div>
                <div class="sticky-text">
                    <h2>Available products:</h2>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Single Bags</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Double Bags
                        </p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Triple Bags</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Tetra Bags</p>
                    </div>
                </div>

                <div class="sticky-text">
                    <h2>Plasma Fractionation Plant</h2>
                    <h3>We can help you with facilities for Hemoderivatives production from plasma. With our technical know-hows, we can extract highly effectively which fractionate the plasma high-quality plants, ensuring proper virus inactivation processes.</h3>

                </div>


                <div class="sticky-text">
                    <h2>Biotech & Multipurpose Plants</h2>
                    <h3>We can help you with facilities that produce biotech products with either standard, disposable of mixed technologies. We ensure the top quality assurance standards by utilizing advanced fermentation techniques.</h3>

                </div>
                <div class="sticky-text">
                    <h2>Available products:</h2>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Interferon</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Erythropoietin</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Epidermal Growth Factor</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Insulin</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Human Growth Hormone</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Monoclonal Antibodies</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Antibiotics</p>
                    </div>
                </div>
                <div class="sticky-text">
                    <h2>Packaging Capabilities</h2>
                    <h3>We view packaging as integral to our formula to help you become successful. Our pharmaceutical packaging services will help you identify the best drug packaging for your complex solid dose, semi-solid creams and suppositories and liquid products.</h3>

                </div>
                <div class="sticky-text">

                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Solution & Suspension Small Volume Parenteral</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Lyophilized Small Volume Parenteral</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Non-Sterile Liquids or Suspensions</p>
                    </div>
                    <div class="sticky-bullet">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/check.svg')}}">
                        <p>Terminal Sterilization</p>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>

@endsection
@section('front_js')
{{--    <script src="{{URL::to('storage/app/public/Frontassets/js/index.js')}}"></script>--}}

    <script src="{{URL::to('storage/app/public/Frontassets/js/objectFitPolyfill.min.js')}}"></script>
    <script>
        // Pageload Delay

        // Back To Top

        $(document).ready(function() {
            $('a[href^="#"]').click(function(event) {
                var id = $(this).attr("href");
                var offset = 0;
                var target = $(id).offset().top - offset - $('.nav-pc').height();
                $('html, body').animate({scrollTop:target}, 1000);
                event.preventDefault();
            });
        });

        // Pageload Delay

        // function delay (URL) {
        //     setTimeout( function() { window.location = URL }, 1700 );
        // }
        // $(window).bind("pageshow", function(event) {
        //     if (event.originalEvent.persisted) {
        //         window.location.reload()
        //     }
        // });
        //
        // $(window).scroll(function() {
        //     if ($(this).scrollTop() > 1) {
        //         $("#nav-hidden").stop().fadeIn(250)
        //     } else {
        //         $("#nav-hidden").stop().fadeOut(250)
        //     }
        // });



        function watchForHover() {
            var hasHoverClass = false;
            var container = document.body;
            var lastTouchTime = 0;
            function enableHover() {
                if (new Date() - lastTouchTime < 500) return;
                if (hasHoverClass) return;
                container.className += ' hasHover';
                hasHoverClass = true;
            }
            function disableHover() {
                if (!hasHoverClass) return;
                container.className = container.className.replace(' hasHover', '');
                hasHoverClass = false;
            }
            function updateLastTouchTime() {
                lastTouchTime = new Date();
            }
            document.addEventListener('touchstart', updateLastTouchTime, true);
            document.addEventListener('touchstart', disableHover, true);
            document.addEventListener('mousemove', enableHover, true);
            enableHover();
        }
        watchForHover();
    </script>

@endsection
@section('footer_sc')
    <div class="footer-icons">
        <a href="#services-wrapper"><img src="{{URL::to('storage/app/public/Frontassets/media/top.svg')}}"></a>
    </div>
@endsection
