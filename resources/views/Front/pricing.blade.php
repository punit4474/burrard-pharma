@extends('layouts.front')
@section('front_title')
    Pricing
@endsection
@section('meta')
    <link rel="canonical" href="http://www.burrardpharma.com/"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Title"/>
    <meta property="og:description" content="Description"/>
    <meta property="og:url" content="http://www.burrardpharma.com/"/>
    <meta property="og:site_name" content="Burrard Pharma"/>
    <meta name="description" content="Description"/>
    <meta name="twitter:title" content="Title">
    <meta name="twitter:description" content="Description">
@endsection
@section('front_css')
    <style>
        #card-element {
            margin-bottom: 20px;
            margin-top: 15px;
        }

        #card-errors {
            margin-bottom: 10px;
            margin-top: 15px;
        }
    </style>
@endsection
@section('front_content')

    <div id="tab-wrapper">
        <div class="tab-container">
            <div class="tab-half">
                <div class="tab-pricing">
                    <div class="tab-title">
                        <h1>Get Full access to Know-How and Technology of manufacturing of +1000 molecules like
                            Administrative Information, Technical Document Summaries, Quality, Clinical and Non Clinical
                            Reports for your next drug development project</h1>
                    </div>
                    <div class="pricing-box">
                        <div class="pricing-container">
                            <div class="pricing-icon">
                                <img src="{{URL::to('storage/app/public/Frontassets/media/dollar.svg')}}">
                            </div>
                            <div class="pricing-text">
                                <h1>Each Know How</h1>
                                <p>$5,000</p>
                            </div>
                        </div>
                        <a class="pricing-btn" href="{{URL::to('generator')}}">Generator</a>
                    </div>
                    <div class="pricing-box">
                        <div class="pricing-container">
                            <div class="pricing-icon">
                                <img src="{{URL::to('storage/app/public/Frontassets/media/big-plus.svg')}}">
                            </div>
                            <div class="pricing-text">
                                <h1>Technical Consultation</h1>
                                <p>$250 Annually</p>
                            </div>
                        </div>
                        <a class="subscribe-btn" href="#">Subscribe</a>
                    </div>
                    <div id="box-no-border" class="pricing-box">
                        <div class="pricing-container">
                            <div id="pricing-text-full" class="pricing-text">
                                <p>BP+ is a yearly paid subscription program from Burrard Pharma that gives users access to additional services otherwise unavailable or available at extra charge to regular clients. Services include consolation one by one with our formulation expert up to one hour with no charge.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-half">
                <div class="tab-content">
                    <div class="tabs">
                        <div class="tab-header">
                            <h1>What's inside a document?</h1>
                            <h2>Organized, Science prepared formulation with our detailed instructions so you never miss
                                a step.</h2>
                        </div>
                        <div class="tab-links">
                            <button class="tablink" onclick="tab(event, 'a')" id="defaultOpen"><img
                                    src="{{URL::to('storage/app/public/Frontassets/media/a.svg')}}"></button>
                            <button class="tablink" onclick="tab(event, 'b')"><img
                                    src="{{URL::to('storage/app/public/Frontassets/media/b.svg')}}"></button>
                            <button class="tablink" onclick="tab(event, 'c')"><img
                                    src="{{URL::to('storage/app/public/Frontassets/media/c.svg')}}"></button>
                            <button class="tablink" onclick="tab(event, 'd')"><img
                                    src="{{URL::to('storage/app/public/Frontassets/media/d.svg')}}"></button>
                        </div>
                        <div id="a" class="tabbody">
                            <div class="tab-list">
                                <h1>Administrative Information and Prescribing Information</h1>
                                <div class="list-expand">
                                    <h2>Non Clinical Study Reports</h2>
                                    <img class="list-plus"
                                         src="{{URL::to('storage/app/public/Frontassets/media/plus.svg')}}">
                                </div>
                                <div class="list-info">
                                    <p>Study Reports</p>
                                    <p>Pharmacology</p>
                                    <p>Primary Pharmacodynamics</p>
                                    <p>Secondary Pharmcodynamics</p>
                                    <p>Safety Pharmacology</p>
                                    <p>Pharmacodynamic Drug Interactions</p>
                                    <p>Pharmacokinetics</p>
                                    <p>Method of Analysis</p>
                                    <p>Absorption</p>
                                    <p>Distribution</p>
                                    <p>Metabolism</p>
                                    <p>Excretion</p>
                                    <p>Pharmacokinetic Drug Interactions</p>
                                    <p>(nonclinical)</p>
                                    <p>Other Pharmacokinetic Studies</p>
                                    <p>Toxicology</p>
                                    <p>Single-Dose Toxicity</p>
                                    <p>Repeat-Dose Toxicity</p>
                                    <p>Genotoxicity</p>
                                    <p>Carcinogenicity</p>
                                    <p>Reproductive and Development Toxicity</p>
                                    <p>Local Tolerance</p>
                                    <p>Other Toxicity Studies</p>
                                </div>
                            </div>
                            <div class="tab-list">
                                <div class="list-expand">
                                    <h2>Clinical Study Reports</h2>
                                    <img class="list-plus"
                                         src="{{URL::to('storage/app/public/Frontassets/media/plus.svg')}}">
                                </div>
                                <div class="list-info">
                                    <p>Clinical Study Reports</p>
                                    <p>Biopharmaceutics Reports</p>
                                    <p>Bioavailability Study Reports</p>
                                    <p>Bioequivalence Study Reports</p>
                                    <p>In-vivo/In-vitro Correlation Studies</p>
                                    <p>Analytical Method Used in Bioavailability</p>
                                    <p>Study</p>
                                    <p>Studies Pertinent to Pharmacokinetics using Human</p>
                                    <p>Biomaterials</p>
                                    <p>Human Pharmacokinetics</p>
                                    <p>Human Pharmacodynamics</p>
                                    <p>Clinical Efficacy and Safety Studies</p>
                                </div>
                            </div>
                        </div>
                        <div id="b" class="tabbody">
                            <div class="tab-list">
                                <h1>Know how</h1>
                                <h3>Batch Formula</h3>
                                <div class="list-expand">
                                    <h2>Description of manufacturing process and process control</h2>
                                    <img class="list-plus"
                                         src="{{URL::to('storage/app/public/Frontassets/media/plus.svg')}}">
                                </div>
                                <div class="list-info">
                                    <p>Flow Diagram representing the steps of process</p>
                                    <p>Manufacturing Flow Chart</p>
                                    <p>Equipment Used in manufacturing process</p>
                                    <p>Manufacturing Process</p>
                                </div>
                            </div>
                            <div class="tab-list">
                                <h3>In Process Quality Control</h3>
                                <div class="list-expand">
                                    <h2>Manufacturing process controls of critical steps and intermediates</h2>
                                    <img class="list-plus"
                                         src="{{URL::to('storage/app/public/Frontassets/media/plus.svg')}}">
                                </div>
                                <div class="list-info">
                                    <p>In-Process Control Parameters during processing,</p>
                                    <p>In-Process Control Parameters during Filling:</p>
                                    <p>In-Process Control during Inspection:</p>
                                    <p>In-Process Control during Packing:</p>
                                </div>
                                <h3>Process validation and Evaluation</h3>
                            </div>
                        </div>
                        <div id="c" class="tabbody">
                            <div class="tab-list">
                                <h1>Quality Control</h1>
                                <h3>Process Validation and/or Evaluation</h3>
                                <h3>Validation of Analytical Procedures</h3>
                                <h3>Pharmaceutical Development</h3>
                                <h3>Post-approval Stability Protocol and Stability Commitment</h3>
                            </div>
                        </div>
                        <div id="d" class="tabbody">
                            <div class="tab-list">
                                <h1>Technology Transfer</h1>
                            </div>
                            <div class="tab-list">
                                <div class="list-expand">
                                    <h2>Drug Substance</h2>
                                    <img class="list-plus"
                                         src="{{URL::to('storage/app/public/Frontassets/media/plus.svg')}}">
                                </div>
                                <div class="list-info">
                                    <p>General Information</p>
                                    <p>Nomenclature</p>
                                </div>
                            </div>
                            <div class="tab-list">
                                <div class="list-expand">
                                    <h2>Structure</h2>
                                    <img class="list-plus"
                                         src="{{URL::to('storage/app/public/Frontassets/media/plus.svg')}}">
                                </div>
                                <div class="list-info">
                                    <p>General Properties</p>
                                    <p>Characterisation</p>
                                    <p>Elucidation of Structure and other Characteristics</p>
                                    <p>Impurities</p>
                                </div>
                            </div>
                            <div class="tab-list">
                                <div class="list-expand">
                                    <h2>Control of Drug Substance</h2>
                                    <img class="list-plus"
                                         src="{{URL::to('storage/app/public/Frontassets/media/plus.svg')}}">
                                </div>
                                <div class="list-info">
                                    <p>Specification</p>
                                    <p>Analytical Procedures</p>
                                </div>
                            </div>
                            <div class="tab-list">
                                <div class="list-expand">
                                    <h2>Batch Analyses</h2>
                                    <img class="list-plus"
                                         src="{{URL::to('storage/app/public/Frontassets/media/plus.svg')}}">
                                </div>
                                <div class="list-info">
                                    <p>Justification of Specification</p>
                                    <p>Reference Standards or Materials</p>
                                    <p>Container Closure System</p>
                                </div>
                            </div>
                            <div class="tab-list">
                                <div class="list-expand">
                                    <h2>Stability</h2>
                                    <img class="list-plus"
                                         src="{{URL::to('storage/app/public/Frontassets/media/plus.svg')}}">
                                </div>
                                <div class="list-info">
                                    <p>Stability Summary and Conclusions</p>
                                    <p>Stability Data</p>
                                </div>
                            </div>
                            <div class="tab-list">
                                <div class="list-expand">
                                    <h2>Drug Product</h2>
                                    <img class="list-plus"
                                         src="{{URL::to('storage/app/public/Frontassets/media/plus.svg')}}">
                                </div>
                                <div class="list-info">
                                    <p>Description and Composition of the Drug Product</p>
                                    <p>Control of Excipients</p>
                                    <p>Analytical Procedures</p>
                                    <p>Excipients of Human or Animal Origin</p>
                                    <p>Novel Excipients</p>
                                </div>
                                <h3>Control of Drug Product</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @if(Auth::check())
        <div class="pop-up-wrapper">
            <div class="pop-up-container">
                <div class="pop-up-content">
                    <form action="{{URL::to('subscription')}}" method="post" name="subscription" id="subscription">
                        @csrf
                        <div class="subscribe-title"><h1>Fill in the form below to get started.</h1></div>
                        <input class="subscribe-input" type="text" id="name" name="name" placeholder="Full name"
                               value="{{Auth::user()->first_name}} {{Auth::user()->last_name}}">
                        <input class="subscribe-input" type="email" id="email" name="email"
                               placeholder="email@exacmple.com" value="{{Auth::user()->email}}">
                        <input class="subscribe-input" type="text" id="address" name="address" placeholder="Billing Address"
                               value="">
                        <input class="subscribe-input" type="text" id="city" name="city" placeholder="City"
                               value="">
                        <input class="subscribe-input" type="text" id="country" name="country" placeholder="Country"
                               value="">
                        <input class="subscribe-input" type="text" id="postalcode" name="postcode" placeholder="Postal Code"
                               value="">
                        <input class="subscribe-input" type="text" id="phonenumber" name="phonenumber" placeholder="Phone Number"
                               value="">
                        <div class="stripe-option">
                            <h4 for="card-element mb-4" class="mb-4">
                                Credit or debit card
                            </h4>
                            <div id="card-element">
                                <!-- A Stripe Element will be inserted here. -->
                            </div>

                            <!-- Used to display Element errors. -->
                            <div id="card-errors" role="alert"></div>
                        </div>

                        <button class="subscribe-close" type="button">Close</button>
                        <button class="subscribe-submit">Get Started</button>
                    </form>
                </div>
            </div>
        </div>
    @endif

@endsection
@section('front_js')
    <script src="https://js.stripe.com/v3/"></script>
    <script>

        // Tab


        function tab(evt, pageName, elmnt, color) {
            var i, tabbody, tablinks;
            tabbody = document.getElementsByClassName("tabbody");
            for (i = 0; i < tabbody.length; i++) {
                tabbody[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablink");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(pageName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        document.getElementById("defaultOpen").click();


        // Subscribe


        function incrementValue(e) {
            e.preventDefault();
            var fieldName = $(e.target).data('field');
            var parent = $(e.target).closest('div');
            var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

            if (!isNaN(currentVal)) {
                parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
            } else {
                parent.find('input[name=' + fieldName + ']').val(0);
            }
        }

        function decrementValue(e) {
            e.preventDefault();
            var fieldName = $(e.target).data('field');
            var parent = $(e.target).closest('div');
            var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

            if (!isNaN(currentVal) && currentVal > 0) {
                parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
            } else {
                parent.find('input[name=' + fieldName + ']').val(0);
            }
        }

        $('.credits-box').on('click', '.button-plus', function (e) {
            incrementValue(e);
        });
        $('.credits-box').on('click', '.button-minus', function (e) {
            decrementValue(e);
        });


        // Tab List Info Expand

        $(".list-expand").click(function (e) {
            e.preventDefault();
            $(this).find(".list-plus").toggleClass("plus-rotate");
            $(this).siblings(".list-info").animate({height: "toggle", opacity: "toggle"});
        });


        // Subscribe Pop up

        $(".subscribe-btn").click(function (e) {
            e.preventDefault();
            $(".pop-up-wrapper").fadeIn(500);
        });

        $(".subscribe-close").click(function (e) {
            e.preventDefault();
            $(".pop-up-wrapper").fadeOut(250);
        });


        // Back To Top

        $(document).ready(function () {
            $('a[href^="#"]').click(function (event) {
                var id = $(this).attr("href");
                var offset = 0;
                var target = $(id).offset().top - offset;
                $('html, body').animate({scrollTop: target}, 500);
                event.preventDefault();
            });
        });


        // Disabling Hove In Mobile

        function watchForHover() {
            var hasHoverClass = false;
            var container = document.body;
            var lastTouchTime = 0;

            function enableHover() {
                if (new Date() - lastTouchTime < 500) return;
                if (hasHoverClass) return;
                container.className += ' hasHover';
                hasHoverClass = true;
            }

            function disableHover() {
                if (!hasHoverClass) return;
                container.className = container.className.replace(' hasHover', '');
                hasHoverClass = false;
            }

            function updateLastTouchTime() {
                lastTouchTime = new Date();
            }

            document.addEventListener('touchstart', updateLastTouchTime, true);
            document.addEventListener('touchstart', disableHover, true);
            document.addEventListener('mousemove', enableHover, true);
            enableHover();
        }

        watchForHover();


        $(".subscribe-btn").click(function (e) {
            var login = "{{Auth::check()}}";
            if (login != 1) {
                localStorage.setItem("reverse", "pricing");
                window.location.href = 'login';

            } else {
                var emailverify = "{{@Auth::user()->email_verified_at}}";
                if(emailverify == ''){
                    window.location.href = 'email/verify';
                }
                e.preventDefault();
                $(".pop-up-wrapper").fadeIn(500);
            }

        });

        $(".subscribe-close").click(function (e) {
            e.preventDefault();
            $(".pop-up-wrapper").fadeOut(250);
        });


    </script>

    <script>
        document.getElementById("year").innerHTML = new Date().getFullYear();
        var stripe = Stripe('pk_live_51BGb01GpPRVu83dOA5IHbgpNWer4fFA6YWTLCxCl7Man9Z8kThfwHJleWY6A88E992JLO0f7tiemCmvFvbXb9Znz004K8EDmHG');
        // var stripe = Stripe('pk_test_51HBe6ZJdvXf76erCnMG8GKI82QLbj1TkFptSfbaJ4FTw0DBkEgZBb5hoEI0HM9kJQhVeMUktQrO2enIzgJRKMxDg00gnS3hiPk');
        var elements = stripe.elements();
        var style = {
            base: {
                // Add your base input styles here. For example:
                fontSize: '16px',
                color: '#32325d',
            },
        };

        // Create an instance of the card Element.
        var card = elements.create('card', {style: style});

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');

        var form = document.getElementById('subscription');

        form.addEventListener('submit', function (event) {
            event.preventDefault();
            stripe.createToken(card).then(function (result) {
                if (result.error) {
                    // Inform the customer that there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    // Send the token to your server.
                    stripeTokenHandler(result.token);
                }
            });
        });


        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('subscription');
            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'stripeToken');
            hiddenInput.setAttribute('value', token.id);
            form.appendChild(hiddenInput);

            // Submit the form
            form.submit();
        }
    </script>

@endsection
@section('footer_sc')
    <div class="footer-icons">
        <a href="#landing-wrapper"><img src="{{URL::to('storage/app/public/Frontassets/media/top.svg')}}"></a>
    </div>
@endsection
