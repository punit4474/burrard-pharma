@extends('layouts.front')
@section('front_title')
    Generator
@endsection
@section('meta')
    <link rel="canonical" href="http://www.burrardpharma.com/"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Title"/>
    <meta property="og:description" content="Description"/>
    <meta property="og:url" content="http://www.burrardpharma.com/"/>
    <meta property="og:site_name" content="Burrard Pharma"/>
    <meta name="description" content="Description"/>
    <meta name="twitter:title" content="Title">
    <meta name="twitter:description" content="Description">
@endsection
@section('front_css')
<style>
    .generator {
        background: #00c0ff;
        color: #fff;
    }

    </style>
    <style>
        #loader {
            border: 12px solid #00c0ff;
            border-radius: 50%;
            border-top: 12px solid #00c0ff;
            width: 70px;
            height: 70px;
            animation: spin 1s linear infinite;
            z-index: 9999;
        }

        @keyframes spin {
            100% {
                transform: rotate(360deg);
            }
        }

        .center {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: auto;
        }

        ::-webkit-scrollbar {
        -webkit-appearance: none;
        margin-right: 2px
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 4px;
            margin: 3px;
            background-color: rgb(0 192 255);
            box-shadow: 0 0 1px rgba(255, 255, 255, .5);
        }

        #strengh {
            max-height: 65px;
            overflow-y: scroll;
            margin-right: 7px;
        }

        .drug-name h1 {
            max-width: 500px;
            word-break: break-all;
        }

    </style>

@endsection
@section('front_content')
<div id="loader" class="center" style="display: none"></div>
<div class="generator-navbar">
    <div class="generator-container">
        <div class="generator-filter-wrap">
            <div class="generator-search">
                <input type="text" name="search" placeholder="Search" class="search" autocomplete="off">
            </div>
            <div class="generator-filter">
                <div class="filter-dropdown">
                    <div class="filter-click">
                        <span>Filter</span>
                        <img class="filter-icon" src="{{URL::to('storage/app/public/Frontassets/media/filter.svg')}}">
                    </div>
                    <div class="letter-item-wrap">
                        <div class="letter-items a">
                            <button class="letterbtn" id="a">a</button>
                            <button class="letterbtn" id="b">b</button>
                            <button class="letterbtn" id="c">c</button>
                            <button class="letterbtn" id="d">d</button>
                            <button class="letterbtn" id="e">e</button>
                            <button class="letterbtn" id="f">f</button>
                            <button class="letterbtn" id="g">g</button>
                            <button class="letterbtn" id="h">h</button>
                            <button class="letterbtn" id="i">i</button>
                            <button class="letterbtn" id="j">j</button>
                            <button class="letterbtn" id="k">k</button>
                            <button class="letterbtn" id="l">l</button>
                            <button class="letterbtn" id="m">m</button>
                        </div>
                        <div class="letter-items b">
                            <button class="letterbtn" id="n">n</button>
                            <button class="letterbtn" id="o">o</button>
                            <button class="letterbtn" id="p">p</button>
                            <button class="letterbtn" id="q">q</button>
                            <button class="letterbtn" id="r">r</button>
                            <button class="letterbtn" id="s">s</button>
                            <button class="letterbtn" id="t">t</button>
                            <button class="letterbtn" id="u">u</button>
                            <button class="letterbtn" id="v">v</button>
                            <button class="letterbtn" id="w">w</button>
                            <button class="letterbtn" id="x">x</button>
                            <button class="letterbtn" id="y">y</button>
                            <button class="letterbtn" id="z">z</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="generator-btns">
            <button class="letterbtn" id="all" type="reset">Reset filter</button>
            <input class="credits-count" type="text" step="1"  value="0 Documents" name="quantity" readonly>
            <button class="submit-btn download" type="submit" disabled="disabled" form="nameform" value="Submit">Purchase</button>
        </div>
    </div>
</div>


<div id="generator-wrapper">
    <form class="generator-container" action="{{URL::to('checkout')}}" method="post" name="report_generate">
        @csrf
        <input type="hidden" name="report_data" class="report_data">
        <div class="generator-containers">
        @forelse($data as $row)
        <label class="drug-card">
            <input class="select" type="checkbox" name='report[]' data-id="{{$row->id}}" value='{{$row->id}}'>
            <div class="drug-name">
                <h1>{{$row->name}}</h1>
                <div class="checkbox-icon"></div>
            </div>
            <div class="drug-info-left">
                <div id="dosage">{{$row->dosage_form}}</div>
                <div id="strengh">{{$row->strengh}}</div>
            </div>
            <div class="drug-info-right">
                <div id="route">{{$row->route_of_admin}}</div>
                <div id="code">{{$row->atc_code}}</div>
            </div>
        </label>
        @empty
        @endforelse
        </div>
    </form>
</div>



@endsection
@section('front_js')
<script>

    $(document).on('click','.select',function(){
        var count = $('.select').filter(':checked').length;


        var getReport = localStorage.getItem('report');

        if(getReport != '' &&  getReport != null) {
                getReport = getReport.split(",");
            } else {
                getReport = [];
            }

        $('.credits-count').val(getReport.length + " Documents");

        if(getReport.length > 0 || count == 1){
            $('.download').addClass('generator');
            $('.download').removeAttr('disabled');
        }
    });
    // Select Drug Cards

    $(document).on('click','.submit-btn',function(){

        var auth = '{{Auth::check()}}';

        var qty = $('.credits-count').val();
        if(qty <= 0){
            alert('please select any Report');
        } else {
            var value = new Array();;
            $('.select:checkbox:checked').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
                value.push(sThisVal);
            });

            if(auth != ''){
                $('.generator-container').submit();
            } else {
                localStorage.setItem("reverse", "generator");
                window.location.href ='login';
            }

        }
    });

    $(document).on('change','.select',function(){
        var favorite = [];
        var getReport = localStorage.getItem('report');

        var id = $(this).val();

            if(getReport != '' &&  getReport != null) {
                getReport = getReport.split(",");
            } else {
                getReport = [];
            }

            $.each($('.select').filter(':checked'), function(){
                favorite.push($(this).val());
            });

            var newArray = getReport.concat(favorite)
            var unique = newArray.filter(function(elem, index, self) {
                return index === self.indexOf(elem);
            })

        if ($(this).is(":checked")) {
            $(this).closest(".drug-card").addClass('selected-card');
            $('.credits-count').val(unique.length + " Documents");
            $('.report_data').val(unique.toString());
            localStorage.setItem('report',unique.toString());
        } else {
            $(this).closest(".drug-card").removeClass('selected-card');
            var count = $('.select').filter(':checked').length;
            var index = unique.indexOf(id);
            if (index > -1) {
                unique.splice(index, 1);
            }
            console.log(unique);
            localStorage.setItem('report',unique.toString());
            $('.report_data').val(unique.toString());
            $('.credits-count').val(unique.length + " Documents");

            if(unique.length == 0){
                $('.download').removeClass('generator');
                $('.download').attr('disabled','disabled');
            }
        }
    });

    // Open Filter In Generator

    $(".filter-click").click(function() {
        $(".filter-click").toggleClass("filter-radius");
        $(".filter-dropdown").toggleClass("filter-shadow");
        $(".letter-items").toggleClass("filter-show");
    });

    $(".filter-click").click(function() {
        $(".filter-on").toggleClass("hide-filter");
        $(".filter-off").toggleClass("show-close");
    });

    // Filter Drug Cards By First Letter

    var $boxes = $('.generator-container > .generator-containers > .drug-card');

    var $letterbtn = $('.letterbtn').click(function() {
        var id = this.id;
        console.log(id);
        if (id == 'all') {
            getData('a');
        } else {

            getData(id);
        }
        $letterbtn.removeClass('active');
        $(this).addClass('active');
    })



    function getData(id){
        $.ajax({
                url: "generator-ajax?page="+id,
                type: "GET",
                beforeSend: function() {

                    $('#loader').css('display','block');
                    $('#generator-wrapper').css('opacity','0.2');
                },
                success: function (data) {
                   var html = '';
                   var getReport = localStorage.getItem('report');

                    if(getReport != '' &&  getReport != null) {
                            getReport = getReport.split(",");
                    } else {
                        getReport = [];
                    }
                    console.log(getReport);
                    var finalId = [];
                   $(data).each(function(index, value) {
                       html +='<label class="drug-card">'+
                                '<input class="select" type="checkbox" name="report[]" data-id="'+value.id+'" value="'+value.id+'">'+
                                '<div class="drug-name">'+
                                    '<h1>'+value.name+'</h1>'+
                                    '<div class="checkbox-icon"></div>'+
                                '</div>'+
                                '<div class="drug-info-left">'+
                                    '<div id="dosage">'+value.dosage_form+'</div>'+
                                    '<div id="strengh">'+value.strengh+'</div>'+
                                '</div>'+
                                '<div class="drug-info-right">'+
                                    '<div id="route">'+value.route_of_admin+'</div>'+
                                    '<div id="code">'+value.atc_code+'</div>'+
                                '</div>'+
                            '</label>';

                        if($.inArray((value.id).toString(), getReport) >= 0) {
                            finalId.push(value.id);
                        }

                    });

                    $('.generator-containers').html(html);
                    jQuery("#preloader").remove();
                    $('#loader').css('display','none');
                    $('#generator-wrapper').css('opacity','1');
                    $(finalId).each(function(index,value){
                        $('.select[data-id='+value+']').trigger( "click" );
                    });

                }
            });

    }

    $(document).on('keyup','.search',function(){
        var value  = $(this).val();

        if(value.length >= 3){
            getData(value);
        } else if(value.length == 0) {
            getData('a');
        }

    });

    $(document).ready(function(){
        var getReport = localStorage.getItem('report');

        if(getReport != '' &&  getReport != null) {
                getReport = getReport.split(",");
        } else {
            getReport = [];
        }

        $.each(getReport, function(index, value){
            $('.select[data-id='+value+']').trigger( "click" );

        });

    });
</script>
<script>
    // Back To Top

    $(document).ready(function () {
        $('a[href^="#"]').click(function (event) {
            var id = $(this).attr("href");
            var offset = 0;
            var target = $(id).offset().top - offset;
            $('html, body').animate({scrollTop: target}, 500);
            event.preventDefault();
        });
    });


    // Pageload Delay

    function delay (URL) {
        setTimeout( function() { window.location = URL }, 1700 );
    }
    $(window).bind("pageshow", function(event) {
        if (event.originalEvent.persisted) {
            window.location.reload()
        }
    });

    $(window).scroll(function() {
        if ($(this).scrollTop() > 1) {
            $("#nav-hidden").stop().fadeIn(250)
        } else {
            $("#nav-hidden").stop().fadeOut(250)
        }
    });

    function watchForHover() {
        var hasHoverClass = false;
        var container = document.body;
        var lastTouchTime = 0;

        function enableHover() {
            if (new Date() - lastTouchTime < 500) return;
            if (hasHoverClass) return;
            container.className += ' hasHover';
            hasHoverClass = true;
        }

        function disableHover() {
            if (!hasHoverClass) return;
            container.className = container.className.replace(' hasHover', '');
            hasHoverClass = false;
        }

        function updateLastTouchTime() {
            lastTouchTime = new Date();
        }

        document.addEventListener('touchstart', updateLastTouchTime, true);
        document.addEventListener('touchstart', disableHover, true);
        document.addEventListener('mousemove', enableHover, true);
        enableHover();
    }

    watchForHover();

</script>
@endsection

@section('footer_sc')
    <div class="footer-icons">
        <a href="#landing-wrapper"><img src="{{URL::to('storage/app/public/Frontassets/media/top.svg')}}"></a>
    </div>
@endsection
