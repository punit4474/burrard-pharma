@extends('layouts.front')
@section('front_title')
    Chekout
@endsection
@section('meta')
    <link rel="canonical" href="http://www.burrardpharma.com/"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Title"/>
    <meta property="og:description" content="Description"/>
    <meta property="og:url" content="http://www.burrardpharma.com/"/>
    <meta property="og:site_name" content="Burrard Pharma"/>
    <meta name="description" content="Description"/>
    <meta name="twitter:title" content="Title">
    <meta name="twitter:description" content="Description">
@endsection
@section('front_css')

    <style type="text/css">
        .panel-title {
            display: inline;
            font-weight: bold;
        }

        .display-table {
            display: table;
        }

        .display-tr {
            display: table-row;
        }

        .display-td {
            display: table-cell;
            vertical-align: middle;
            width: 61%;
        }

        .has-error input {
            border-color: red;
        }

        .mb-4 {
            margin-bottom: 10px;
        }

        h4, #card-element, #card-errors {
            font-family: 'SofiaPro-light';
        }

    </style>
@endsection
@section('front_content')
    <div id="checkout-wrapper">
        <div class="checkout-container">
            <div class="checkout-content">

                <div class="checkout-content-box">
                    <h1>Technical Documents</h1>
                </div>

                <div class="drug-list-scroll">
                    @foreach($data['reportData'] as $row)

                        <label class="checkout-drug-card">
                            <input class="deselect" type="checkbox">
                            <div class="drug-name">
                                <h1>{{$row->name}}</h1>
                            </div>
                            <div class="drug-info-left">
                                <div id="dosage">{{$row->dosage_form}}</div>
                                <div id="strengh">{{$row->strengh}}</div>
                            </div>
                            <div class="drug-info-right">
                                <div id="route">{{$row->route_of_admin}}</div>
                                <div id="code">{{$row->atc_code}}</div>
                            </div>
                            <div class="selected-checkbox-icon remove" data-id="{{$row->id}}"><img
                                    src="{{URL::to('storage/app/public/Frontassets/media/cancel.svg')}}"></div>
                        </label>
                    @endforeach
                    <a href="{{URL::to('generator')}}" class="new_rp" style="display: none">
                        <button class="btn btn-primary btn-lg btn-block">Add New Report</button>
                    </a>
                </div>


            </div>
            <div class="checkout-content">
                <div class="checkout-content-box content-box-2">
                    <h1>Check out</h1>
                </div>
                <form action="{{route('stripe.post')}}" method="post" class="require-validation " name="charge"
                      id="payment-form">
                    @csrf
                    <div class="card-display" style="width: 100%;">

                        <div class='form-row row'>
                            <div class='col-xs-12 form-group required'>
                                <label class='control-label'>Name on Card</label> <input
                                    class='form-control' size='4' type='text' placeholder="Name" name="name" required autocomplete="off"
                                    value="">
                            </div>
                        </div>
                        <div class='form-row row'>
                            <div class='col-xs-12 form-group required'>
                                <label class='control-label'>E-mail</label>
                                <input class="form-control" type="email" id="email" name="email" required autocomplete="off"
                                       placeholder="email@exacmple.com" value="">
                            </div>
                        </div>
                        <div class='form-row row'>
                            <div class='col-xs-12 form-group required'>
                                <label class='control-label'>Billing Address</label>
                                <input class="form-control" type="text" id="address" name="address" required autocomplete="off"
                                       placeholder="Billing Address" value="">
                            </div>
                        </div>
                        <div class='form-row row'>
                            <div class='col-xs-12 form-group required'>
                                <label class='control-label'>City</label>
                                <input class="form-control" type="text" id="city" name="city" placeholder="City" required autocomplete="off"
                                       value="">
                            </div>
                        </div>
                        <div class="input-50-wrap">
                            <div class="form__row form__row_2">
                                <div class='col-xs-12 form-group required'>
                                    <label class='control-label'>Country</label>
                                    <input class="form-control" type="text" id="country" name="country" required autocomplete="off"
                                           placeholder="Country" value="">
                                </div>
                            </div>
                            <div class="form__row form__row_2">
                                <label class='control-label'>Postal Code</label>
                                <input class="form-control" type="text" id="postalcode" name="postalcode" required autocomplete="off"
                                       placeholder="Postal Code" value="">
                            </div>

                        </div>


                        <div class='form-row row'>
                            <div class='col-xs-12 form-group required'>
                                <label class='control-label'>Phone Number</label>
                                <input class="form-control" type="text" id="phone_number" name="phone_number" required autocomplete="off"
                                       placeholder="Phone Number" value="">
                            </div>
                        </div>

                        <input type="hidden" name="report" class="report_data"
                               value="{!! implode(',',$data['report']) !!}">
                        <input type="hidden" name="amount" class="amount" value="{{count($data['reportData']) * 5000}}">
                        {{-- <input type="hidden" name="report_name" value="{!! implode(',',$data['reportData']) !!}"> --}}

                        <div class="pay_option">
                            <label for="stripe" class="pay_option_box">
                                <input type="radio" id="stripe" name="payment" class="payment" value="stripe" checked>
                                <div class="stripe_dv"><img
                                        src="{{URL::to('storage/app/public/Frontassets/media/bank.svg')}}"></div>
                            </label>
                            <label for="paypal" class="pay_option_box">
                                <input type="radio" id="paypal" name="payment" class="payment" value="paypal">
                                <div class="paypal_dv"><img
                                        src="{{URL::to('storage/app/public/Frontassets/media/paypal.svg')}}"></div>
                            </label>
                        </div>
                        <div class="stripe-option">
                            @if(!empty($userCard))
                                <label for="saved-card" class="saved-card-wrap">
                                    <input type="checkbox" name="last_card" id="saved-card"
                                           value="{{$userCard['card_id']}}">
                                    <h5>**** **** **** {{$userCard['last4']}}</h5>
                                    <div class="saved-card-date-wra">
                                        <p>{{$userCard['exp_month']}}/{{$userCard['exp_year']}}</p>
                                        @if($userCard['card_brand'] == 'Visa')
                                            <img src="{{URL::to('storage/app/public/Frontassets/media/card/visa.svg')}}"
                                                 alt="">
                                        @elseif($userCard['card_brand'] == 'MasterCard')
                                            <img
                                                src="{{URL::to('storage/app/public/Frontassets/media/card/mastercard.svg')}}"
                                                alt="">
                                        @elseif($userCard['card_brand'] == 'UnionPay')
                                            <img
                                                src="{{URL::to('storage/app/public/Frontassets/media/card/unionpay.svg')}}"
                                                alt="">
                                        @elseif($userCard['card_brand'] == 'JCB')
                                            <img src="{{URL::to('storage/app/public/Frontassets/media/card/jcb.svg')}}"
                                                 alt="">
                                        @elseif($userCard['card_brand'] == 'Discover')
                                            <img
                                                src="{{URL::to('storage/app/public/Frontassets/media/card/discover.svg')}}"
                                                alt="">
                                        @elseif($userCard['card_brand'] == 'Diners Club')
                                            <img
                                                src="{{URL::to('storage/app/public/Frontassets/media/card/diners-club.svg')}}"
                                                alt="">
                                        @elseif($userCard['card_brand'] == 'American Express')
                                            <img
                                                src="{{URL::to('storage/app/public/Frontassets/media/card/american-express.svg')}}"
                                                alt="">
                                        @else
                                            <img
                                                src="{{URL::to('storage/app/public/Frontassets/media/card/credit-card.svg')}}"
                                                alt="">
                                        @endif

                                    </div>
                                </label>
                            @endif
                            <h4 for="card-element mb-4" class="mb-4">
                                Credit or debit card
                            </h4>
                            <div id="card-element">
                                <!-- A Stripe Element will be inserted here. -->
                            </div>
                            <label for="saved-check" class="saved-check">
                                <input type="checkbox" id="saved-check" name="savecard" class="savecard"> Save Card
                            </label>
                            <!-- Used to display Element errors. -->
                            <div id="card-errors" role="alert"></div>
                        </div>



                    </div>

                    <button class="btn btn-primary btn-lg btn-block submit_btn">Submit Payment ($<label
                            class="final_amount">{{count($data['reportData']) * 5000}}</label>)
                    </button>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('front_js')
    {{--    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>--}}
    <script src="https://js.stripe.com/v3/"></script>
    <script type="text/javascript">

        document.getElementById("year").innerHTML = new Date().getFullYear();
        var stripe = Stripe('pk_live_51BGb01GpPRVu83dOA5IHbgpNWer4fFA6YWTLCxCl7Man9Z8kThfwHJleWY6A88E992JLO0f7tiemCmvFvbXb9Znz004K8EDmHG');
        // var stripe = Stripe('pk_test_51BGb01GpPRVu83dOD3MKa0pz0UhUYQy0mZM3QlHDuUMEtufLEgcPXgz7DjLoImgUYlFW1fghklhAqBejybkQo4B30038Gdk1RT');
        var elements = stripe.elements();
        var style = {
            base: {
                // Add your base input styles here. For example:
                fontSize: '16px',
                color: '#32325d',
            },
        };

        // Create an instance of the card Element.
        var card = elements.create('card', {style: style});

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');

        var form = document.getElementById('payment-form');


        form.addEventListener('submit', function (event) {
            event.preventDefault();
            var radioValue = $("input[name='payment']:checked").val();

            if (radioValue == 'stripe') {
                if ($('#saved-card').is(':checked')) {
                    form.submit();
                } else {
                    stripe.createToken(card).then(function (result) {
                        if (result.error) {
                            // Inform the customer that there was an error.
                            var errorElement = document.getElementById('card-errors');
                            errorElement.textContent = result.error.message;
                        } else {
                            // Send the token to your server.
                            stripeTokenHandler(result.token);
                        }
                    });
                }

            } else if (radioValue == 'paypal') {
                form.submit();
            }

        });


        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('payment-form');
            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'stripeToken');
            hiddenInput.setAttribute('value', token.id);
            form.appendChild(hiddenInput);

            // Submit the form
            form.submit();
        }

        $(document).on('click', '.remove', function () {
            var id = $(this).data('id');
            var reportData = $('.report_data').val();
            var amount = $('.amount').val();

            var res = reportData.split(",");

            res.splice($.inArray(String(id), res), 1);

            res.join();

            $('.report_data').val(res);
            var amounts = amount - 5000;
            $('.amount').val(amounts);
            $('.final_amount').text(amounts);
            var values = $(this).parent('label');
            $(values).fadeOut("slow", function () {
                $(this).remove();
            });

            if (amounts == '0') {
                $('.new_rp').css('display', 'block');
                $('.submit_btn').attr('disabled', 'disabled');
            }

        });

        $(document).ready(function () {
            checkPayment();
            localStorage.removeItem('report');
            localStorage.removeItem('reverse');
        });

        $(document).on('click', '.payment', function () {
            checkPayment();
        })

        function checkPayment() {
            var radioValue = $("input[name='payment']:checked").val();
            if (radioValue == 'stripe') {
                $('.stripe-option').css('display', 'block');
                $('.paypal-box').css('display', 'none');
            } else if (radioValue == 'paypal') {
                $('.paypal-box').css('display', 'block');
                $('.stripe-option').css('display', 'none');
            }
        }


    </script>
@endsection
@section('footer_sc')
    <div class="footer-icons">
        <a href="#landing-wrapper"><img src="{{URL::to('storage/app/public/Frontassets/media/top.svg')}}"></a>
    </div>
@endsection
