@extends('layouts.front')
@section('front_title')
    Success
@endsection
@section('meta')
    <link rel="canonical" href="http://www.burrardpharma.com/"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Title"/>
    <meta property="og:description" content="Description"/>
    <meta property="og:url" content="http://www.burrardpharma.com/"/>
    <meta property="og:site_name" content="Burrard Pharma"/>
    <meta name="description" content="Description"/>
    <meta name="twitter:title" content="Title">
    <meta name="twitter:description" content="Description">
@endsection
@section('front_css')
    <style>
        h1 {
            font-family: 'SofiaPro-light';
        }
    </style>
@endsection
@section('front_content')
    <div id="checkout-wrapper">
        <div class="checkout-container">

                <div class="">
                    <h1>Payment Failed. Try again later</h1>

                </div>

        </div>
    </div>
@endsection
@section('front_js')
<script>
    function watchForHover() {
        var hasHoverClass = false;
        var container = document.body;
        var lastTouchTime = 0;

        function enableHover() {
            if (new Date() - lastTouchTime < 500) return;
            if (hasHoverClass) return;
            container.className += ' hasHover';
            hasHoverClass = true;
        }

        function disableHover() {
            if (!hasHoverClass) return;
            container.className = container.className.replace(' hasHover', '');
            hasHoverClass = false;
        }

        function updateLastTouchTime() {
            lastTouchTime = new Date();
        }

        document.addEventListener('touchstart', updateLastTouchTime, true);
        document.addEventListener('touchstart', disableHover, true);
        document.addEventListener('mousemove', enableHover, true);
        enableHover();
    }

    watchForHover();
</script>
@endsection
