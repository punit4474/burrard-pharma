@extends('layouts.front')
@section('front_title')
    Company
@endsection
@section('meta')
    <link rel="canonical" href="http://www.burrardpharma.com/"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Title"/>
    <meta property="og:description" content="Description"/>
    <meta property="og:url" content="http://www.burrardpharma.com/"/>
    <meta property="og:site_name" content="Burrard Pharma"/>
    <meta name="description" content="Description"/>
    <meta name="twitter:title" content="Title">
    <meta name="twitter:description" content="Description">
@endsection
@section('front_css')
<script src='https://www.google.com/recaptcha/api.js' async defer ></script>
<script type="text/javascript">
    var onloadCallback = function() {
      grecaptcha.render('html_element', {
        'sitekey' : '6Lc_8cEZAAAAANUCdcYAEj6SIZ6sJJGRnvYa2xqZ'
      });
    };
  </script>
  <style>
  .alert.alert-success {
		margin-bottom: 10px;
		text-align: center;
		padding: 10px;
		background: #00c0ff;
		color: white;
		border-radius: 15px;
		font-family: 'SofiaPro-regular';
		font-size: 1rem;
	}
  .invalid-feedback strong {
      font-family: 'SofiaPro-regular';
  }
  </style
@endsection
@section('front_content')
	<div id="full-wrapper">
		<div class="full-container">
			<video class="video-content"
				poster=""
				src="{{URL::to('storage/app/public/Frontassets/media/company-banner.mp4')}}"
				type="video/mp4"
				autoplay loop playsinline muted
				data-object-fit="cover">
			</video>
			<div class="full-content-box">
				<div class="content-box-text">
					<h1>We are your partner in drug development</h1>
				</div>
			</div>
			<a class="dark-bg-btn" href="#about-wrapper">
				<div class="btn-icon">
					<img src="{{URL::to('storage/app/public/Frontassets/media/down-white.svg')}}">
				</div>
			</a>
		</div>
	</div>

	<div id="about-wrapper">
		<div class="about-container">
			<div class="about-content">
				<div class="about-img">
					<img src="{{URL::to('storage/app/public/Frontassets/media/about-img.jpg')}}">
				</div>
			</div>
			<div class="about-content">
				<div class="about-text">
					<h2>Who We Are</h2>
					<p>Burrard Pharma is an established pharmaceutical R&D organisation headquartered in Vancouver, British Columbia, Canada. Carrying over decades of Drug Development, Drug Discovery experience, empowering us to be focused on developing and commercializing a new generation of molecules, that potentially could lead us to the new pathways of the shake up the basics of treatment for people with any kind of complicated disease experience. Burrard Pharma, with proven skills and established resources, discover quality pharmaceutical products to not only meet your expectations but to exceed them.</p>
					<p>Burrard Pharma represent our strong ties with the Pacific Northwest. We are committed to providing you with excellent solutions in drug development/formulation, technology transfer as well as CRO/CDMO services.</p>
				</div>
			</div>

			<div class="about-content">
				<div class="about-text">
					<h2>Our Vision</h2>
					<p>We aim to improve therapies. Our competitive advantage is built upon delivering successful results to our clients, resulting in maximum patients satisfaction. We are the unrivalled choice when it comes to drug developments, formulation, clinical research and research on gene therapies.</p>
				</div>
			</div>
			<div class="about-content">
				<div class="about-text">
					<h2>Our Values</h2>
					<p>Our Values are, make a revolution in drug discovery to empower disease therapy through constant innovation, diligent work, and ethical practice. Our team is knowledgeable, honest, hardworking and personable. Through aligned setup, Burrard Pharma offers exceptional value, mutual respect and excellence to our clients globally. </p>
				</div>
			</div>
		</div>
	</div>


	<div id="large-wrapper">
		<div class="large-container">

			<div class="large-text">
				<div class="large-container-box">
					<h1>We have over a<br>decade of experience<br>in pharmaceuticals.</h1>
					<p>We have the knowledge and expertise to complete almost any kind of drug formulations.</p>
					<a class="normal-btn" href="{{URL::to('service')}}">Services</a>
				</div>
			</div>


				<div class="space-hidden"></div>
				<video class="large-media"
					poster=""
					src="{{URL::to('storage/app/public/Frontassets/media/company-body.mp4')}}"
					type="video/mp4"
					autoplay loop playsinline muted
					data-object-fit="cover">
				</video>


		</div>
	</div>


    <div id="center-wrapper">
		<div class="center-container">
			<div class="center-text">
				<h1>Meet our team<br>with partners from all over the world</h1>
			</div>
			<div class="team-container">
				<div class="team-column">
					<div class="team-content">
						<div class="team-img">
							<img src="{{URL::to('storage/app/public/Frontassets/media/team/1.jpeg')}}">
						</div>
						<div class="team-member">
							<h2>Dr. Kayhan Moayeri (CEO)</h2>

						</div>
						<div class="team-btn">
							<p>Read More</p>
							<img class="team-plus" src="{{URL::to('storage/app/public/Frontassets/media/plus.svg')}}">
						</div>
						<div class="team-info">
							<p>Dr. Kayhan Moayeri is an entrepreneurial pharmacist who specializes in business development and drug discovery with two decades of experience. As CEO of Amerix Pharmaceutical, Dr. Moayeri combines his unique experience in both business and the pharmaceutical industry to oversee the quality operations of the company. Prior to founding Amerix Pharmaceutical, Dr. Moayeri worked at Sanofi, a French multinational pharmaceutical company and a global healthcare leader that is highly focused on patient needs. He then found Amerix Pharmaceutical to head up their global business development division. Dr. Moayeri is an expert in drug formulation, drug discovery, and contract manufacturing. Over the last few years, he has masterminded contract manufacturing projects worth eight figures of Canadian dollars. Dr. Moayeri holds a Doctor of Philosophy in Pharmacy from Azad University and a Master of Business Administration from University of Nebraska-Lincoln.</p>
						</div>
					</div>
					<div class="team-content">
						<div class="team-img">
							<img src="{{URL::to('storage/app/public/Frontassets/media/team/4.jpg')}}">
						</div>
						<div class="team-member">
							<h2>Dr. James Jaquith</h2>
						</div>
						<div class="team-btn">
							<p>Read More</p>
							<img class="team-plus" src="{{URL::to('storage/app/public/Frontassets/media/plus.svg')}}">
						</div>
						<div class="team-info">
                            <p>Dr. James Jaquith is the Head of Medicinal Chemistry at The Centre for Drug Research and Development (CDRD), Canada’s national translational and commercialization centre. CDRD plays a key role in the evaluation, funding, de-risking, and strategic development of academic research programs from Canadian investigators. Current in-house programs focus on novel approaches for the treatment of rare genetic diseases, fibrotic and inflammatory conditions, pathogenic infections, peripheral neuropathies, diabetes, Parkinson’s disease, and cancer, with a special focus on novel ADC platforms.</p>
                            <p>
                                Dr. Jaquith also leads CDRD’s Regenerative Medicine, Inflammation and Fibrosis Task Force which specifically seeks to identify research projects that are at the cutting edge of the Regenerative Medicine field. Our primary focus is on the development of small molecule, antibody and antibody drug conjugate therapies for the treatment of inflammatory and fibrotic diseases.
                            </p>
                            <p>
                                Prior to joining CDRD, Dr. Jaquith was the Director of Chemistry at Aegera Therapeutics (Pharmascience Inc.) where he made important contributions to the discovery and development of several clinical development candidates which advanced into clinical trials. Dr. Jaquith received his B.Sc. and M.Sc. in Chemistry from the University of Waterloo, and a Ph.D. from the University of Ottawa, before joining Apoptogen Inc. as an NSERC Industrial Research Fellow.
                            </p>
						</div>
                    </div>

					<div class="team-content">
						<div class="team-img">
							<img src="{{URL::to('storage/app/public/Frontassets/media/team/7.jpg')}}">
						</div>
						<div class="team-member">
							<h2>Dr. Sandip Kaur Badyal</h2>

						</div>
						<div class="team-btn">
							<p>Read More</p>
							<img class="team-plus" src="{{URL::to('storage/app/public/Frontassets/media/plus.svg')}}">
						</div>
						<div class="team-info">
                            <p>Dr Sandip Kaur Badyal obtained a PhD in Biological Chemistry from the University of Leicester after which she joined the lab of Professor Bagshaw on a joint venture research project with Dr Kriajevska to study the effects of mobile cells during metastasis. From a relatively short but successful research period, Dr Badyal has 9 publications in peer reviewed journals.</p>
                            <p>Dr Badyal decided to move into the field of Regulatory Affairs to pursue her passion to use her scientific knowledge to help others. Whilst mastering the skills of the Regulatory Affairs profession she co-partnered a Regulatory Affairs consultancy in Hamburg, Germany for 3 years. Where she proficiently and successfully worked on the lifecycle management and registering of numerous pharmaceutical products exceeding client expectations.</p>
                            <p>Dr Badyal moved to Vancouver, BC, Canada and continues to provide Regulatory Affairs consultancy services whilst also branching out into the field of Medical Devices.</p>
						</div>
					</div>
				</div>


				<div class="team-column">
                    <div class="team-content">
						<div class="team-img">
							<img src="{{URL::to('storage/app/public/Frontassets/media/team/2.jpg')}}">
						</div>
						<div class="team-member">
							<h2>Dr. Colin Collins</h2>
						</div>
						<div class="team-btn">
							<p>Read More</p>
							<img class="team-plus" src="{{URL::to('storage/app/public/Frontassets/media/plus.svg')}}">
						</div>
						<div class="team-info">
							<p>Dr. Collins obtained a PhD in Medical Genetics from the University of British Columbia after which he became faculty at Lawrence Berkeley National Laboratory (LBNL). At LBNL Dr. Collins was a cofounder of the Resource for Molecular Cytogenetics in the Department of Life Sciences. In 1998 Dr. Collins moved to the University of California San Francisco’s (UCSF) Comprehensive Cancer Center. At UCSF Dr. Collins invented and patented “paired end sequencing” and was the first to sequence a tumor genome. He also was the first to show that genome copy number aberrations can be used to risk stratify men with prostate cancer for metastasis. This work also received a patent. In 2009 Dr. Collins relocated to the University of British Columbia (UBC) and the Vancouver Prostate Centre (VPC). At the VPC Dr. Collins Directs the Laboratory for Advanced Genome Analysis (LAGA) and the BGI-VPC Joint Research Laboratory.  These support genome and computational biology research within the VPC, Canada and internationally. It also supports a precision oncology program at the VPC. Dr. Collins has a keen interest in treatment resistant prostate cancer where he and his collaborators have made key discoveries into the mechanisms of therapeutic resistance, identification of novel therapeutic targets and the development of liquid biopsies for prostate and other cancers. Dr. Collins has appointments at Simon Fraser University in the School of Computing Science and at BGI in the Think Tank Division. He has over 190 publications and multiple patents.</p>
						</div>
                    </div>


					<div class="team-content">
						<div class="team-img">
							<img src="{{URL::to('storage/app/public/Frontassets/media/team/5.jpg')}}">
						</div>
						<div class="team-member">
							<h2>Dr. Gary Woodnutt</h2>
						</div>
						<div class="team-btn">
							<p>Read More</p>
							<img class="team-plus" src="{{URL::to('storage/app/public/Frontassets/media/plus.svg')}}">
						</div>
						<div class="team-info">
							<p>Recently Dr. Woodnutt has acted as a Consultant to several small Companies in both the Research and Development area to critically evaluate their ongoing projects as well as to provide planning and oversight for Development projects. Dr. Woodnutt served as CSO of Lpath until December 2016 where he led all Discovery and Development activities concerning the exploitation of monoclonal antibodies to bioactive lipids. Prior to that, Dr. Woodnutt served as the Vice President, Open Innovation at Pfizer/CovX from 2012 to 2013, but his primary role at CovX, from 2006 to 2012, was as Vice President of Biology Research. From 2002 to 2006, Dr. Woodnutt was the Senior Vice President of Pharmaceutical Research and Development for Diversa Corporation. He began his career in the pharmaceutical industry with Glaxo SmithKline Pharmaceuticals, where he was employed for more than 20 years and rose to the position of Vice President and Head of Biology in the Antimicrobial and Host Defense Group. Dr. Woodnutt received his Ph.D. in biochemistry/physiology from the University of Reading, and he has authored numerous scientific articles.</p>
						</div>
                    </div>
                    <div class="team-content">
						<div class="team-img">
							<img src="{{URL::to('storage/app/public/Frontassets/media/team/8.jpg')}}">
						</div>
						<div class="team-member">
							<h2>Luca Citton</h2>
						</div>
						<div class="team-btn">
							<p>Read More</p>
							<img class="team-plus" src="{{URL::to('storage/app/public/Frontassets/media/plus.svg')}}">
						</div>
						<div class="team-info">
                            <p>Luca functions as external legal counsel to Burrard Pharmaceuticals</p>
                            <p>Luca Citton is a Shareholder and member of the Board of Directors of Boughton Law. Practicing primarily in the areas of Corporate and Commercial Law, he advises clients on the structuring, sale and continuing operation of a range of businesses, from small and medium enterprises through to national and multinational corporations.</p>
                            <p>His extensive client base includes companies in the entertainment, food and beverage, manufacturing, professional services and technology sectors. In addition to corporate and commercial law, Luca maintains an ancillary practice in IP law and is a registered Canadian Trademark Agent.</p>
						</div>
					</div>

				</div>



				<div class="team-column">
                    <div class="team-content">
						<div class="team-img">
							<img src="{{URL::to('storage/app/public/Frontassets/media/team/3.jpeg')}}">
						</div>
						<div class="team-member">
							<h2>Timothy Morris</h2>
						</div>
						<div class="team-btn">
							<p>Read More</p>
							<img class="team-plus" src="{{URL::to('storage/app/public/Frontassets/media/plus.svg')}}">
						</div>
						<div class="team-info">
                            <p>Mr. Morris brings over 32 years of professional finance and accounting experience and 20 years as a chief financial officer to the Burrard Advisory Board. Mr. Morris has held several CFO roles most recently as chief financial officer and head of business development for AcelRx Pharmaceuticals, a biotechnology company focusing on the development and commercialization of products for the treatment of moderate-to-severe acute pain in medically supervised settings.</p>
                            <p>Previous to joining AcelRx, Mr. Morris held the positions of chief financial officer at VIVUS, Inc and Questcor Pharmaceuticals. During his career as CFO, he raised over $980 million in equity and convertible securities for 5 different companies. He has extensive deal experience, having completed over 70 transactions with a combined value in excess of $2.0 billion.</p>
                            <p>His work at late-stage biotechnology companies have provided him with valuable experience in regulatory filings and product launches. He has been involved with 4 NDA approvals and applications (including the pending application for DSUVIA, AcelRx’s lead candidate), and 2 MAA approvals (one for ZALVISO, AcelRx’s follow-on candidate, which is approved in Europe). His commercial experience includes the launch of QSYMIA an innovative metabolic small molecule and preparations for the launch of DSUVIA in the US.</p>
                            <p>He also serve of the Board of Directors for KaloBios, (KBIO), a biopharmaceutical company focused on rare and orphan disease and on PAION, Inc, the US subsidiary of PAION AG.</p>
						</div>
                    </div>
                    <div class="team-content">
						<div class="team-img">
							<img src="{{URL::to('storage/app/public/Frontassets/media/team/6.jpg')}}">
						</div>
						<div class="team-member">
							<h2>Dr. Patricia-Ann Truter</h2>

						</div>
						<div class="team-btn">
							<p>Read More</p>
							<img class="team-plus" src="{{URL::to('storage/app/public/Frontassets/media/plus.svg')}}">
						</div>
						<div class="team-info">
                            <p>Dr. Patricia-Ann Truter is a Material Scientist with more than 22 years of product and research and development experience. Her education includes a PhD in Polymer science (Thesis: “The Chemical Development of a Hydrogel to be used as an Artificial Skin”), a MSc in Chemistry (Thesis: “Hydroxy methylation of Kraft Lignin for Novel Adhesive Applications”) and a BSc (Main subjects: Chemistry and Human Physiology). She also attended a 6-week intensive short course: on water soluble polymers at MIT, Boston, USA.</p>
                            <p>Patricia is a professional, technical manager with extensive experience in formulation, biomaterial and product development, technology transfer from idea, development, scale up, clinical trials and commercialization (liquids, parenterals, coatings, solid dosage forms. implants, topical products, hydrogels, etc.). She is the inventor and author of > 20 patents, publications, presentations and posters.</p>
                            <p>Patricia’s work experience range from Vice President and Chief Scientific Officer: R&D (ARC Medical Devices, Vancouver, BC), Director: R&D (Kisameet, Vancouver, BC), Director: Drug Delivery and Biomaterials (Angiotech Pharmaceuticals, Vancouver, BC,) and a Board member of Ellipsoid Technologies (Pretoria, South Africa). Patricia has worked closely and was involved in technology transfers, clinical batch manufacture with international pharmaceutical companies such as Ethypharm, Abbott Laboratories, SA Druggists and Pfizer.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <div id="career-wrapper">
		<div class="career-container">
			<div class="career-text">
				<h1>Why work with us?</h1>
			</div>
			<div class="career-content">
				<div class="career-box">
					<div class="career-img">
						<img src="{{URL::to('storage/app/public/Frontassets/media/experience.svg')}}">
					</div>
					<h1>Experience</h1>
				</div>
				<div class="career-box">
					<div class="career-img">
						<img src="{{URL::to('storage/app/public/Frontassets/media/culture.svg')}}">
					</div>
					<h1>Multicultural</h1>
				</div>
				<div class="career-box">
					<div class="career-img">
						<img src="{{URL::to('storage/app/public/Frontassets/media/innovation.svg')}}">
					</div>
					<h1>Innovation</h1>
				</div>
			</div>
		</div>
	</div>

    <div id="contact-wrapper">
		<div class="contact-container">
			<div class="contact-text">
				<h1>Get in touch!</h1>
			</div>
			<div class="contact-content">
				<div class="contact-box">
					<div class="contact-img">
						<img src="{{URL::to('storage/app/public/Frontassets/media/location.svg')}}">
					</div>
					<div class="contact-info">
						<p>2240 Chippendale Rd West Vancouver,<br/> BC V7S 3J5</p>
					</div>
				</div>
				<div class="contact-box">
					<div class="contact-img">
						<img src="{{URL::to('storage/app/public/Frontassets/media/email.svg')}}">
					</div>
					<div class="contact-info">
						<p><a href="mailto:info@burrardpharma.com" style="text-decoration: none;color: #2c3c49;">info@burrardpharma.com</a></p>
					</div>
				</div>
				<div class="contact-box">
					<div class="contact-img">
						<img src="{{URL::to('storage/app/public/Frontassets/media/phone.svg')}}">
					</div>
					<div class="contact-info">
						<p><a href="tel:+1-778-279-3901" style="text-decoration: none;color: #2c3c49;">+1-778-279-3901</a>/ +1-844-9-PHARMA</p>
					</div>
				</div>
			</div>

			<div class="contact-content">
				<div class="contact-map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2599.1348405980266!2d-123.1740605841335!3d49.34959637394776!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54866e5d9c61768b%3A0x80d53e0698e5aa7c!2s2240%20Chippendale%20Rd%2C%20West%20Vancouver%2C%20BC%20V7S%203J5%2C%20Canada!5e0!3m2!1sen!2sin!4v1598035791941!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

				</div>
			</div>
            <div class="company-img"><img src="{{URL::to('storage/app/public/Frontassets/media/building.jpeg')}}"></div>
			<div class="contact-text" id="contact-form">
				<h1>Or send us a message</h1>
			</div>
			<div class="message-content">
				<div class="row">
                    <div class="col-md-12">
                        @if(Session::has('message'))
                            {!! Session::get('message') !!}
                        @endif
                    </div>
                </div>
                {{Form::open(array('url'=>"contact-us",'name'=>'contact-us','method'=>'post','class'=>'contact-form'))}}
					<div class="w-50">
						{{Form::text('first_name','',array('class'=>'contact-input','id'=>'fname','placeholder'=>'First name','required','style'=>"margin-right: 10px"))}}
						@error('first_name')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
					<div class="w-50">
						{{Form::text('last_name','',array('class'=>'contact-input','id'=>'lname','placeholder'=>'Last name','required'))}}
						@error('last_name')
						<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
					<div class="w-50">
						{{Form::text('phone_number','',array('class'=>'contact-input','id'=>'phone','placeholder'=>'Phone','required','style'=>"margin-right: 10px"))}}
						@error('phone_number')
						<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
					<div class="w-50">
						{{Form::email('email','',array('class'=>'contact-input','id'=>'email','placeholder'=>'E-mail','required'))}}
						@error('email')
						<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
					<div class="w-100">
						{{Form::textarea('message','',array('class'=>'contact-msg','id'=>'message','placeholder'=>'Message','rows'=>3,'required','style'=>'height:200px'))}}
						@error('message')
						<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
                    <div id="html_element" style="margin-bottom: 30px"></div>
                    @error('g-recaptcha-response')
                    <span class="invalid-feedback" role="alert" style="margin-top: 25px;">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                    {{Form::submit('Send Message',array('class'=>"contact-send",'id'=>'send'))}}

                {{Form::close()}}
			</div>
		</div>
	</div>


@endsection
@section('front_js')
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
async defer>
</script>


    <script>


        $(".team-btn").click(function (e) {
            e.preventDefault();
            $(this).find(".team-plus").toggleClass("plus-rotate");
            $(this).siblings(".team-info").animate({height: "toggle", opacity: "toggle"});
        });


    </script>
<script>
    // Back To Top

    $(document).ready(function () {
        $('a[href^="#"]').click(function (event) {
            var id = $(this).attr("href");
            var offset = 0;
            var target = $(id).offset().top - offset - $('.nav-pc').height();
            $('html, body').animate({scrollTop: target}, 500);
            event.preventDefault();
        });
    });


    // Pageload Delay

    function delay (URL) {
        setTimeout( function() { window.location = URL }, 1700 );
    }
    $(window).bind("pageshow", function(event) {
        if (event.originalEvent.persisted) {
            window.location.reload()
        }
    });

    $(window).scroll(function() {
        if ($(this).scrollTop() > 1) {
            $("#nav-hidden").stop().fadeIn(250)
        } else {
            $("#nav-hidden").stop().fadeOut(250)
        }
    });

    function watchForHover() {
        var hasHoverClass = false;
        var container = document.body;
        var lastTouchTime = 0;

        function enableHover() {
            if (new Date() - lastTouchTime < 500) return;
            if (hasHoverClass) return;
            container.className += ' hasHover';
            hasHoverClass = true;
        }

        function disableHover() {
            if (!hasHoverClass) return;
            container.className = container.className.replace(' hasHover', '');
            hasHoverClass = false;
        }

        function updateLastTouchTime() {
            lastTouchTime = new Date();
        }

        document.addEventListener('touchstart', updateLastTouchTime, true);
        document.addEventListener('touchstart', disableHover, true);
        document.addEventListener('mousemove', enableHover, true);
        enableHover();
    }

    watchForHover();

</script>

@endsection

@section('footer_sc')
    <div class="footer-icons">
        <a href="#about-wrapper"><img src="{{URL::to('storage/app/public/Frontassets/media/top.svg')}}"></a>
    </div>
@endsection



