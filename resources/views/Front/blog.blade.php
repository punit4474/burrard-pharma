@extends('layouts.front')
@section('front_title')
    Blogs
@endsection
@section('meta')
    <link rel="canonical" href="http://www.burrardpharma.com/"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Title"/>
    <meta property="og:description" content="Description"/>
    <meta property="og:url" content="http://www.burrardpharma.com/"/>
    <meta property="og:site_name" content="Burrard Pharma"/>
    <meta name="description" content="Description"/>
    <meta name="twitter:title" content="Title">
    <meta name="twitter:description" content="Description">
@endsection
@section('front_css')

@endsection
@section('front_content')
    <div id="blog-wrapper">
        <div class="blog-container">
            @forelse($data as $row)
                <div class="blog-post">
                    <div class="blog-img">
                        <a href="{{URL::to('press/'.$row->slug)}}"><img src="{{URL::to('storage/app/public/blog/'.$row->image)}}"></a>
                    </div>
                    <div class="blog-title">
                        <h1>{{$row->title}}</h1>
                    </div>
                    <div class="blog-text">
                        <p>{!! substr($row->description,0,200) !!}</p>
                    </div>
                    <button class="blog-btn">
                        <a href="{{URL::to('press/'.$row->slug)}}">read more</a>
                    </button>
                </div>
            @empty
            @endforelse
        </div>
    </div>
@endsection
@section('front_js')
    <script src="{{URL::to('storage/app/public/Frontassets/js/index.js')}}"></script>

    <script src="{{URL::to('storage/app/public/Frontassets/js/objectFitPolyfill.min.js')}}"></script>
    <script>
        // Back To Top

        $(document).ready(function () {
            $('a[href^="#"]').click(function (event) {
                var id = $(this).attr("href");
                var offset = 0;
                var target = $(id).offset().top - offset;
                $('html, body').animate({scrollTop: target}, 500);
                event.preventDefault();
            });
        });


        // Pageload Delay

        function delay (URL) {
            setTimeout( function() { window.location = URL }, 1700 );
        }
        $(window).bind("pageshow", function(event) {
            if (event.originalEvent.persisted) {
                window.location.reload()
            }
        });

        $(window).scroll(function() {
            if ($(this).scrollTop() > 1) {
                $("#nav-hidden").stop().fadeIn(250)
            } else {
                $("#nav-hidden").stop().fadeOut(250)
            }
        });

        function watchForHover() {
            var hasHoverClass = false;
            var container = document.body;
            var lastTouchTime = 0;

            function enableHover() {
                if (new Date() - lastTouchTime < 500) return;
                if (hasHoverClass) return;
                container.className += ' hasHover';
                hasHoverClass = true;
            }

            function disableHover() {
                if (!hasHoverClass) return;
                container.className = container.className.replace(' hasHover', '');
                hasHoverClass = false;
            }

            function updateLastTouchTime() {
                lastTouchTime = new Date();
            }

            document.addEventListener('touchstart', updateLastTouchTime, true);
            document.addEventListener('touchstart', disableHover, true);
            document.addEventListener('mousemove', enableHover, true);
            enableHover();
        }

        watchForHover();

    </script>
@endsection
@section('footer_sc')
    <div class="footer-icons">
        <a href="#blog-wrapper"><img src="{{URL::to('storage/app/public/Frontassets/media/top.svg')}}"></a>
    </div>
@endsection

