@extends('layouts.front')
@section('front_title')
    Register
@endsection
@section('front_css')
    <style>
        .social_image {
            height: 100%;
            width: 100%;
            max-width: 50px;
            margin-top: 20px;
            text-align: center;
        }

        .register-generator-btn a:hover, .register-generator-btn a {
            color: aliceblue;
        }

        .is-invalid {
            border: red 1px solid;
        }

        .invalid-feedback {
            color: red;
            font-family: 'SofiaPro-regular';
        }

        .invalid-feedback strong {
            position: absolute;
            top: 60px;
            left: 15px;
            font-weight: 100;
        }

        .org:after {
            content: none;
        }

        #loader {
            border: 12px solid #00c0ff;
            border-radius: 50%;
            border-top: 12px solid #00c0ff;
            width: 70px;
            height: 70px;
            animation: spin 1s linear infinite;
            z-index: 9999999999999999;
        }
        @keyframes spin {
            100% {
                transform: rotate(360deg);
            }
        }

        .center {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: auto;
        }

        ::-webkit-scrollbar {
            -webkit-appearance: none;
            margin-right: 2px
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 4px;
            margin: 3px;
            background-color: rgb(0 192 255);
            box-shadow: 0 0 1px rgba(255, 255, 255, .5);
        }
    </style>
@endsection
@section('front_content')
    <div id="loader" class="center" style="display: none;"></div>
    <div class="register-wrapper">
        <div class="register-container">

            <div class="register-content">
                <div class="register-content-box">
                    <h1>Registration takes less than a minute but it gives you full control over our generator.</h1>
                </div>
                <div class="register-generator-btn"><a href="{{URL::to('/generator')}}" style="text-decoration: none;"
                                                       class="gen">Generator</a></div>
            </div>
            <div class="register-content">
                <div class="row">
                    <div class="col-md-12 session">
                        @if(Session::has('message'))
                            {!! Session::get('message') !!}
                        @endif
                    </div>
                </div>
                <div id="tabs">
                    <button class="tab tab__active" data-id="signup">Sign up</button>
                    <button class="tab" data-id="signin">Sign in</button>
                </div>
                <div class="tabsContent">
                    <div class="tabContent show">
                        {{Form::open(array('method'=>'post','name'=>'signUp','id'=>'signUp','class'=>'form'))}}

                        <div class="form__row form__row_2">
                            <input class="form__input form__input_name"
                                   name="first_name" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,15}$" required=""
                                   title="Please Enter letter or number, max symbols 15" type="text"> <label
                                class="form__placeholder" for="name">First Name</label>

                            <span class="invalid-feedback first_name_err" role="alert" style="display: none">
                                    <strong>First Name is required</strong>
                                </span>

                        </div>
                        <div class="form__row form__row_2">
                            <input class="form__input form__input_lastName @error('last_name') is-invalid @enderror"
                                   name="last_name" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,15}$" required=""
                                   title="Please Enter letter or number, max symbols 15" type="text"> <label
                                class="form__placeholder" for="lastName">Last Name</label>
                            <span class="invalid-feedback last_name_err" role="alert" style="display: none">
                                    <strong>Last Name is required</strong>
                                </span>
                        </div>
                        <div class="form__row form__row_2">
                            <input class="form__input form__input_email @error('email') is-invalid @enderror"
                                   name="email"
                                   pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
                                   required="" type="email"> <label class="form__placeholder" for="email"
                                                                    title="example@example.com">Email Address</label>
                            <span class="invalid-feedback email_err" role="alert" style="display: none">
                                    <strong>Last Name is required</strong>
                                </span>
                        </div>
                        <div class="form__row form__row_2">
                            <input class="form__input form__input_pass @error('password') is-invalid @enderror"
                                   name="password"
                                   pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
                                   required=""
                                   title="Password (UpperCase, LowerCase, Number/SpecialChar and min 8 Chars)"
                                   type="password"> <label class="form__placeholder" for="pass">Password</label>
                            <span class="invalid-feedback password_err" role="alert" style="display: none">
                                    <strong>Password is required</strong>
                                </span>
                        </div>
                        <div class="form__row form__row_2">
                            <input
                                class="form__input form__input_name organization @error('organization') is-invalid @enderror"
                                name="organization" pattern="^[a-zA-Z][a-zA-Z0-9-_\. ]{1,15}$" required=""
                                title="Please Enter letter or number, max symbols 15" type="text"> <label
                                class="form__placeholder org" for="organization">Organization</label>
                            <span class="invalid-feedback organization_err" role="alert" style="display: none">
                                    <strong>Organization is required</strong>
                                </span>
                        </div>
                        <div class="form__row form__row_2">
                            <input
                                class="form__input form__input_lastName phone_number @error('phone_number') is-invalid @enderror"
                                name="phone_number" pattern="^[0-9]{1,15}$" required=""
                                title="Please Enternumber, max symbols 15" type="text"> <label
                                class="form__placeholder" for="phone_number">Phone Number</label>
                            <span class="invalid-feedback phone_number_err" role="alert" style="display: none">
                                    <strong>Phone Number is required</strong>
                                </span>
                        </div>
                        <input type="hidden" name="redirect" class="reverse">
                        <input class="form__btn tab register" type="button" value="Register">

                        <a class="social-register-btn linkedin" href="#"><img
                                src="{{URL::to('storage/app/public/Frontassets/media/linkedin.svg')}}">Sign up</a>
                        <a class="social-register-btn google" href="#"><img
                                src="{{URL::to('storage/app/public/Frontassets/media/google-icon.svg')}}">Sign up</a>
                        {{Form::close()}}
                    </div>

                    <div class="tabContent">
                        {{Form::open(array('method'=>'post','name'=>'logIn','id'=>'logIn','class'=>'form'))}}

                        <div class="form__row form__row_first">
                            <input class="form__input form__input_email lg_email "
                                   name="email"
                                   pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
                                   required="" type="email"> <label class="form__placeholder" for="email"
                                                                    title="example@example.com">Email Address</label>
                            <span class="invalid-feedback e_err" role="alert" style="display: none">
                            <strong>Last Name is required</strong>
                        </span>
                        </div>
                        <div class="form__row">
                            <input class="form__input form__input_pass lg_pass"
                                   name="password"

                                   required=""
                                   title="Password (UpperCase, LowerCase, Number/SpecialChar and min 8 Chars)"
                                   type="password"> <label class="form__placeholder" for="pass">Password</label>
                            <span class="invalid-feedback pass_err" role="alert" style="display: none">
                                <strong>Password is required</strong>
                            </span>
                        </div>
                        <div class="forgot-password">Forgot password?</div>
                        <input class="form__btn tab login" type="button" value="Sign in">
                        <input type="hidden" name="redirect" class="reverse">

                        <a class="social-register-btn " href="{{URL::to('/auth/linkedin')}}"><img
                                src="{{URL::to('storage/app/public/Frontassets/media/linkedin.svg')}}">Sign in</a>
                        <a class="social-register-btn " href="{{URL::to('/auth/google')}}"><img
                                src="{{URL::to('storage/app/public/Frontassets/media/google-icon.svg')}}">Sign in</a>
                        {{Form::close()}}
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="pop-up-wrapper" id="pop-up-wrapper">
        <div class="pop-up-container">
            <div class="pop-up-content">
                {{Form::open(array('url'=>'forgot-password','method'=>'post','name'=>'forgot-password','id'=>'forgot-password','class'=>'form'))}}

                <div class="title-forgot"><h1>Please type in your E-mail address and click change password</h1></div>
                <input class="email-forgot" type="email" id="email" name="email" placeholder="email@exacmple.com"
                       required
                       pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$">
                <button class="close-forgot" type="submit">Close</button>
                <button class="send-forgot" type="submit">Forget Password</button>
                {{Form::close()}}
            </div>
        </div>
    </div>
    <div class="pop-up-wrapper" id="register-modal">
        <div class="pop-up-container">
            <div class="pop-up-content">
                <div id="terms-container">
                    <div class="terms-container">

                        <h2 style="text-align: center">BURRARD PHARMA</h2>
                        <h2 style="text-align: center">TERMS AND CONDITIONS OF USE</h2>
                        <p>These Terms and Conditions were last updated on: September 15, 2020</p>
                        <h4>1. ACCEPTANCE OF THE TERMS AND CONDITIONS</h4>
                        <p>1.1 &nbsp;These website terms and conditions of use for burrardpharma.com (the <b>“Website”</b>),
                            constitute a legal
                            agreement and are entered into by and between you and Burrard Pharmaceuticals Enterprises Ltd. (referred
                            to herein as <b>“Burrard Pharma”</b>, <b>“we”</b>, <b>“us”</b>, <b>“our”</b>). The following terms and
                            conditions (these <b>“Terms and
                                Conditions”</b>), govern your access to and use of our Website, including any content, functionality
                            and
                            services offered on or through our Website (the <b>“Service”</b>).
                        </p>
                        <p><b>BY CLICKING TO ACCEPT THE TERMS AND CONDITIONS, YOU ACCEPT AND AGREE TO BE BOUND AND COMPLY WITH THESE
                                TERMS AND CONDITIONS AND OUR PRIVACY POLICY, FOUND AT <a
                                    href="{{URL::to('privacy-policy')}}">{{URL::to('privacy-policy')}}</a>. IF
                                YOU DO NOT AGREE TO THESE TERMS AND CONDITIONS OR THE PRIVACY POLICY, YOU MUST NOT ACCESS OR USE THE
                                WEBSITE.</b></p>
                        <p>1.3 &nbsp; By using the Website, you represent and warrant that you are the legal age of majority under
                            applicable law to form a binding contract with Burrard Pharma and meet all of the foregoing eligibility
                            requirements. If you do not meet all of these requirements, you must not access or use the Website.
                        </p>
                        <h4>2. MODIFICATIONS TO THE TERMS AND CONDITIONS AND TO THE WEBSITE</h4>
                        <p>2.1 &nbsp; We reserve the right in our sole discretion to revise and update these terms and conditions
                            from time to time. Any and all such modifications are effective immediately upon posting and apply to
                            all access to and continued use of the Website. You agree to periodically review the terms and
                            conditions in order to be aware of such modifications and your continued use shall be your acceptance of
                            these. Notwithstanding this Section 2.1, if we make material changes to how we treat our user’s personal
                            information under our Privacy Policy, we will notify you of any changes to our Privacy Policy or to
                            these Terms and Conditions, as applicable, by email to the primary email address specified in your
                            account, and through a notice on our Website home page.
                        </p>
                        <p>2.2 &nbsp;The information and material on this Website may be changed, withdrawn, or terminated at any
                            time in our sole discretion and without notice. We will not be liable if, for any reason, all or any
                            part of the Website is restricted to users or unavailable at any time or for any period.
                        </p>
                        <h4>3. USING THE BURRARD PHARMA SERVICES, SERVICE FEE AND ANNUAL SUBSCRIPTION FEE</h4>
                        <p>3.1 &nbsp;You acknowledge and agree that these Terms and Conditions apply to you when you visit the
                            Website, irrespective of registering an account and subscribing to use our Services. Should you create
                            an account to use our Services through our login and registration page on our Website, you will have
                            access to various Services we offer, including the Drug Formulation Services, the Know-how Services, the
                            Drug Manufacturing Services and the Technology Transfer Services more particularly described on our
                            Website, in accordance with the pricing set out on our Website (the <b>“Service Fee”</b>).
                        </p>
                        <p>3.2 &nbsp;Further and for greater clarity, in addition to the Service Fee as set out in our Website, you
                            will also pay to Burrard Pharma a $250.00 annual subscription fee for your use of the Services (the
                            <b>“Annual Subscription Fee”</b>). The Annual Subscription Fee shall be paid upon the date of purchase
                            of the
                            Services.</p>
                        <p>3.3 &nbsp;You acknowledge and agree that the Service Fee and the Annual Fee shall be paid by credit card
                            through our third party payment processor, Stripe or PayPal. You further acknowledge and agree that the
                            Service Fee and the Annual Subscription Fee are non-refundable.
                        </p>
                        <h4>4. PAYMENT PROCESSING</h4>
                        <p>4.1 &nbsp;Unless otherwise stated, all fees (including the Service Fee and the Annual Subscription Fee)
                            are quoted in US Dollars. If your payment method fails or your account is past due, we may collect
                            amounts by retaining collection agencies and legal counsel, suspend or limit your account.
                        </p>
                        <p>4.2 &nbsp;You must provide us with a valid credit card or other form of electronic payment (such as
                            PayPal or Stripe). We will automatically charge you based on the Services you purchase. We will share
                            your account information with financial institutions and payment processing companies, including your
                            submitted payment information, to process your purchase.
                        </p>
                        <p>4.3 &nbsp;Prices for the Services are subject to change on 30 days’ notice, provided that no price change
                            will apply during your then-current subscription term with respect to your Annual Subscription Fee.
                            Depending on where you reside, foreign exchange fees or differences in price may apply, including
                            because of exchange rates. We do not support all payment methods, currencies, or locations of payment.
                            If the payment method you use is no longer valid (such as your credit card has expired) and you do not
                            edit your payment information, you authorize us to continue to bill you for our Annual Subscription Fee
                            and you remain responsible for all uncollected amounts with respect to the Annual Subscription Fee and
                            the Service Fee, as applicable.
                        </p>
                        <h4>5. YOUR USE OF THE WEBSITE AND ACCOUNT-SET UP AND SECURITY</h4>
                        <p>5.1 &nbsp;The security of your personal information is very important to us. We use physical, electronic,
                            and administrative measures designed to secure your personal information from accidental loss and from
                            unauthorized access, use, alteration, and disclosure.
                        </p>
                        <p>5.2 &nbsp;The safety and security of your information also depends on you. Users are responsible for
                            obtaining their own access to the Website. The Website, including content or areas of the Website, may
                            require user registration. It is a condition of your use of the Website that all the information you
                            provide on the Website is correct, current and complete.
                        </p>
                        <p>5.3 &nbsp;You recognize that the transmission of information via the Internet is not completely secure.
                            We do not guarantee the security of your personal information transmitted to our Website. Any personal
                            information provided is at your own risk. We are not responsible for circumvention of any privacy
                            settings or security measures contained on our Website.
                        </p>
                        <p>5.4 &nbsp;Your provision of registration information and any submissions you make to the Website through
                            any functionality such as applications or e-mail (the <b>“Interactive Functions”</b>) constitutes your
                            consent
                            to all actions we take with respect to such information consistent with our Privacy Policy found at
                            <a href="{{URL::to('privacy-policy')}}">{{URL::to('privacy-policy')}}</a>.
                        </p>
                        <p>5.5 &nbsp;Any username, password, or any other piece of information chosen by you, or provided to you as
                            part of our security procedures, must be treated as confidential, and you must not disclose it to any
                            other person or entity. You must exercise caution when accessing your account from a public or shared
                            computer so that others are not able to view or record your password or other personal information. You
                            understand and agree that should you be provided an account, your account is personal to you and you
                            agree not to provide any other person with access to the Website or portions of it using your username,
                            password, or other security information. You agree to notify us immediately of any unauthorized access
                            to or use of your username or password or any other breach of security. You also agree to ensure that
                            you logout of your account at the end of each session. You are responsible for any password misuse or
                            any unauthorized access.</p>
                        <p>5.6 &nbsp;We reserve the right at any time and from time to time, to disable or terminate your account,
                            any username, password, or other identifier, whether chosen by you or provided by us, in our sole
                            discretion for any or no reason, including any violation of any provision of these Terms and Conditions.
                        </p>
                        <p>5.7 &nbsp;You are prohibited from attempting to circumvent and from violating the security of this
                            Website, including without limitation: (a) accessing content and data that is not intended for you; (b)
                            attempting to breach or breaching the security and/or authentication measures which are not authorized;
                            (c) restricting, disrupting, or disabling service to users, hosts, servers, or networks; (d) disrupting
                            network services and otherwise disrupting the Website owner’s ability to monitor the Website; (e) using
                            any robot, spider, or other automatic device, process, or means to access the Website for any purpose,
                            including monitoring or copying any of the material on the Website; (f) introducing any viruses, Trojan
                            horses, worms, logic bombs, or other material that is malicious or technologically harmful; (g)
                            attacking the Website via a denial of service attack, distributed denial of service attack, flooding,
                            mail bombing, or crashing; and (h) otherwise attempting to interfere with the proper working of the
                            Website or the Services in any way.
                        </p>
                        <h4>6. INTELLECTUAL PROPERTY RIGHTS AND OWNERSHIP</h4>
                        <p>6.1 &nbsp;The term <b>“Burrard Pharma IP”</b> means all intellectual property rights in and to the
                            Services (including any object code or source code related thereto), any and all documentation related
                            thereto, and all registered and unregistered rights granted, applied for, or otherwise now or hereafter
                            in existence under or related to any patent, copyright, trademark, trade secret, data base protection,
                            or other intellectual property provided to you in connection with the foregoing. For the avoidance of
                            doubt, Burrard Pharma IP includes any information, data, or other content derived from Burrard Pharma
                            monitoring your access to or use of the Services.
                        </p>
                        <p>6.2 &nbsp;Without limiting Section 6.1, you understand and agree that the Website and its entire
                            contents, features, and functionality, including but not limited to, all information, software, code,
                            text, displays, graphics, photographs, video, audio, design, presentation, selection, and arrangement,
                            are owned by Burrard Pharma, its licensors, or other providers of such material and are protected in all
                            forms by intellectual property laws including without limitation, copyright, trademark, patent, trade
                            secrets, and any other proprietary rights. By accessing the Website or purchasing the Services, you do
                            not and will not own the Burrard Pharma IP in any way whatsoever. </p>
                        <p>6.3 &nbsp;If you send or transmit any communications or materials to Burrard Pharma by mail, email,
                            telephone or otherwise, suggesting or recommending changes to the Website, including without limitation,
                            new features or functionality relating thereto, or any comments, questions, suggestions, or the like
                            (<b>“Feedback”</b>), Burrard Pharma is free to use such Feedback. You hereby assign to Burrard Pharma on
                            your
                            behalf, and on behalf of your employees, contractors, and/or agents, as applicable, all right, title,
                            and interest in, and Burrard Pharma is free to use, without any attribution or compensation to any
                            party, any ideas, know-how, concepts, techniques, or other intellectual property rights contained in the
                            Feedback, for any purpose whatsoever, provided that Burrard Pharma is not required to use any Feedback.
                        </p>
                        <p>6.4 &nbsp;Without limiting Section 6.1, our name, and all related names, logos, product and service
                            names, designs, images, and slogans are trademarks owned by us, our affiliates, or our licensors, as
                            applicable. For greater certainty, our trademarks include, without limitation:
                        </p>
                        <p class="inner">
                            (a) &nbsp;Burrard Pharmaceuticals
                        </p>
                        <p class="inner">
                            (b) &nbsp;Burrard Pharma
                        </p>
                        <p class="inner">
                            (c) &nbsp;
                            <a class="logo" href="#" style="">
                                <svg fill="currentcolor" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 250 36"
                                     style="    margin-left: 35px;enable-background: new 0 0 250 36;height: 100px;width: 300px;margin-top: -55px;"
                                     xml:space="preserve">
            <g>
                <path d="M231.95,18c0-4.99,4.04-9,9.02-9c4.99,0,9.02,4.01,9.02,9s-4.04,9-9.02,9C235.99,27,231.95,22.99,231.95,18z M249.33,18
            c0-4.66-3.74-8.4-8.35-8.4s-8.37,3.74-8.37,8.4s3.76,8.4,8.37,8.4S249.33,22.66,249.33,18z M236.22,18c0-2.67,2.07-4.79,4.84-4.79
            c1.79,0,3.29,0.92,4.06,2.32l-1.05,0.6c-0.55-1.12-1.75-1.74-3.02-1.74c-2.17,0-3.59,1.7-3.59,3.62s1.42,3.61,3.59,3.61
            c1.3,0,2.49-0.62,3.02-1.77l1.07,0.62c-0.77,1.4-2.29,2.32-4.09,2.32C238.28,22.79,236.22,20.67,236.22,18z"/>
                <g>
                    <path d="M130.92,26.71h4.5V16.5c0-1.66,1.35-3,3-3c1.66,0,3,1.34,3,3v10.21h4.5V16.5c0-4.14-3.36-7.5-7.5-7.5
            c-1.07,0-2.08,0.23-3,0.64V0h-4.5V26.71z"/>
                    <path d="M198.21,9c-2.04,0-3.9,0.83-5.25,2.16c-1.35-1.33-3.2-2.16-5.25-2.16c-4.14,0-7.5,3.36-7.5,7.5v10.21h4.5V16.5
            c0-1.66,1.35-3,3-3c1.66,0,3,1.34,3,3v10.21h4.5V16.5c0-1.66,1.34-3,3-3s3,1.34,3,3v10.21h4.5V16.5
            C205.71,12.36,202.35,9,198.21,9z"/>
                    <path
                        d="M169.72,15.99v10.73h4.5V15.99c0-1.37,1.12-2.49,2.49-2.49H178V9h-1.29C172.85,9,169.72,12.13,169.72,15.99z"/>
                    <path d="M128.21,18c0-4.96-4.04-9-9-9s-9,4.04-9,9v18h4.5V25.79c1.32,0.77,2.86,1.21,4.5,1.21C124.18,27,128.21,22.96,128.21,18z
            M123.71,18c0,2.48-2.02,4.5-4.5,4.5s-4.5-2.02-4.5-4.5s2.02-4.5,4.5-4.5C121.7,13.5,123.71,15.52,123.71,18z"/>
                    <path d="M161.71,26.7h4.5v-8.71c0-4.96-4.04-8.99-9-8.99s-9,4.03-9,8.99s4.04,9.25,9,9c2.08-0.1,3.53-0.96,4.5-2.17V26.7z
            M157.21,22.49c-2.48,0-4.5-2.02-4.5-4.5s2.02-4.5,4.5-4.5s4.5,2.02,4.5,4.5S159.69,22.49,157.21,22.49z"/>
                    <path d="M221.5,26.7h4.5v-8.71c0-4.96-4.04-8.99-9-8.99s-9,4.03-9,8.99s4.04,9.25,9,9c2.08-0.1,3.53-0.96,4.5-2.17V26.7z
            M217,22.49c-2.48,0-4.5-2.02-4.5-4.5s2.02-4.5,4.5-4.5s4.5,2.02,4.5,4.5S219.48,22.49,217,22.49z"/>
                    <path
                        d="M30.82,19.5c0,1.66-1.35,3-3,3s-3-1.34-3-3V9.29h-4.5V19.5c0,4.14,3.36,7.5,7.5,7.5s7.5-3.36,7.5-7.5V9.29h-4.5V19.5z"/>
                    <path
                        d="M38.68,15.99v10.73h4.5V15.99c0-1.37,1.11-2.49,2.49-2.49h1.29V9h-1.29C41.82,9,38.68,12.13,38.68,15.99z"/>
                    <path
                        d="M49.18,15.99v10.73h4.5V15.99c0-1.37,1.11-2.49,2.49-2.49h1.29V9h-1.29C52.31,9,49.18,12.13,49.18,15.99z"/>
                    <path
                        d="M79.79,15.99v10.73h4.5V15.99c0-1.37,1.11-2.49,2.48-2.49h1.29V9h-1.29C82.92,9,79.79,12.13,79.79,15.99z"/>
                    <path d="M71.78,26.7h4.5v-8.71c0-4.96-4.04-8.99-9-8.99s-9,4.03-9,8.99s4.04,9.25,9,9c2.08-0.1,3.53-0.96,4.5-2.17V26.7z
            M67.28,22.49c-2.48,0-4.5-2.02-4.5-4.5s2.02-4.5,4.5-4.5s4.5,2.02,4.5,4.5S69.76,22.49,67.28,22.49z"/>
                    <path d="M9,9c-1.64,0-3.18,0.44-4.5,1.21V0H0v18c0,4.96,4.04,9,9,9s9-4.04,9-9S13.96,9,9,9z M9,22.5c-2.48,0-4.5-2.02-4.5-4.5
            s2.02-4.5,4.5-4.5s4.5,2.02,4.5,4.5S11.48,22.5,9,22.5z"/>
                    <path d="M88.88,18c0,4.96,4.04,9,9,9s9-4.04,9-9V0h-4.5v10.21C101.06,9.44,99.52,9,97.88,9C92.92,9,88.88,13.04,88.88,18z
            M93.38,18c0-2.48,2.02-4.5,4.5-4.5s4.5,2.02,4.5,4.5s-2.02,4.5-4.5,4.5C95.4,22.5,93.38,20.48,93.38,18z"/>
                </g>
            </g>
            </svg>
                            </a>
                            (collectively, the “Burrard Pharma Trademarks”)<br/><br/>
                            You must not use the Burrard Pharma Trademarks, without our authorization in writing, in any way
                            whatsoever. Your unauthorized use or display of the Burrard Pharma Trademarks shall constitute an
                            infringement or violation of our intellectual property rights.
                        </p>
                        <p>6.5 &nbsp;Other names, trademarks, logos, product and service names, designs, images, and slogans
                            mentioned, or which appear on this Website are the trademarks of their respective owners. Use of any
                            such property, except as expressly authorized, shall constitute an infringement or violation of the
                            rights of the property owner and may be a violation of federal or other laws and could subject the
                            infringer to legal action.
                        </p>
                        <p>6.6 &nbsp;You may only use the Website for your personal and non-commercial use. You shall not directly
                            or indirectly reproduce, compile for an internal database, distribute, modify, create derivative works
                            of, publicly display, publicly perform, republish, download, store, or transmit any of the material on
                            our Website, in any form or medium whatsoever.
                        </p>
                        <p>6.7 &nbsp;If you print, copy or download any part of our Website in breach of these Terms and Conditions,
                            your right to use the Website will cease immediately and you must, at our option, return or destroy any
                            copies of the materials you have made. You have no right, title, or interest in or to the Website or to
                            any content on the Website, and all rights not expressly granted are reserved by us. Any use of the
                            Website not expressly permitted by these Terms and Conditions is a breach of these Terms and Conditions
                            and may infringe or violate copyright, trademark, and other intellectual property and other proprietary
                            laws.
                        </p>
                        <p>6.8 &nbsp;For greater certainty and without limiting the generality of the foregoing, nothing contained
                            in these Terms and Conditions will be construed as an assignment to you of any Burrard Pharma IP,
                            including without limitation, any patent, copyright, trade secret, trademark or any other intellectual
                            property rights in or to the Services. All of the Services and the data and technology therein will
                            remain the sole and exclusive property of Burrard Pharma.
                        </p>
                        <h4>7. CONDITIONS OF USE AND USER SUBMISSIONS AND SITE CONTENT STANDARDS</h4>
                        <p>7.1 &nbsp;As a condition of your access and use, you agree that you may use the Website only for lawful
                            purposes and in accordance with these Terms and Conditions.</p>
                        <p>7.2 &nbsp;The following content standards apply to any and all content, material, and information a user
                            submits, posts, publishes, displays or transmits (collectively, <b>“submit”</b>) to the Website, to
                            other users
                            or other persons (collectively, <b>“User Submissions”</b>) and any and all Interactive Functions. Any
                            and all
                            User Submissions must comply with all applicable federal, provincial, local, and international laws,
                            regulations, and terms of service.
                        </p>
                        <p>7.3 &nbsp;Without limiting the foregoing, you warrant and agree that your use of the Website and any User
                            Submissions shall not:
                        </p>
                        <p class="inner">
                            (a) &nbsp;in any manner violate any applicable federal, provincial, local, or international law or
                            regulation
                            including, without limitation, any laws regarding the export of data or software, patent, trademark,
                            trade secret, copyright, or other intellectual property, legal rights (including the rights of publicity
                            and privacy of others) or contain any material that could give rise to any civil or criminal liability
                            under applicable laws or regulations or that otherwise may be in conflict with these Terms and
                            Conditions and our Privacy Policy found at <a
                                href="{{URL::to('privacy-policy')}}">{{URL::to('privacy-policy')}}</a>;
                        </p>
                        <p class="inner">(b) &nbsp;in any manner violate the terms of use of any third-party website that is linked
                            to the Website;</p>
                        <p class="inner">(c) &nbsp;include or contain any material that is exploitive, obscene, harmful,
                            threatening, abusive, harassing, hateful, defamatory, violent, inflammatory, or discriminatory based on
                            race, sex, religion, nationality, disability, sexual orientation, or age or other such legally
                            prohibited ground or be otherwise objectionable, such determination to be made in our sole discretion;
                        </p>
                        <p class="inner">(d) &nbsp;involve attempting to exploit or harm any individual (including minors) in any
                            way or by seeking the personal information of other individuals as prohibited under applicable laws,
                            regulations, or code;
                        </p>
                        <p class="inner">(e) &nbsp;provide or contribute any false, inaccurate or misleading information;
                        </p>
                        <p class="inner">(f) &nbsp;impersonate or attempt to impersonate Burrard Pharma, a Burrard Pharma employee,
                            another user, or any other person or entity (including without limitation, by using email addresses, or
                            screen names associated with any of the foregoing);
                        </p>
                        <p class="inner">(g) &nbsp;transmit, or procure the sending of, any advertisements or promotions, sales, or
                            encourage any other commercial activities, including without limitation, any “spam”, “junk mail”, “chain
                            letter”, contests, sweepstakes and other sales promotions, barter, or advertising or any other similar
                            solicitation;
                        </p>
                        <p class="inner">(h) &nbsp;encourage any other conduct that restricts or inhibits anyone’s use or enjoyment
                            of the Website, or which, as determined by us, may harm Burrard Pharma or users of the Website or expose
                            us or them to liability;
                        </p>
                        <p class="inner">(i) &nbsp;cause annoyance, inconvenience, or needless anxiety or be likely to upset,
                            embarrass, or alarm any other person; or
                        </p>
                        <p class="inner">(j) &nbsp;promote any illegal activity, or advocate, promote, or assist any unlawful act.
                        </p>
                        <h4>8. USER SUBMISSIONS: GRANT OF LICENSE</h4>
                        <p>8.1 &nbsp;The Website may contain Interactive Functions allowing User Submissions on or through the
                            Website. You acknowledge that the User Submissions you submit to the Website will not be subject to any
                            confidentiality obligations by Burrard Pharma.
                        </p>
                        <p>8.2 &nbsp;By submitting the User Submissions, you declare and warrant that you own or have the necessary
                            rights to submit the User Submissions and that all User Submissions comply with applicable laws and
                            regulations.

                        </p>
                        <p>8.3 &nbsp;You understand and agree that you, not Burrard Pharma nor Burrard Pharma’s parent,
                            subsidiaries, affiliates nor their respective directors, officers, employees, agents, service providers,
                            contractors, licensors, suppliers or successors, are fully responsible for any User Submissions you
                            submit or contribute, and you are fully responsible and legally liable, including to any third party,
                            for such content and its accuracy. We are not responsible or legally liable to any third party for the
                            content or accuracy of any User Submissions submitted by you or any other use of the Website.

                        </p>
                        <h4>9. SITE MONITORING AND ENFORCEMENT, SUSPENSION, AND TERMINATION</h4>
                        <p>9.1 &nbsp;Burrard Pharma has the right, without provision of notice to:
                        </p>
                        <p class="inner">(a) &nbsp;remove or refuse to provide the Services to any user;</p>
                        <p class="inner">(b) &nbsp;at all times, take such actions with respect to any User Submission deemed
                            necessary or appropriate in our sole discretion, including, without limitation, for violating the
                            Website and User Submissions or these Terms and Conditions;
                        </p>
                        <p class="inner">(c) &nbsp;take appropriate legal action, including without limitation, referral to law
                            enforcement or regulatory authority, or notifying the harmed party of any illegal or unauthorized use of
                            the Website. Without limiting the foregoing, Burrard Pharma has the right to fully cooperate with any
                            law enforcement authorities or court order requesting or directing us to disclose the identity or other
                            information of any user posting any materials on or through the Website; and
                        </p>
                        <p class="inner">(d) &nbsp;terminate or suspend your access to all or parts of the Website for any reason or
                            no reason, including without limitation, any violation of these Terms and Conditions.
                        </p>
                        <p>9.2 &nbsp;YOU WAIVE AND HOLD HARMLESS BURRARD PHARMA AND ITS PARENT, SUBSIDIARIES, AFFILIATES, AND THEIR
                            RESPECTIVE DIRECTORS, OFFICERS, EMPLOYEES, AGENTS, SERVICE PROVIDERS, CONTRACTORS, LICENSORS, LICENSEES,
                            SUPPLIERS, AND SUCCESSORS FROM ANY AND ALL CLAIMS RESULTING FROM ANY ACTION TAKEN BY BURRARD PHARMA AND
                            ANY OF THE FOREGOING PARTIES RELATING TO ANY, INVESTIGATIONS BY EITHER BURRARD PHARMA OR SUCH PARTIES OR
                            BY LAW ENFORCEMENT AUTHORITIES.
                        </p>
                        <p>9.3 &nbsp;We have no obligation, nor any responsibility to any party to monitor the Website or its use,
                            and do not and cannot undertake to review material that you or other users submit to the Website. We
                            cannot ensure prompt removal or objectionable material after it has been posted and we have no liability
                            for any action or inaction regarding transmissions, communications, or content provided by any user or
                            third party, subject to applicable laws.
                        </p>
                        <h4>10. PRIVACY</h4>
                        <p>10.1 &nbsp;By submitting your personal information and using our Website, you consent to the collection,
                            use, reproduction, hosting, transmission, and disclosure of any such user content submissions in
                            compliance with our Privacy Policy found at <a
                                href="{{URL::to('privacy-policy')}}">{{URL::to('privacy-policy')}}</a>, as we deem
                            necessary for the use of the Website and the provision of the Services.
                        </p>
                        <h4>11. THIRD-PARTY WEBSITES</h4>
                        <p>11.1 &nbsp;For your convenience, this Website may provide links or pointers to third-party sites. We make
                            no representations or warranties about any other websites or service providers that may be accessed from
                            this Website. If you choose to access any such sites, you do so at your own risk. We have no control
                            over the contents of any such third-party sites and accept no responsibility for such sites or for any
                            loss or damage that may arise from your use of same. You acknowledge and agree that you are subject to
                            any terms and conditions and privacy policies of such third party sites at all times.
                        </p>
                        <h4>12. GEOGRAPHIC RESTRICTIONS</h4>
                        <p>12.1 &nbsp;The owner of the Website is based in the Province of British Columbia, in Canada. If you
                            access the Website from outside Canada, you do so at your own risk and you are responsible for
                            compliance with local laws of your jurisdiction.
                        </p>
                        <h4>13. DISCLAIMER OF WARRANTIES</h4>
                        <p>13.1 &nbsp;YOU UNDERSTAND AND AGREE THAT YOUR USE OF THE WEBSITE, ITS CONTENT, AND ANY SERVICES OR ITEMS
                            FOUND OR ATTAINED THROUGH THE WEBSITE (INCLUDING WITHOUT LIMITATION, THE SERVICES PURCHASED BY YOU
                            THROUGH OUR WEBSITE AND THE DOCUMENTS RELATED TO THE SERVICES PROVIDED TO YOU IN RELATION TO SAME) IS AT
                            YOUR OWN RISK. THE WEBSITE, ITS CONTENT AND THE SERVICES OR ITEMS FOUND OR ATTAINED THROUGH THE WEBSITE
                            ARE PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS, WITHOUT ANY WARRANTIES OR CONDITIONS OF ANY KIND,
                            EITHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
                            FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. THE FOREGOING DOES NOT AFFECT ANY WARRANTIES THAT
                            CANNOT BE EXCLUDED OR LIMITED UNDER APPLICABLE LAW.
                            <br/>
                            NEITHER BURRARD PHARMA NOR ITS PARENT, SUBSIDIARIES, AFFILIATES, OR THEIR RESPECTIVE DIRECTORS,
                            OFFICERS, EMPLOYEES, AGENTS, SERVICE PROVIDERS, CONTRACTORS, LICENSORS, LICENSEES, SUPPLIERS, OR
                            SUCCESSORS MAKE ANY WARRANTY, REPRESENTATION OR ENDORSEMENT WITH RESPECT TO THE COMPLETENESS, SECURITY,
                            RELIABILITY, SUITABILITY, ACCURACY, CURRENCY, OR AVAILABILITY OF THE WEBSITE OR ITS CONTENTS. WITHOUT
                            LIMITING THE FOREGOING, NEITHER BURRARD PHARMA NOR ITS PARENT, SUBSIDIARIES, AFFILIATES OR THEIR
                            RESPECTIVE DIRECTORS, OFFICERS, EMPLOYEES, AGENTS, SERVICE PROVIDERS, CONTRACTORS, LICENSORS, LICENSEES,
                            SUPPLIERS, OR SUCCESSORS REPRESENT OR WARRANT THAT THE WEBSITE, ITS CONTENT, OR ANY SERVICES OR ITEMS
                            FOUND OR ATTAINED THROUGH THE WEBSITE WILL BE ACCURATE, RELIABLE, ERROR-FREE, OR UNINTERRUPTED, THAT
                            DEFECTS WILL BE CORRECTED, THAT OUR WEBSITE OR THE SERVER THAT MAKES IT AVAILABLE ARE FREE OF VIRUSES OR
                            OTHER HARMFUL COMPONENTS.
                            <br/>
                            WE CANNOT AND DO NOT GUARANTEE OR WARRANT THAT FILES OR DATA AVAILABLE FOR DOWNLOADING FROM THE INTERNET
                            OR THE WEBSITE WILL BE FREE OF VIRUSES OR OTHER DESTRUCTIVE CODE. YOU ARE SOLELY AND ENTIRELY
                            RESPONSIBLE FOR YOUR USE OF THE WEBSITE AND YOUR COMPUTER, INTERNET, AND DATA SECURITY. TO THE FULLEST
                            EXTENT PROVIDED BY LAW, WE WILL NOT BE LIABLE FOR ANY LOSS OR DAMAGE CAUSED BY DENIAL OF SERVICE ATTACK,
                            DISTRIBUTED DENIAL OF SERVICE ATTACK, OVERLOADING, FLOODING, MAIL BOMBING, OR CRASHING, VIRUSES, TROJAN
                            HORSES, WORMS, LOGIC BOMBS, OR OTHER TECHNOLOGICALLY HARMFUL MATERIAL THAT MAY INFECT YOUR COMPUTER
                            EQUIPMENT, COMPUTER PROGRAMS, DATA, OR OTHER PROPRIETARY MATERIAL DUE TO YOUR USE OF THE WEBSITE OR ANY
                            SERVICES OR ITEMS FOUND OR ATTAINED THROUGH THE WEBSITE OR TO YOUR DOWNLOADING OF ANY MATERIAL POSTED ON
                            IT, OR ON ANY WEBSITE LINKED TO IT.
                        </p>
                        <h4>14. LIMITATION ON LIABILITY</h4>
                        <p>14.1 &nbsp;EXCEPT WHERE SUCH EXCLUSIONS ARE PROHIBITED BY LAW, UNDER NO CIRCUMSTANCES WILL BURRARD PHARMA
                            NOR ITS PARENT, SUBSIDIARIES, AFFILIATES OR THEIR RESPECTIVE DIRECTORS, OFFICERS, EMPLOYEES, AGENTS,
                            SERVICE PROVIDERS, CONTRACTORS, LICENSORS, LICENSEES, SUPPLIERS, OR SUCCESSORS BE LIABLE FOR NEGLIGENCE,
                            GROSS NEGLIGENCE, NEGLIGENT MISREPRESENTATION, FUNDAMENTAL BREACH, DAMAGES OF ANY KIND, UNDER ANY LEGAL
                            THEORY, INCLUDING ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL, OR PUNITIVE DAMAGES,
                            INCLUDING, BUT NOT LIMITED TO, PERSONAL INJURY, PAIN AND SUFFERING, EMOTIONAL DISTRESS, LOSS OF REVENUE,
                            LOSS OF PROFITS, LOSS OF BUSINESS OR ANTICIPATED SAVINGS, LOSS OF USE, LOSS OF GOODWILL, LOSS OF DATA,
                            AND WHETHER CAUSED BY TORT (INCLUDING NEGLIGENCE), BREACH OF CONTRACT, BREACH OF PRIVACY, OR OTHERWISE,
                            EVEN IF THE PARTY WAS ALLEGEDLY ADVISED OR HAD REASON TO KNOW, ARISING OUT OF OR IN CONNECTION WITH YOUR
                            USE, OR INABILITY TO USE, OR RELIANCE ON, THE WEBSITE, THE SERVICES PURCHASED THROUGH THE WEBSITE
                            (INCLUDING WITHOUT LIMITATION THE SERVICES RELATED TO THE DRUG FORMULATION, KNOW-HOW, DRUG
                            MANUFACTURING, OR TECHNOLOGY TRANSFER) ANY LINKED WEBSITES OR SUCH OTHER THIRD PARTY WEBSITES, NOR ANY
                            WEBSITE CONTENT, MATERIALS, POSTING, OR INFORMATION THEREON EVEN IF THE PARTY WAS ALLEGEDLY ADVISED OR
                            HAD REASON TO KNOW.
                        </p>
                        <h4>15. INDEMNIFICATION</h4>
                        <p>15.1 &nbsp;To the maximum extent permitted by applicable law, you agree to defend, indemnify, and hold
                            harmless Burrard Pharma, its parent, subsidiaries, affiliates, and their respective directors, officers,
                            employees, agents, service providers, contractors, licensors, suppliers, successors, and assigns from
                            and against any claims, liabilities, damages, judgments, awards, losses, costs, expenses, or fees
                            (including reasonable attorneys’ fees) arising out of or relating to the breach of these Terms and
                            Conditions or your use of the Website, including, but not limited to, your User Submissions, third-party
                            sites, any use of the Website’s content, services, and products other than as expressly authorized in
                            these Terms and Conditions.</p>
                        <h4>16. GOVERNING LAW AND CHOICE OF FORUM</h4>
                        <p>16.1 &nbsp;The Website and these Terms and Conditions will be governed by and construed in accordance
                            with the laws of the Province of British Columbia and the federal laws of Canada applicable therein,
                            without giving effect to any choice or conflict of law provision, principle, or rule (whether of the
                            laws of the Province of British Columbia or any other jurisdiction) and notwithstanding your domicile,
                            residence or physical location.
                        </p>
                        <p>16.2 &nbsp;Any action or proceeding arising out of or relating to this Website and under these Terms and
                            Conditions will be instituted in the courts of the Province of British Columbia and/or the Federal Court
                            of Canada, and each party irrevocably submits to the exclusive jurisdiction of such courts in any such
                            action or proceeding. You waive any and all objections to the exercise of jurisdiction over you by such
                            courts and to the venue of such courts.
                        </p>
                        <h4>17. WAIVER</h4>
                        <p>17.1 &nbsp;No failure to exercise, or delay in exercising, any right, remedy, power, or privilege arising
                            from these Terms and Conditions operates, or may be construed as a waiver thereof. No single or partial
                            exercise of any right, remedy, or privilege hereunder precludes any other or further exercise thereof or
                            the exercise of any other right, remedy, power or privilege.
                        </p>
                        <h4>18. SEVERABILITY</h4>
                        <p>18.1 &nbsp;If any term or provision of these Terms and Conditions is invalid, illegal, or unenforceable
                            in any jurisdiction, such invalidity, illegality, or unenforceability shall not affect any other term or
                            provision of these Terms and Conditions or invalidate or render unenforceable such term or provision in
                            any other jurisdiction.
                        </p>
                        <h4>19. ENTIRE AGREEMENT</h4>
                        <p>19.1 &nbsp;The Terms and Conditions and our Privacy Policy constitute the sole and entire agreement
                            between you and Burrard Pharma regarding the Website and supersedes all prior and contemporaneous
                            understandings, agreements, representations and warranties, both written and oral, regarding such
                            subject matter.
                        </p>
                        <h4>20. REPORTING AND CONTACT</h4>
                        <p>20.1 &nbsp;This Website is operated by Burrard Pharmaceuticals Enterprises Ltd.</p>
                        <p>20.2 &nbsp;Should you become aware of misuse of this Website, you must report it to <a href="mailto:info@burrardpharma.com">info@burrardpharma.com</a> </p>
                        <p style="text-align: center"><b>[End of Terms and Conditions]</b></p>
                    </div>
                </div>
                <label for="agree" class="agree-here">
                    <input type="checkbox" name="agree" id="agree">
                    <p>I agree to the Burrard Pharma terms & conditions and <a href="{{URL::to('privacy-policy')}}">
                            privacy policy</a>.</p>
                </label>
                <div class="btn-wrap">
                    <button class="close-forgot" type="button">Close</button>
                    <button class="send-forgot rgconfirm" type="submit" disabled>Register</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('front_js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>

        window.addEventListener("load", function () {
            var tabs = document.getElementById("tabs"),
                tab = document.getElementsByClassName("tab"),
                tabContent = document.getElementsByClassName("tabContent"),
                input = document.getElementsByClassName("form__input"),
                active = "tab__active",
                form = document["signUp"],
                formEl = document["signUp"].elements,
                formLogIn = document["logIn"],
                dataInLocal = window.localStorage;



            form.addEventListener("submit", function (e) {
                if (e.target) {
                    for (var i = 0, len = form.length; i < len; i++) {
                        dataInLocal.setItem(form[i].name, form[i].value);
                    }
                }
            }, false);

            for (var i = 0, len = input.length; i < len; i++) {
                input[i].addEventListener("focus", function (e) {
                    var label = e.target.nextElementSibling;
                    if (e.target.value == "") {
                        label.style.display = "none";
                    }
                }, false);
                input[i].addEventListener("blur", function (e) {
                    var label = e.target.nextElementSibling;
                    if (e.target.value == "") {
                        label.style.display = "block";
                    } else {
                        label.style.display = "none";
                    }
                }, false);
            }


            tabs.addEventListener("click", function (e) {
                for (var i = 0, a = tabContent.length; i < a; i++) {
                    tab[i].classList.remove(active);

                    if (e.target.className == "tab") {
                        if (e.target.className != active) {
                            for (var j = 0; j < a; j++) {
                                tabContent[j].classList.remove("show");
                            }
                            e.target.classList.add(active);
                            tabContent[i].classList.add("show");

                        }
                    }
                }
            }, false);
        });

        $(".forgot-password").click(function (e) {
            e.preventDefault();
            $("#pop-up-wrapper").fadeIn(500);
        });

        $(".register").click(function (e) {
            e.preventDefault();

        });

        $(".close-forgot").click(function (e) {
            e.preventDefault();
            $(".pop-up-wrapper").fadeOut(500);
        });

        $(document).ready(function () {
            var redirect = localStorage.getItem('reverse');

            $('.reverse').val(redirect);

        });

        $(document).on('click', '.login', function () {
            var email = $('.lg_email').val();
            var password = $('.lg_pass').val();

            if (email == '') {
                swal("Alert!", "Email is required", "error");
                return false;
            } else {
                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if (!emailReg.test(email)) {
                    swal("Alert!", "Please, Enter valid email", "error");
                    return false;
                }
            }

            if (password == '') {
                swal("Alert!", "Passwrod is required", "error");
                return false;
            }

            var reverse = localStorage.getItem('reverse');
            $.ajax({
                url: "login",
                type: "POST",
                beforeSend: function () {
                    $("#loading-image").show();
                },
                data: {
                    "_token": "{{ csrf_token() }}",
                    "email": email,
                    "password": password,
                    "redirect": reverse,
                },
                success: function (data) {
                    // console.log(data);
                    if (data.ResponseCode == 0) {
                        $('.lg_email').addClass('is-invalid');
                        $('.e_err').css('display', 'block');
                        $('.e_err strong').text(data.ResponseText);
                    } else if (data.ResponseCode == 1) {
                        if (reverse == 'pricing') {
                            window.location.href = "pricing";
                        } else if (reverse == 'generator') {
                            window.location.href = "generator";
                        } else {
                            window.location.href = "user-profile";
                        }
                    }
                    //

                },
                error: function (data) {
                    var data = data.responseJSON.errors;

                    $(data).each(function (index, element) {

                        if (element['email'] != undefined) {
                            $('.lg_email').addClass('is-invalid');
                            $('.e_err').css('display', 'block');
                            $('.e_err strong').text(element['email'][0]);
                        }
                        if (element['password'] != undefined) {
                            $('.lg_pass').addClass('is-invalid');
                            $('.pass_err').css('display', 'block');
                            $('.pass_err strong').text(element['password'][0]);
                        }

                    });

                }
            });

        });

        $(document).on('click', '.register', function () {

            var first_name = $('.form__input_name').val();
            var last_name = $('.form__input_lastName').val();
            var email = $('.form__input_email').val();
            var password = $('.form__input_pass').val();
            var organization = $('.organization').val();
            var phone_number = $('.phone_number').val();

            if (first_name == '') {
                swal("Alert!", "First name is required", "error");
                return false;
            } else {
                var namereg = /^[a-zA-Z][a-zA-Z0-9-_\.]{1,15}$/;
                if (!namereg.test(first_name)) {
                    swal("Alert!", "Please, Enter valid First Name", "error");
                    return false;
                }
            }

            if (last_name == '') {
                swal("Alert!", "Last name is required", "error");
                return false;
            } else {
                var namereg = /^[a-zA-Z][a-zA-Z0-9-_\.]{1,15}$/;
                if (!namereg.test(last_name)) {
                    swal("Alert!", "Please, Enter valid Last Name", "error");
                    return false;
                }
            }

            if (email == '') {
                swal("Alert!", "Email is required", "error");
                return false;
            } else {
                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if (!emailReg.test(email)) {
                    swal("Alert!", "Please, Enter valid email", "error");
                    return false;
                }
            }

            if (password == '') {
                swal("Alert!", "Passwrod is required", "error");
                return false;
            } else {
                var passwordreg = /^(?=.*\d).{8,}$/;
                if (!passwordreg.test(password)) {
                    swal("Alert!", "Password must be at least 8 characters", "error");
                    return false;
                }
            }


            if (phone_number == '') {
                swal("Alert!", "Phone Number is required", "error");
                return false;
            } else {
                var namereg = /^[0-9]{1,10}$/;
                if (!namereg.test(phone_number)) {
                    swal("Alert!", "Please, Enter valid Phone Number", "error");
                    return false;
                }
            }

            localStorage.setItem('tplogstd', 'email');
            $("#register-modal").fadeIn(500);


        });

        $(document).ready(function () {
            $('a[href^="#"]').click(function (event) {
                var id = $(this).attr("href");
                var offset = 0;
                var target = $(id).offset().top - offset;
                $('html, body').animate({scrollTop: target}, 500);
                event.preventDefault();
            });
        });

        $(document).on('click', '#agree', function () {
            if ($(this).prop("checked") == true) {
                $('.rgconfirm').removeAttr("disabled");
            }
        });

        $(document).on('click', '.rgconfirm', function () {
            var loginType = localStorage.getItem('tplogstd');

            if (loginType == 'google') {
                window.location.href = 'auth/google';
            } else if (loginType == 'linkedin') {
                window.location.href = 'auth/linkedin';
            } else {
                var first_name = $('.form__input_name').val();
                var last_name = $('.form__input_lastName').val();
                var email = $('.form__input_email').val();
                var password = $('.form__input_pass').val();
                var organization = $('.organization').val();
                var phone_number = $('.phone_number').val();

                var reverse = localStorage.getItem('reverse');

                $.ajax({
                    url: "register",
                    type: "POST",
                    beforeSend: function () {
                        $('#loader').css('display','block');
                        $('.register-wrapper').css('opacity','0.2');
                        $('#register-modal').css('opacity','0.4');
                    },
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "first_name": first_name,
                        "last_name": last_name,
                        "email": email,
                        "password": password,
                        "organization": organization,
                        "phone_number": phone_number,
                        "redirect": reverse,
                    },
                    success: function (data) {
                        $('#loader').css('display','none');
                        if(data.status == 2){
                            window.location.href = "email/verify";
                        } else {
                            if (reverse == 'pricing') {
                                window.location.href = "pricing";
                            } else if (reverse == 'generator') {
                                window.location.href = "generator";
                            } else {
                                window.location.href = "user-profile";
                            }
                        }


                    },
                    error: function (data) {

                        var data = data.responseJSON.errors;

                        $(data).each(function (index, element) {
                            if (element['first_name'] != undefined) {
                                $('.form__input_name').addClass('is-invalid');
                                $('.first_name_err').css('display', 'block');
                                $('.first_name_err strong').text(element['first_name'][0]);
                            }

                            if (element['last_name'] != undefined) {
                                $('.form__input_lastName').addClass('is-invalid');
                                $('.last_name_err').css('display', 'block');
                                $('.last_name_err strong').text(element['last_name'][0]);
                            }
                            if (element['email'] != undefined) {
                                $('.form__input_email').addClass('is-invalid');
                                $('.email_err').css('display', 'block');
                                $('.email_err strong').text(element['email'][0]);
                            }
                            if (element['password'] != undefined) {
                                $('.form__input_pass').addClass('is-invalid');
                                $('.password_err').css('display', 'block');
                                $('.password_err strong').text(element['password'][0]);
                            }

                            if (element['phone_number'] != undefined) {
                                $('.phone_number').addClass('is-invalid');
                                $('.phone_number_err').css('display', 'block');
                                $('.phone_number_err strong').text(element['phone_number'][0]);
                            }
                        });

                        $(".pop-up-wrapper").fadeOut(500);


                    }
                });
            }

        })

        $(document).on('click', '.linkedin', function () {
            localStorage.setItem('tplogstd', 'linkedin');
            $("#register-modal").fadeIn(500);
        });

        $(document).on('click', '.google', function () {
            localStorage.setItem('tplogstd', 'google');
            $("#register-modal").fadeIn(500);
        })

        // Pageload Delay

        function delay(URL) {
            setTimeout(function () {
                window.location = URL
            }, 1700);
        }

        $(window).bind("pageshow", function (event) {
            if (event.originalEvent.persisted) {
                window.location.reload()
            }
        });

        $(window).scroll(function () {
            if ($(this).scrollTop() > 1) {
                $("#nav-hidden").stop().fadeIn(250)
            } else {
                $("#nav-hidden").stop().fadeOut(250)
            }
        });

        function watchForHover() {
            var hasHoverClass = false;
            var container = document.body;
            var lastTouchTime = 0;

            function enableHover() {
                if (new Date() - lastTouchTime < 500) return;
                if (hasHoverClass) return;
                container.className += ' hasHover';
                hasHoverClass = true;
            }

            function disableHover() {
                if (!hasHoverClass) return;
                container.className = container.className.replace(' hasHover', '');
                hasHoverClass = false;
            }

            function updateLastTouchTime() {
                lastTouchTime = new Date();
            }

            document.addEventListener('touchstart', updateLastTouchTime, true);
            document.addEventListener('touchstart', disableHover, true);
            document.addEventListener('mousemove', enableHover, true);
            enableHover();
        }

        watchForHover();


    </script>
@endsection
