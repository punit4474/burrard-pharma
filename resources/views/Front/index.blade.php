@extends('layouts.front')
@section('front_title')
    Home
@endsection
@section('meta')
    <link rel="canonical" href="http://www.burrardpharma.com/"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Title"/>
    <meta property="og:description" content="Description"/>
    <meta property="og:url" content="http://www.burrardpharma.com/"/>
    <meta property="og:site_name" content="Burrard Pharma"/>
    <meta name="description" content="Description"/>
    <meta name="twitter:title" content="Title">
    <meta name="twitter:description" content="Description">
@endsection
@section('front_css')

@endsection
@section('front_content')
	<div id="landing-wrapper">
		<div class="landing-container">
			<video class="video-content" poster="" src="{{URL::to('storage/app/public/Frontassets/media/landing.mp4')}}" type="video/mp4" autoplay loop playsinline muted data-object-fit="cover"></video>
			<div class="landing-text-container">
				<div class="landing-text">
					<h1><span>Your partner</span><br>in drug development</h1>
					<p>Burrard Pharma is a leader in technology transfer and drug development. We strive to empower ideas into successful pharmaceutical projects.</p>
					<a class="landing-btn" href="#blue-wrapper-swiper"><img src="{{URL::to('storage/app/public/Frontassets/media/down.svg')}}"></a>
				</div>
			</div>
		</div>
	</div>

    <div class="twin-wrapper" id="blue-wrapper-swiper">
        <div class="twin-container">
            <div class="twin-left">
                <div class="twin-blue">
                    <h1>Our core comprehensive drug formulation services are designed to reduce your costs and time required, helping you achieve your drug discovery goals.</h1>
                </div>
            </div>
            <div class="twin-right">
                <div class="twin-white">
                    <h1>Since everything is built together, it just works together, better and has proven several times in our highly equipped laboratories.</h1>
                </div>
            </div>
        </div>
    </div>


    <div class="fourbox-wrapper">
        <div class="fourbox-container">
            <div class="fourbox-content">
                <div class="fourbox-icon">
                    <img src="{{URL::to('storage/app/public/Frontassets/media/formulation.svg')}}" alt="">
                </div>
                <div class="fourbox-copy">
                    <h1>Drug formulation</h1>
                    <p>Our core services include process development, formulation development, preclinical (toxicology) and batch production.</p>
                    <p>1. Master formulation you need to produce a a drug either in clinical size or commercial in one place.</p>
                    <p>2. You will have full access to our knowledge, manufacturing secrets, formulation, master formula, products specifications, studies and more from a single dashboard.</p>
                </div>
            </div>
            <div class="fourbox-content">
                <div class="fourbox-icon">
                    <img src="{{URL::to('storage/app/public/Frontassets/media/know-how.svg')}}" alt="">
                </div>
                <div class="fourbox-copy">
                    <h1>know-How portal Everything in one place</h1>
                    <p>On our knowledge access portal, everything about your project is in one place. Your selected drug formulation content, your master formula, know-how, stability studies … it all works seamlessly together so you can focus on your project either in schools or at production site.</p>
                </div>
            </div>
            <div class="fourbox-content">
                <div class="fourbox-icon">
                    <img src="{{URL::to('storage/app/public/Frontassets/media/pay.svg')}}" alt="">
                </div>
                <div class="fourbox-copy">
                    <h1>Pay on what you need</h1>
                    <p>Simple, seamless integration with major credit cards and PayPal is easier, most reliable and faster.</p>
                </div>
            </div>
            <div class="fourbox-content">
                <div class="fourbox-icon">
                    <img src="{{URL::to('storage/app/public/Frontassets/media/manufacturing.svg')}}" alt="">
                </div>
                <div class="fourbox-copy">
                    <h1>Choose a drug</h1>
                    <p>Customize with dosage form available in USP. Then purchase!!! EASY. Your know-how is now fully accessible with one click and integrated with the rest of your business.</p>
                </div>
            </div>
        </div>
    </div>


    <div class="holder-blank">
        <div class="group-wrapper">
            <div class="group-container">
                <div class="group-copy">
                    <div class="copy-content">
                        <h1>Choose your product</h1>
                        <p>Our Master formulation developed by our scientist. include Manufacturing process, Quality controls, Lab analysis and Studies.</p>
                    </div>
                    <div class="copy-space">
                        <!-- SPACE -->
                    </div>
                </div>
                <div class="group-media">
                    <div class="media-space">
                        <!-- SPACE -->
                    </div>
                    <div class="media-content">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/choose.svg')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="group-wrapper">
            <div class="group-container">
                <div class="group-copy">
                    <div class="copy-space">
                        <!-- SPACE -->
                    </div>
                    <div class="copy-content">
                        <h1>Check your mailbox</h1>
                        <p>We guarantee the delivery of all master formula in 2 business days.</p>
                    </div>
                </div>
                <div class="group-media">
                    <div class="media-content">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/mailbox.svg')}}" alt="">
                    </div>
                    <div class="media-space">
                        <!-- SPACE -->
                    </div>
                </div>
            </div>
        </div>
        <div class="group-wrapper">
            <div class="group-container">
                <div class="group-copy">
                    <div class="copy-content">
                        <h1>Create Magic</h1>
                        <p>Following our scientists step-by-step instructions, you'll experience the magic that our formulation developers create with your projects in mind.</p>
                    </div>
                    <div class="copy-space">
                        <!-- SPACE -->
                    </div>
                </div>
                <div class="group-media">
                    <div class="media-space">
                        <!-- SPACE -->
                    </div>
                    <div class="media-content">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/magic.svg')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="holder-color">
        <div class="group-wrapper">
            <div class="group-container">
                <div class="group-copy">
                    <div class="copy-content">
                        <h1>Pharmaceutical Full Service Provider</h1>
                        <p>Founded over ten years ago, Burrard Pharma brings together industry leading professionals dedicated to offering high-quality drug products and services.</p>
                        <button class="copy-btn">
                            <a href="{{URL::to('service')}}">Learn more</a>
                        </button>
                    </div>
                    <div class="copy-space">
                        <!-- SPACE -->
                    </div>
                </div>
                <div class="group-media">
                    <div class="media-space">
                        <!-- SPACE -->
                    </div>
                    <video class="video-content-small"
                           poster=""
                           src="{{URL::to('storage/app/public/Frontassets/media/home.mp4')}}"
                           type="video/mp4"
                           autoplay loop playsinline muted
                           data-object-fit="cover">
                    </video>
                </div>
            </div>
        </div>
    </div>


    <div class="holder-blank">
        <div class="group-wrapper">
            <div class="group-container">
                <div class="group-copy">
                    <div class="copy-space">
                        <!-- SPACE -->
                    </div>
                    <div class="copy-content">
                        <h1>24/7 Support</h1>
                        <p>We’re devoted about your success Our specialist team is available to answer your questions at any time, day or night. Just push the button.</p>
                        <button class="copy-btn">
                            <a href="{{URL::to('company')}}">Learn more</a>
                        </button>
                    </div>
                </div>
                <div class="group-media">
                    <div class="media-content">
                        <img src="{{URL::to('storage/app/public/Frontassets/media/support.svg')}}" alt="">
                    </div>
                    <div class="media-space">
                        <!-- SPACE -->
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="newsletter-wrapper">
        <div class="newsletter-container">
            <div class="news-title">
                <h1>Subscribe to our newsletter</h1>
                <p></p>
            </div>
            <div class="news-form">
                <input class="news-email" type="email" name="mail" placeholder="E-mail address">
                <input class="news-subscribe" type="submit" name="news" value="Subscribe">
            </div>
        </div>
    </div>


@endsection
@section('front_js')
    <script src="{{URL::to('storage/app/public/Frontassets/js/index.js')}}"></script>

    <script src="{{URL::to('storage/app/public/Frontassets/js/objectFitPolyfill.min.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        var swiper = new Swiper('.swiper-container', {

            slidesPerView: 1,
            spaceBetween: 50,
            freeMode: true,
            grabCursor: true,

            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },

            breakpoints: {
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 50,
                    freeMode: true,
                    grabCursor: true,
                },
                1440: {
                    slidesPerView: 3,
                    spaceBetween: 80,
                    freeMode: false,
                    grabCursor: true,
                },
            }
        });
        $(document).on('click','.news-subscribe',function (){
           var email = $('.news-email').val();

            if(email == '') {
                swal("Alert!", "Email is required", "error");
                return false;
            }else {
                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if( !emailReg.test( email ) ) {
                    swal("Alert!", "Please, Enter valid email", "error");
                    return false;
                }
            }

            $.ajax({
                url: "news-letter",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "email": email
                },
                success: function (data) {
                    if(data.status == 1){
                        $('.news-email').val('');
                        swal("Success!", "Newsletter Subscribe Successfully", "success");
                    }
                }
            });
        });
    </script>
    <script>
        // Back To Top

        $(document).ready(function () {
            console.log($('.nav-pc').height());
            $('a[href^="#"]').click(function (event) {
                var id = $(this).attr("href");
                var offset = 0;
                var target = $(id).offset().top - offset - $('.nav-pc').height();
                $('html, body').animate({scrollTop: target}, 500);
                event.preventDefault();
            });
        });


        // Pageload Delay

        function delay (URL) {
            setTimeout( function() { window.location = URL }, 1700 );
        }
        $(window).bind("pageshow", function(event) {
            if (event.originalEvent.persisted) {
                window.location.reload()
            }
        });

        $(window).scroll(function() {
            if ($(this).scrollTop() > 1) {
                $("#nav-hidden").stop().fadeIn(250)
            } else {
                $("#nav-hidden").stop().fadeOut(250)
            }
        });

        function watchForHover() {
            var hasHoverClass = false;
            var container = document.body;
            var lastTouchTime = 0;

            function enableHover() {
                if (new Date() - lastTouchTime < 500) return;
                if (hasHoverClass) return;
                container.className += ' hasHover';
                hasHoverClass = true;
            }

            function disableHover() {
                if (!hasHoverClass) return;
                container.className = container.className.replace(' hasHover', '');
                hasHoverClass = false;
            }

            function updateLastTouchTime() {
                lastTouchTime = new Date();
            }

            document.addEventListener('touchstart', updateLastTouchTime, true);
            document.addEventListener('touchstart', disableHover, true);
            document.addEventListener('mousemove', enableHover, true);
            enableHover();
        }

        watchForHover();

    </script>
@endsection
@section('footer_sc')
    <div class="footer-icons">
        <a href="#landing-wrapper"><img src="{{URL::to('storage/app/public/Frontassets/media/top.svg')}}"></a>
    </div>
@endsection
