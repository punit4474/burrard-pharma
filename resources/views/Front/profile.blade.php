@extends('layouts.front')
@section('front_title')
    Profile
@endsection
@section('meta')
    <link rel="canonical" href="http://www.burrardpharma.com/"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Title"/>
    <meta property="og:description" content="Description"/>
    <meta property="og:url" content="http://www.burrardpharma.com/"/>
    <meta property="og:site_name" content="Burrard Pharma"/>
    <meta name="description" content="Description"/>
    <meta name="twitter:title" content="Title">
    <meta name="twitter:description" content="Description">
@endsection
@section('front_css')
    <style>
        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 1rem;
            background-color: transparent;
            font-family: 'SofiaPro-regular';
            font-weight: normal;
            color: #2c3c49;
            font-size: 1.025rem;
        }
        thead {
            display: table-header-group;
            vertical-align: middle;
            border-color: inherit;
        }
        tr {
            display: table-row;
            vertical-align: inherit;
            border-color: inherit;
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #dee2e6;
        }

        .table td, .table th {
                white-space: nowrap;
                padding: .75rem;
                vertical-align: top;
                border-top: 1px solid #dee2e6;
            }
        td {
            display: table-cell;
            vertical-align: inherit;
        }

        a.btn.left-panel.edit-profiles {
            background: #00c0ff;
            color: #fff;
            padding: 8px 24px;
            border-radius: 4rem;
            text-decoration: none;
            margin-left: auto;
        }

        .list-title {
            display: flex;
            align-items: center;
        }

        .alert.alert-success {
            margin-bottom: 10px;
            text-align: center;
            padding: 10px;
            background: #00c0ff;
            color: white;
            border-radius: 15px;
            font-family: 'SofiaPro-regular';
            font-size: 1rem;
        }
    </style>
@endsection
@section('front_content')
<div id="tab-wrapper">
    <div class="user-container">
        <div class="user-title">
            <h1>Hello, <span>{{Auth::user()->first_name}}</span><span>!</span></h1>
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('message'))
                        {!! Session::get('message') !!}
                    @endif
                </div>
            </div>
        </div>
        <div class="user-content">
            <div class="dashboard">
                <div class="dashboard-btns">
                    <button class="userbtn" onclick="tab(event, 'a')" id="defaultOpen">Profile</button>
                    <button class="userbtn" onclick="tab(event, 'b')">Payments</button>
                    <button class="userbtn" onclick="tab(event, 'c')">Subscription</button>
                </div>
                <div style="display: flex;height: 100%;overflow: auto;align-items: flex-start;">
                    <div id="a" class="userbody">
                        <div class="list-title">
                            <p>Your information</p>
                            <a href="" class="btn left-panel edit-profiles">Edit Profile</a>
                        </div>
                        <div class="user-info">
                            <p>First name</p>
                            <h5>{{Auth::user()->first_name}}</h5>
                        </div>
                        <div class="user-info">
                            <p>Last name</p>
                            <h5>{{Auth::user()->last_name}}</h5>
                        </div>
                        <div class="user-info">
                            <p>E-mail</p>
                            <h5>{{Auth::user()->email}}</h5>
                        </div>
                        <div class="user-info">
                            <p>Phone</p>
                            <h5>{{Auth::user()->phone_number == '' ? '-' : Auth::user()->phone_number}}</h5>
                        </div>
                        <div class="user-info">
                            <p>Organization</p>
                            <h5>{{Auth::user()->organization == ''? '-':Auth::user()->organization}}</h5>
                        </div>
                        <div class="user-info">
                            <p>Password</p>
                            <h5>*****************<a href="#" class='change-password'>Change</a></h5>
                        </div>
                    </div>
                    <div id="b" class="userbody">
                        <div class="list-title">
                            <p>Your recent payments</p>
                        </div>
                        @foreach($order as $row)
                            <div class="purchase-content">
                                <div class="purchase-title">
                                    <div class="purchase-detail">
                                        <p>Order id (#{{$row->order_id}})</p>
                                        <span>{{\Carbon\Carbon::parse($row->order_date)->format('d.m.Y')}}</span>
                                    </div>
                                    <div class="purchase-cost">
                                        <p>${{number_format($row->payment_amount)}}</p>
                                    </div>
                                </div>
                                @foreach($row->orderDetails as $item)
                                    <div class="purchase-info" style="margin-bottom:10px">
                                        <div class="drug-name">
                                            <h1>{{$item->name}}</h1>
                                        </div>
                                        <div class="drug-info-left">
                                            <div id="dosage">{{$item->dosage_form}}</div>
                                            <div id="strengh">{{$item->strengh}}</div>
                                        </div>
                                        <div class="drug-info-right">
                                            <div id="route">{{$item->route_of_admin}}</div>
                                            <div id="code">{{$item->atc_code}}</div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach

                    </div>
                    <div id="c" class="userbody">
                        <div class="list-title">
                            <p>Your Subscription</p>
                        </div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Subscription Id</th>
                                <th>Amount</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($subscription as $row)
                                <tr>
                                    <td>{{$row->name}}</td>
                                    <td>{{$row->email}}</td>
                                    <td>{{$row->subscription_id}}</td>
                                    <td>{{$row->amount}}</td>
                                    <td>{{$row->start_date}}</td>
                                    <td>{{$row->end_date}}</td>
                                    <td>{{$row->status}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="pop-up-wrapper changePass">
    <div class="pop-up-container">
        <div class="pop-up-content">
            {{Form::open(array('url'=>'change-password','method'=>'post','name'=>'change-password','id'=>'change-password','class'=>'form'))}}

            <div class="title-forgot"><h1>Change password</h1></div>

            <input class="email-forgot" type="password" id="email" name="password" placeholder="enter new password" required
            pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$">
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            <button class="close-forgot" type="submit">Close</button>
            <button class="send-forgot" type="submit">Submit</button>
            {{Form::close()}}
        </div>
    </div>
</div>

<div class="pop-up-wrapper edit-profile">
    <div class="pop-up-container">
        <div class="pop-up-content">
            {{Form::open(array('url'=>'update-profile','method'=>'post','name'=>'change-password','id'=>'edit-profile','class'=>'form'))}}

            <div class="title-forgot"><h1>Edit Profile</h1></div>
            <input class="form__input form__input_name"
                   name="first_name" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,15}$" required=""
                   title="Please Enter letter or number, max symbols 15" type="text" value="{{Auth::user()->first_name}}" placeholder="First Name">

            <input class="form__input form__input_lastName @error('last_name') is-invalid @enderror"
                   name="last_name" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,15}$" required="" value="{{Auth::user()->last_name}}"
                   title="Please Enter letter or number, max symbols 15" type="text" placeholder="Last Name">
            <input
                class="form__input form__input_lastName phone_number @error('phone_number') is-invalid @enderror"
                name="phone_number" pattern="^[0-9]{1,15}$" required="" value="{{Auth::user()->phone_number}}"
                title="Please Enternumber, max symbols 15" type="text" placeholder="Phone Number">

            <input
                class="form__input form__input_name organization @error('organization') is-invalid @enderror"
                name="organization" pattern="^[a-zA-Z][a-zA-Z0-9-_\. ]{1,15}$" required="" value="{{Auth::user()->organization}}"
                title="Please Enter letter or number, max symbols 15" type="text" placeholder="Organization">

            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            <button class="close-profile" type="submit">Close</button>
            <button class="send-forgot" type="submit">Submit</button>
            {{Form::close()}}
        </div>
    </div>
</div>

@endsection
@section('front_js')
    <script>

        function tab(evt,pageName,elmnt,color) {
        var i, userbody, tablinks;
        userbody = document.getElementsByClassName("userbody");
        for (i = 0; i < userbody.length; i++) {
            userbody[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("userbtn");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }

        document.getElementById(pageName).style.display = "flex";
        evt.currentTarget.className += " active";
        }

        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();

        function incrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal)) {
            parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
        }

        function decrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

        if (!isNaN(currentVal) && currentVal > 0) {
            parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
        }

        $('.credits-box').on('click', '.button-plus', function(e) {
        incrementValue(e);
        });

        $('.credits-box').on('click', '.button-minus', function(e) {
        decrementValue(e);
        });

        $(".change-password").click(function(e) {
			e.preventDefault();
			$(".changePass").fadeIn(500);
		});

		$(".close-forgot").click(function(e) {
			e.preventDefault();
			$(".changePass").fadeOut(500);
		});

        $(".edit-profiles").click(function(e) {
			e.preventDefault();
			$(".edit-profile").fadeIn(500);
		});

		$(".close-profile").click(function(e) {
			e.preventDefault();
			$(".edit-profile").fadeOut(500);
		});


    </script>
    <script>
        // Back To Top

        $(document).ready(function () {
            $('a[href^="#"]').click(function (event) {
                var id = $(this).attr("href");
                var offset = 0;
                var target = $(id).offset().top - offset;
                $('html, body').animate({scrollTop: target}, 500);
                event.preventDefault();
            });
        });


        // Pageload Delay

        function delay (URL) {
            setTimeout( function() { window.location = URL }, 1700 );
        }
        $(window).bind("pageshow", function(event) {
            if (event.originalEvent.persisted) {
                window.location.reload()
            }
        });

        $(window).scroll(function() {
            if ($(this).scrollTop() > 1) {
                $("#nav-hidden").stop().fadeIn(250)
            } else {
                $("#nav-hidden").stop().fadeOut(250)
            }
        });

        function watchForHover() {
            var hasHoverClass = false;
            var container = document.body;
            var lastTouchTime = 0;

            function enableHover() {
                if (new Date() - lastTouchTime < 500) return;
                if (hasHoverClass) return;
                container.className += ' hasHover';
                hasHoverClass = true;
            }

            function disableHover() {
                if (!hasHoverClass) return;
                container.className = container.className.replace(' hasHover', '');
                hasHoverClass = false;
            }

            function updateLastTouchTime() {
                lastTouchTime = new Date();
            }

            document.addEventListener('touchstart', updateLastTouchTime, true);
            document.addEventListener('touchstart', disableHover, true);
            document.addEventListener('mousemove', enableHover, true);
            enableHover();
        }

        watchForHover();

    </script>
@endsection
@section('footer_sc')
    <div class="footer-icons">
        <a href="#landing-wrapper"><img src="{{URL::to('storage/app/public/Frontassets/media/top.svg')}}"></a>
    </div>
@endsection
