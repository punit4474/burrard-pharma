@extends('layouts.front')
@section('front_title')
    Privacy Policy
@endsection
@section('meta')
    <link rel="canonical" href="http://www.burrardpharma.com/"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Title"/>
    <meta property="og:description" content="Description"/>
    <meta property="og:url" content="http://www.burrardpharma.com/"/>
    <meta property="og:site_name" content="Burrard Pharma"/>
    <meta name="description" content="Description"/>
    <meta name="twitter:title" content="Title">
    <meta name="twitter:description" content="Description">
@endsection
@section('front_css')
@endsection
@section('front_content')


    <div id="privacy-wrapper">
        <div class="privacy-container">

            <h2>Privacy and policy</h2>
            <p>This Privacy Policy was last modified on:: September 15, 2020</p>
            <h4>INTRODUCTION</h4>
            <p> Burrard Pharmaceuticals Enterprises Ltd. (referred to herein as “Burrard Pharma”, “we”, “us”, “our”)
                respect your privacy and are committed to protecting it by complying with this Privacy Policy.
                Capitalized terms not otherwise defined under this Privacy Policy have the same meanings ascribed to
                them under our Terms and Conditions found at <a
                    href="{{URL::to('terms-and-conditions')}}">{{URL::to('terms-and-conditions')}}</a></p>
            <p>
                This Privacy Policy describes:
            </p>
            <p class="inner">
                (a) how we collect, use, disclose, and protect the personal information of our customers and
                website users (“you”);
            </p>
            <p class="inner">
                (b) the types of information we may collect from you or that you may provide when you visit
                our Website;
            </p>
            <p class="inner">
                (c) our practices for collecting, using, maintaining, protecting, and disclosing that
                information.
            </p>
            <p>
                We will only use your personal information in accordance with this Privacy Policy unless otherwise
                required by applicable law. We take steps to ensure that the personal information that we collect about
                you is adequate, relevant, not excessive, and used for limited purposes. Privacy laws in Canada
                generally
                define <b>“personal information”</b> as any information about an identifiable individual, which includes
                information that can be used on its own or with other information to identify, contact, or locate a
                single
                person. Personal information does not include business contact information, including your name, title,
                or
                business contact information. This Privacy Policy applies to information we collect, use, or disclose
                about
                you:
            </p>
            <p class="inner">(a) on this Website; </p>
            <p class="inner">(b) in email, text, and other electronic messages between you and this Website; </p>
            <p class="inner">(c) through mobile and desktop applications you download from the Website, which provide
                dedicated non-browser based interaction between you and this Website; and </p>
            <p class="inner">(d) when you interact with our advertising and applications on third-party websites and
                services if those applications or advertising include links to this Privacy Policy. </p>
            <p>
                This Website may include links to third-party websites, plug-ins, services, social networks, or
                applications. Clicking on those links or enabling those connections may allow the third party to collect
                or
                share data about you. If you follow a link to a third-party website or engage in a third-party plugin,
                please
                note that these third parties have their own privacy policies and we do not accept any responsibility or
                liability for these policies. We do not control these third-party websites, and we encourage you to read
                the
                privacy policy of every website you visit.
            </p>

            <p>
                <b>
                    Please read this Privacy Policy carefully to understand our policies and practices for collecting,
                    processing, and storing your information. If you do not agree with our policies and practices, your
                    choice is not to use this Website or the Services. By accessing or using this Website, you indicate
                    that you understand, accept, and consent to the practices described in this Privacy Policy. This
                    Privacy Policy may change from time to time. We will notify you in advance of any material
                    changes to this Privacy Policy and obtain your consent to any new ways that we collect, use, and
                    disclose your personal information.
                </b>
            </p>
            <h4>2. INFORMATION WE COLLECT ABOUT YOU</h4>
            <p>
                2.1 &nbsp; <b>What Information We Collect.</b> We collect and use several types of information from and
                about
                you, including:
            </p>
            <p class="inner">(a) personal information, that we can reasonably use to directly or indirectly identify
                you, such as your name, mailing address, email address, telephone number, Linkedin ID (if you choose
                Linkedin as your sign up method), Internet protocol (IP) address used to connect your computer to the
                Internet, user name or other similar identifier and any other identifier we may use to contact you
                (<b>“personal information”</b>);</p>
            <p class="inner">(b) billing and other payment information, including payment method details, such as a
                credit card number; </p>
            <p class="inner">(c) non-personal information, that does not directly or indirectly reveal your identity or
                directly relate to an identifiable individual, such demographic information, or statistical
                or aggregated information. Statistical or aggregated data does not directly identify a
                specific person, but we may derive non-personal statistical or aggregated data from
                personal information. For example, we may aggregate personal information to calculate
                the percentage of users using a specific Website feature; </p>
            <p class="inner">(d) technical information, including your login information, browser type and version,
                time-
                zone setting, browser plug-in types and versions, operating system and platform, or
                information about your internet connection, the equipment you use to access our Website,
                and usage details; </p>
            <p class="inner">(e) non-personal details about your Website interactions, including the full URLs,
                clickstream to, through and from our Website (including date and time), products you
                viewed or searched for, page response times, download errors, lengths of visits to certain
                pages, page interaction information (such as scrolling, clicks, and mouse-overs), methods
                used to browse away from the page, or any phone number used to call our customer
                service number.</p>

            <h4>3. HOW WE COLLECT INFORMATION ABOUT YOU</h4>

            <p>3.1 &nbsp;<b>Method of Collection.</b> We use different methods to collect your information, including
                through:</p>

            <p class="inner">(a) direct interactions with you when you provide it to us, for example, by filling in
                forms or corresponding with us by phone, email, or otherwise;
            </p>
            <p class="inner">(b) User Submissions, whereby you provide information to us through the use of our
                Services; and </p>
            <p class="inner">(c) automated technologies or interactions, as you navigate through our Website. </p>

            <p>3.2 &nbsp;<b>Information We Collect From You.</b> The information we collect directly from you or on or
                through our Website may include:
            </p>
            <p class="inner">(a) information that you provide by filling in forms on our Website, including, registering
                to use our Website or subscribing to the Services, posting materials to the Website (if applicable),
                and/or requesting further services;
            </p>
            <p class="inner">(b) records and copies of your correspondence, if you contact us;</p>
            <p class="inner">(c) your responses to surveys that we might ask you to complete for research purposes;
            </p>
            <p class="inner">(d) details of transactions you carry out through our Website and of the fulfillment of
                your orders, if applicable; and
            </p>
            <p class="inner">(e) your search queries on our Website.
            </p>

            <p>3.3 &nbsp;<b>Information We Collect Through Cookies and Other Automated Data Collection Technologies.</b>
                As you navigate through and interact with our Website, we may use cookies or other automatic data
                collection technologies to collect certain information about your equipment, browsing actions, and
                patterns, including:
            </p>
            <p class="inner">(a) details of your visits to our Website, including traffic data, location data, logs, and
                other communication data and the resources that you access and use on the Website;</p>
            <p class="inner">(b) information about your computer and Internet connection, including your IP address,
                operating system, and browser type.</p>

            <p>
                We may also use these technologies to collect information about your online activities over time and
                across third-party websites or other online services (behavioural tracking).
                To learn more or to opt out of tailored advertising please visit <a
                    href="https://youradchoices.ca/en/tools%20"> Digital Advertising Alliance of Canada Opt-Out Tool</a>
                for information on how you can opt out of behavioural
                tracking on this Website and how we respond to web browser signals and other mechanisms that enable
                consumers to exercise choice about behavioural tracking.
            </p>

            <p>The information we collect automatically is statistical data and may include personal information. It
                helps us improve our Website and deliver a better and more personalized service, including by enabling
                us to:</p>
            <p class="inner">(a) estimate our audience size and usage patterns;</p>
            <p class="inner">(b) store information about your preferences, allowing us to customize our Website
                according to your individual interests;</p>
            <p class="inner">(c) speed up your searches;</p>
            <p class="inner">(d) recognize you when you return to our Website.</p>

            <p>3.4 <b>Technology Used for Automatic Data Collection.</b> The technologies we use for this automatic data
                collection may include:</p>

            <p class="inner">(a) Cookies: a cookie is a small file placed on the hard drive of your computer. You may
                refuse to accept browser cookies by activating the appropriate setting on your browser. However, if you
                select this setting you may be unable to access certain parts of our Website or Services. Unless you
                have adjusted your browser setting so that it will refuse cookies, our system will issue cookies when
                you direct your browser to our Website.</p>
            <p class="inner">(b) Flash cookies: certain features of our Website may use local stored objects (or flash
                cookies) to collect and store information about your preferences and navigation to, from, and on our
                Website. Flash cookies are not managed by the same browser setting that are used for browser cookies.
            </p>
            <p class="inner">(c) Web beacons: certain features of our Website and our emails may contain small
                electronic files known as web beacons (also referred to as clear gifs, pixel tags, and single-pixel
                gifs) that permit Burrard Pharma to count users who have visited those pages or opened an email and for
                other related website statistics.
            </p>

            <p>3.5 <b>Third Party Use of Cookies and Other Tracking Technologies.</b> Some content or applications on
                the Website, including advertisements, are served by third parties, including advertisers, ad networks
                and servers, content providers, and application providers. These third parties may use cookies, or other
                tracking technologies, to collect information about you when you use our Website. The information they
                collect may be associated with your personal information or they may collect information, including
                personal information, about your online activities over time and across different websites and other
                online services. They may use this information to provide you with interest-based (behavioural)
                advertising or other targeted content.
            </p>
            <p>You can opt out of several third-party ad servers’ and networks’ cookies simultaneously by using the <a
                    href="https://youradchoices.ca/en/tools."> Digital Advertising Alliance of Canada Opt Out Tool.</a>
            </p>
            <p>
                You can also access this website to learn more about online behavioural advertising and how to stop
                websites from placing cookies on your device. Opting out of a network does not mean you will no longer
                receive online advertising. It does mean that the network from which you opted out will no longer
                deliver ads tailored to your web preferences and usage patterns.
            </p>

            <h4>4. HOW WE USE YOUR INFORMATION</h4>

            <p>4.1 <b>Use.</b> We use information that we collect about you or that you provide to us, including any
                personal information:</p>
            <p class="inner">(a) to present our Website and its contents to you, including to provide the Services to
                you;</p>
            <p class="inner">(b) to provide you with information that you request from us;
            </p>
            <p class="inner">(c) to fulfill the purpose for which you provided the information or that were described
                when it was collected, or for any other purpose for which you provide it;
            </p>
            <p class="inner">(d) to carry out our obligations and enforce our rights arising from any agreements with
                you, including to comply with legal requirements;
            </p>
            <p class="inner">(e) to notify you about changes to our Website, your account, or any products or services
                we offer or provide through our Website;
            </p>
            <p class="inner">(f) to improve our Website and the Services, marketing, or customer relationships and
                experiences;
            </p>
            <p class="inner">(g) in any other way we may describe when you provide the information.
            </p>
            <h4>5. DISCLOSURE OF YOUR INFORMATION</h4>
            <p>5.1 <b>Disclosure.</b> We may disclose personal information that we collect or you provide as described
                in this Privacy Policy as follows:</p>
            <p class="inner">(a) to our subsidiaries and affiliates;
            </p>
            <p class="inner">(b) in accordance with applicable law, to a buyer or other successor in the event of a
                merger, divestiture, restructuring, reorganization, dissolution, or other sale or transfer of some or
                all of Burrard Pharma’s assets, whether as a going concern or part of liquidation or similar proceeding,
                in which personal information held by Burrard Pharma about our customers and users is among the assets
                transferred;
            </p>
            <p class="inner">(c) to contractors, service providers, and other third parties we use to support our
                business (such as analytics and search engine providers that assist us with Website improvement and
                optimization) and who are contractually obligated to keep personal information confidential, use it only
                for the purposes for which we disclose it to them, and to process the personal information with the same
                standards set out in this Privacy Policy;
            </p>
            <p class="inner">(d) to fulfill the purpose for which you provide it; and
            </p>
            <p class="inner">(e) for any other purpose disclosed by us when you provide the information.
            </p>

            <p>5.2 <b>Disclosure Required.</b> We may also disclose your personal information:</p>
            <p class="inner">(a) to comply with any court order, law, or legal process, including to respond to any
                government or regulatory request, in accordance with applicable law;</p>
            <p class="inner">(b) to enforce or apply our Terms and Conditions found at
                https://burrardpharma.com/terms-and-conditions.;</p>
            <p class="inner">(c) where we believe disclosure is necessary or appropriate to protect the rights,
                property, or safety of Burrard Pharma or our users, including for the purposes of fraud protection and
                fraud prevention.</p>

            <h4>6. TRANSFERRING AND STORAGE OF PERSONAL INFORMATION</h4>
            <p>6.1 <b>Transfer to Service Providers.</b>We may transfer your personal information that we collect or
                that you provide as described in this Privacy Policy to contractors, service providers, and other third
                parties we use to support our business (such as analytics and search engine providers that assist us
                with Website improvement and optimization) and who are contractually obligated to keep personal
                information confidential, use it only for the purposes for which we disclose it to them, and to process
                the personal information with the same standards set out in this Privacy Policy.
            </p>
            <p>6.2 <b>Storage.</b> We may process, store, and transfer your personal information in and to a foreign
                country, with different privacy laws that may or may not be as comprehensive as Canadian laws. In these
                circumstances, the governments, courts, law enforcement, or regulatory agencies of that country may be
                able to obtain access to your personal information through the laws of the foreign country. Whenever we
                engage a service provider, we require that its privacy and security standards adhere to this Privacy
                Policy and applicable Canadian privacy legislation.
            </p>
            <h4>7. DATA SECURITY</h4>
            <p>7.1 <b>Security.</b>The security of your personal information is very important to us. We use physical,
                electronic, and administrative measures designed to secure your personal information from accidental
                loss and from unauthorized access, use, alteration, and disclosure. The safety and security of your
                information also depends on you. Where we have given you (or where you have chosen) a password for
                access to certain parts of our Website, you are responsible for keeping this password confidential. You
                shall not share your password with any other person. You acknowledge that the transmission of
                information via the Internet is not completely secure. We cannot guarantee the security of your personal
                information transmitted to our Website. Any transmission of personal information is at your own risk. We
                are not responsible for the circumvention of any privacy settings or security measures contained on the
                Website.</p>

            <h4>8. RETENTION</h4>
            <p>8.1 <b>Data Retention.</b> Except as otherwise permitted or required by applicable law or regulation, we
                will only retain your personal information for as long as necessary to fulfill the purposes we collected
                it for, including for the purposes of satisfying any legal, accounting, or reporting requirements.</p>

            <h4>9. CHILDREN UNDER THE AGE OF 13</h4>
            <p>9.1 <b>Age.</b> Our Website is not intended for children under 13 years of age. No one under age 13 may
                provide any information to or on the Website. We do not knowingly collect personal information from
                children under 13. If you are under the age of 13, do not use or provide any information on this Website
                or on or through any of its features/register on the Website, or provide any information about yourself
                to us, including your name, address, telephone number, email address, or any screen name or user name
                you may use. If we learn that we have collected or received personal information from a child under the
                age of 13 without verification of parental consent, we will delete that information. If you believe we
                might have any information from or about a child under 13, please contact our Privacy Officer at:
                <a href="mailto:privacy@burrardpharma.com">privacy@burrardpharma.com</a> and <a
                    href="tel:1-844-9-742762">1-844-9-742762</a>.
            </p>
            <h4>10. ACCESSING OR CORRECTING YOUR PERSONAL INFORMATION</h4>
            <p>10.1 <b>Accuracy of Personal Information.</b> It is important that the personal information we hold about
                you is accurate and current. Please keep us informed if your personal information changes. You have the
                right to request access to and to correct personal information we hold about you.</p>
            <p>10.2 <b>How to Review and Change Personal Information.</b> You can review and change your personal
                information, or withdraw consent to the use of your personal information by:
            </p>
            <p class="inner">(a) logging into your account on the Website and visiting your account profile page; or</p>
            <p class="inner">(b) send us an email at <a href="mailto:privacy@burrardpharma.com">privacy@burrardpharma.com</a>.
            </p>
            <p>We may request specific information from you to help us confirm your identity and your right to access,
                and to provide you with the personal information that we hold about you or make your requested changes.
                Applicable law may allow or require us to refuse to provide you with access to some or all of the
                personal information that we hold about you, or we may have destroyed, erased, or made your personal
                information anonymous in accordance with our record retention obligations and practices. If we cannot
                provide you with access to your personal information, we will inform you of the reasons why, subject to
                any legal or regulatory restrictions.
            </p>
            <h4>11. WITHDRAWING YOUR CONSENT</h4>
            <p>11.1 <b>Withdrawing Consent.</b> Where you have provided your consent to the collection, use, and
                transfer of your personal information, you may have the legal right to withdraw your consent under
                certain circumstances. To withdraw your consent, if applicable, contact us at <a
                    href="mailto:privacy@burrardpharma.com">privacy@burrardpharma.com</a>.
                Please note that if you withdraw your consent, we may not be able to provide you with a particular
                product or service. We will explain the impact to you at the time to help you with your decision.
            </p>
            <h4>12. AMENDMENTS TO THIS PRIVACY POLICY</h4>
            <p>12.1 <b>Amendments.</b> We will notify you if and when we make any changes to our Privacy Policy through
                our Website home page. If we make material changes to how we treat our user’s personal information, we
                will notify you by email to the primary email address specified in your account, and through a notice on
                our Website home page. Once we have notified you of any changes to our Privacy Policy, you are
                responsible for ensuring that you read and review the updated Privacy Policy.
            </p>
            <h4>13. CONTACT INFORMATION AND CHALLENGING COMPLIANCE</h4>
            <p>13.1 We welcome your questions, comments, and requests regarding this Privacy Policy and our privacy
                practices. Please contact our Privacy Officer at:
                <br/>
                <br/>
                <a
                    href="mailto:privacy@burrardpharma.com">privacy@burrardpharma.com</a><br/><a
                    href="tel:1-844-9-742762">1-844-9-742762</a>
                <br/>
                <br/>
                We have procedures in place to receive and res pond to complaints or inquiries about our handling of
                personal information, our compliance with this Privacy Policy, and with applicable privacy laws. To
                discuss our compliance with this Privacy Policy please contact our Privacy Officer using the contact
                information listed above.
            </p>
            <p style="text-align: center"><b>[End of Privacy Policy]</b></p>
        </div>
    </div>

@endsection
@section('front_js')
    <script src="{{URL::to('storage/app/public/Frontassets/js/index.js')}}"></script>

    <script src="{{URL::to('storage/app/public/Frontassets/js/objectFitPolyfill.min.js')}}"></script>
    <script>
        // Back To Top

        $(document).ready(function () {
            $('a[href^="#"]').click(function (event) {
                var id = $(this).attr("href");
                var offset = 0;
                var target = $(id).offset().top - offset;
                $('html, body').animate({scrollTop: target}, 500);
                event.preventDefault();
            });
        });


        // Pageload Delay

        function delay(URL) {
            setTimeout(function () {
                window.location = URL
            }, 1700);
        }

        $(window).bind("pageshow", function (event) {
            if (event.originalEvent.persisted) {
                window.location.reload()
            }
        });

        $(window).scroll(function () {
            if ($(this).scrollTop() > 1) {
                $("#nav-hidden").stop().fadeIn(250)
            } else {
                $("#nav-hidden").stop().fadeOut(250)
            }
        });

        function watchForHover() {
            var hasHoverClass = false;
            var container = document.body;
            var lastTouchTime = 0;

            function enableHover() {
                if (new Date() - lastTouchTime < 500) return;
                if (hasHoverClass) return;
                container.className += ' hasHover';
                hasHoverClass = true;
            }

            function disableHover() {
                if (!hasHoverClass) return;
                container.className = container.className.replace(' hasHover', '');
                hasHoverClass = false;
            }

            function updateLastTouchTime() {
                lastTouchTime = new Date();
            }

            document.addEventListener('touchstart', updateLastTouchTime, true);
            document.addEventListener('touchstart', disableHover, true);
            document.addEventListener('mousemove', enableHover, true);
            enableHover();
        }

        watchForHover();

    </script>
@endsection
@section('footer_sc')
    <div class="footer-icons">
        <a href="#privacy-wrapper"><img src="{{URL::to('storage/app/public/Frontassets/media/top.svg')}}"></a>
    </div>
@endsection
