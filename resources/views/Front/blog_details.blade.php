@extends('layouts.front')
@section('front_title')
    {{$data['title']}}
@endsection
@section('meta')
    <link rel="canonical" href="http://www.burrardpharma.com/"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Title"/>
    <meta property="og:description" content="Description"/>
    <meta property="og:url" content="http://www.burrardpharma.com/"/>
    <meta property="og:site_name" content="Burrard Pharma"/>
    <meta name="description" content="Description"/>
    <meta name="twitter:title" content="Title">
    <meta name="twitter:description" content="Description">
@endsection
@section('front_css')
    <style>
    .blog-content-text ul,.blog-content-text ol{
        margin-left: 38px;
        font-family: 'SofiaPro-Regular';
        font-size: 14px;
        font-weight: 100;
    }
    .blog-content-text h3{
        margin-left: 30px;
        font-family: 'SofiaPro-Regular';
    }
    </style>

@endsection
@section('front_content')

    <div id="blog-wrapper">
        <div class="blog-page-container">


            <div class="blog-content">
                <div class="blog-content-img">
                    <img src="{{URL::to('storage/app/public/blog/'.$data['image'])}}">
                </div>
                <div class="blog-content-title">
                    <h1>{{$data['title']}}</h1>
                </div>
                <div class="blog-content-text">
                    {!! $data['description'] !!}
                </div>
                <div class="blog-date">
                    <p>{{\Carbon\Carbon::parse($data['date'])->format('M d,Y')}}</p>
                </div>
            </div>


        </div>
    </div>

@endsection
@section('front_js')
    <script src="{{URL::to('storage/app/public/Frontassets/js/index.js')}}"></script>

    <script src="{{URL::to('storage/app/public/Frontassets/js/objectFitPolyfill.min.js')}}"></script>
    <script>
        // Back To Top

        $(document).ready(function () {
            $('a[href^="#"]').click(function (event) {
                var id = $(this).attr("href");
                var offset = 0;
                var target = $(id).offset().top - offset;
                $('html, body').animate({scrollTop: target}, 500);
                event.preventDefault();
            });
        });


        // Pageload Delay

        function delay (URL) {
            setTimeout( function() { window.location = URL }, 1700 );
        }
        $(window).bind("pageshow", function(event) {
            if (event.originalEvent.persisted) {
                window.location.reload()
            }
        });

        $(window).scroll(function() {
            if ($(this).scrollTop() > 1) {
                $("#nav-hidden").stop().fadeIn(250)
            } else {
                $("#nav-hidden").stop().fadeOut(250)
            }
        });

        function watchForHover() {
            var hasHoverClass = false;
            var container = document.body;
            var lastTouchTime = 0;

            function enableHover() {
                if (new Date() - lastTouchTime < 500) return;
                if (hasHoverClass) return;
                container.className += ' hasHover';
                hasHoverClass = true;
            }

            function disableHover() {
                if (!hasHoverClass) return;
                container.className = container.className.replace(' hasHover', '');
                hasHoverClass = false;
            }

            function updateLastTouchTime() {
                lastTouchTime = new Date();
            }

            document.addEventListener('touchstart', updateLastTouchTime, true);
            document.addEventListener('touchstart', disableHover, true);
            document.addEventListener('mousemove', enableHover, true);
            enableHover();
        }

        watchForHover();

    </script>
@endsection
@section('footer_sc')
    <div class="footer-icons">
        <a href="#blog-wrapper"><img src="{{URL::to('storage/app/public/Frontassets/media/top.svg')}}"></a>
    </div>
@endsection

