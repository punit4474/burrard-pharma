@extends('layouts.admin')
@section('title')
    Create User
@endsection
@section('css')
@endsection
@section('content')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('burrard-admin')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{URL::to('burrard-admin/user')}}">User</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Create User</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Create User</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-1">Create User Form</h4>

                    <hr/>

                  {{Form::open(array('url'=>'burrard-admin/user','method'=>'post','name'=>'create-category','files'=>'true','class'=>'needs-validation','novalidate'))}}
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">First name</label>
                                {{Form::text('first_name','',array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'First Name','required'))}}

                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Last Name</label>
                                {{Form::text('last_name','',array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Last Name','required'))}}

                            </div>

                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Email</label>
                                {{Form::text('email','',array('class'=>'form-control','id'=>'validationCustom03','placeholder'=>'Email','required'))}}

                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-lg-4">

                            <div class="form-group mb-3">
                                   <label for="validationCustom01">Password</label>
                                {{Form::password('password',array('class'=>'form-control','id'=>'validationCustom04','placeholder'=>'password','required'))}}

                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Phone Number</label>
                                {{Form::text('phone_number','',array('class'=>'form-control','id'=>'validationCustom03','placeholder'=>'Phone Number','required'))}}

                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">organization</label>
                                {{Form::text('organization','',array('class'=>'form-control','id'=>'validationCustom03','placeholder'=>'Organization','required'))}}

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4">

                            <div class="form-group mb-3">
                                   <label for="validationCustom01">Roles</label>
                                {{Form::select('roles',array(''=>'Select Role','1'=>'Admin','2'=>'User'),'',array('class'=>'form-control','id'=>'validationCustom04','required'))}}

                            </div>
                        </div>


                    </div>

                    {{Form::submit('Submit',array('class'=>'btn btn-primary'))}}
                    <a href="{{URL::to('burrard-admin/user')}}" class="btn btn-danger" >Cancel</a>
                    {{Form::close()}}

                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
@endsection

@section('plugin')
    <!-- Plugin js-->
    <script src="{{URL::to('storage/app/public/Adminassets/libs/parsleyjs/parsley.min.js')}}"></script>
@endsection
@section('js')
    <!-- Validation init js-->
    <script src="{{URL::to('storage/app/public/Adminassets/js/pages/form-validation.init.js')}}"></script>

@endsection

