@extends('layouts.admin')
@section('title')
Edit Order Status
@endsection
@section('css')
@endsection
@section('content')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{URL::to('burrard-admin')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{URL::to('burrard-admin/order')}}">Orders</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit Order Status</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Edit Order Status</h4>
    </div>
</div>
<div class="row">
    <div class="col-xl-4">
        <div class="card">
            <div class="row no-gutters align-items-center">
                <div class="card-body">
                 <h5>Order Id: {{$data['order_id']}}</h5>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4">
        <div class="card">
            <div class="row no-gutters align-items-center">
                <div class="card-body">
                    <h5>Name: {{$data['name']}}</h5>
                    <h5>Email: {{$data['email']}}</h5>
                </div>
            </div>
        </div>

    </div>

    <div class="col-xl-4">
        <div class="card">
            <div class="row no-gutters align-items-center">
                <div class="card-body">
                    <h5>Amount: ${{$data['payment_amount']}}</h5>
                    <h5>Date: ${{$data['order_date']}}</h5>
                </div>
            </div>
        </div>
    </div>
</div>
<h5>Report Details</h5>
<hr/>
<div class="row">
        @foreach($data['report'] as $row)
        <div class="col-xl-4">
            <div class="card">
                <div class="row no-gutters align-items-center">
                        <div class="col-md-7">
                        <div class="card-body">
                        <h5 class="card-title font-size-16">{{$row->name}} ({{$row->dosage_form}})</h5>
                            <p class="card-text text-muted">{{$row->strengh}}</p>
                        <p class="card-text">{{$row->atc_code}} (<small class="text-muted">{{$row->route_of_admin}}</small>)</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        @endforeach
</div>
<h5>Order Status</h5>
<hr/>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
            {{Form::open(array('url'=>'burrard-admin/order/'.$data['id'],'method'=>'PUT','name'=>'edit-order','files'=>'true','class'=>'needs-validation','novalidate'))}}
            <div class="row">
                <div class="col-lg-4">

                    <div class="form-group mb-3">
                           <label for="validationCustom01">Status</label>
                        {{Form::select('order_status',array(''=>'Select Status','pending'=>'Pending','processing'=>'Processing','success'=>'Success','failed'=>'Failed'),$data['order_status'],array('class'=>'form-control','id'=>'validationCustom04','required'))}}

                    </div>
                </div>
            </div>


            {{Form::submit('Submit',array('class'=>'btn btn-primary'))}}
            <a href="{{URL::to('burrard-admin/report')}}" class="btn btn-danger" >Cancel</a>
            {{Form::close()}}
        </div> <!-- end card-body-->
        </div> <!-- end card-->
    </div> <!-- end col-->
</div>
@endsection

@section('plugin')
    <!-- Plugin js-->
    <script src="{{URL::to('storage/app/public/Adminassets/libs/parsleyjs/parsley.min.js')}}"></script>
@endsection
@section('js')
    <!-- Validation init js-->
    <script src="{{URL::to('storage/app/public/Adminassets/js/pages/form-validation.init.js')}}"></script>

@endsection
