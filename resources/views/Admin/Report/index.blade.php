@extends('layouts.admin')
@section('title')
    Report
@endsection
@section('css')
    <!-- plugin css -->
    <link href="{{URL::to('storage/app/public/Adminassets/libs/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL::to('storage/app/public/Adminassets/libs/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right">
                <a href="{{URL::to('burrard-admin/reports/bulk')}}" class="btn btn-primary text-white">+ Bulk Upload</a>
                <a href="{{URL::to('burrard-admin/report/create')}}" class="btn btn-primary text-white">+ Create Report</a>

            </nav>
            <h4 class="mb-1 mt-1">Report</h4>
        </div>
    </div>
    <div class="row">
                <div class="col-md-12">
                    @if(Session::has('message'))
                        {!! Session::get('message') !!}
                    @endif
                </div>
            </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-1">Report</h4>


                    <table id="basic-datatable" class="table dt-responsive nowrap">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Drug Name</th>
                            <th>Dosage Form</th>
                            <th>Strengh</th>
                            <th>Route of Admin</th>
                            <th>ATC Code</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>


                        <tbody>

                            @foreach($data as $row)
                            <tr>
                                <td>{{$row->id}}</td>
                                <td>{{$row->name}}</td>
                                <td>{{$row->dosage_form}}</td>
                                <td>{{$row->strengh}}</td>
                                <td>{{$row->route_of_admin}}</td>
                                <td>{{$row->atc_code}}</td>
                                <td>
                                    <a href="{{URL::to('burrard-admin/report/'.$row->id.'/status')}}"> <label
                                        class="badge badge-soft-{{$row->status == 'active' ? 'success' : 'danger'}}">{{$row->status}}</label>
                                </a>
                                </td>

                                <td>
                                    <a class="" href="{{URL::to('burrard-admin/report/'.$row->id.'/edit')}}"> <i class="uil-pen"></i></a>
                                    <a class="" href="{{URL::to('burrard-admin/report/'.$row->id.'/destroy')}}"><i class="uil-trash-alt"></i></a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
@endsection

@section('plugin')
    <!-- datatable js -->
    <script src="{{URL::to('storage/app/public/Adminassets/libs/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::to('storage/app/public/Adminassets/libs/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{URL::to('storage/app/public/Adminassets/libs/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{URL::to('storage/app/public/Adminassets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>


@endsection
@section('js')
    <!-- Datatables init -->
    <script src="{{URL::to('storage/app/public/Adminassets/js/pages/datatables.init.js')}}"></script>
@endsection
