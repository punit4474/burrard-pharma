@extends('layouts.admin')
@section('title')
    Edit Report
@endsection
@section('css')
@endsection
@section('content')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('burrard-admin')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{URL::to('burrard-admin/report')}}">Report</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Report</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Edit Report</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-1">Edit Report Form</h4>

                    <hr/>

                  {{Form::open(array('url'=>'burrard-admin/report/'.$data['id'],'method'=>'PUT','name'=>'Edit-report','files'=>'true','class'=>'needs-validation','novalidate'))}}
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Drug Name</label>
                                {{Form::text('name',$data['name'],array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Drug Name','required'))}}

                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Dosage Form</label>
                                {{Form::text('dosage_form',$data['dosage_form'],array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Dosage Form','required'))}}

                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Strengh</label>
                                {{Form::text('strengh',$data['strengh'],array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Strengh','required'))}}

                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Route of Admin</label>
                                {{Form::text('route_of_admin',$data['route_of_admin'],array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'Route of Admin','required'))}}

                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">ATC Code</label>
                                {{Form::text('atc_code',$data['atc_code'],array('class'=>'form-control','id'=>'validationCustom01','placeholder'=>'ATC Code','required'))}}

                            </div>
                        </div>
                        <div class="col-lg-4">

                            <div class="form-group mb-3">
                                   <label for="validationCustom01">Status</label>
                                {{Form::select('status',array(''=>'Select Status','active'=>'Active','inactive'=>'Inactive'),$data['status'],array('class'=>'form-control','id'=>'validationCustom04','required'))}}

                            </div>
                        </div>
                    </div>


                    {{Form::submit('Update',array('class'=>'btn btn-primary'))}}
                    <a href="{{URL::to('burrard-admin/report')}}" class="btn btn-danger" >Cancel</a>
                    {{Form::close()}}

                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
@endsection

@section('plugin')
    <!-- Plugin js-->
    <script src="{{URL::to('storage/app/public/Adminassets/libs/parsleyjs/parsley.min.js')}}"></script>
@endsection
@section('js')
    <!-- Validation init js-->
    <script src="{{URL::to('storage/app/public/Adminassets/js/pages/form-validation.init.js')}}"></script>

@endsection

