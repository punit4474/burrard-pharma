@extends('layouts.admin')
@section('title')
    Create Bulk Report
@endsection
@section('css')
@endsection
@section('content')
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{URL::to('burrard-admin')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{URL::to('burrard-admin/report')}}">Report</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Create Bulk Report</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Create Report</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-1">Create Bulk Report Form</h4>
                    <hr/>

                  {{Form::open(array('url'=>'burrard-admin/reports/bulk','method'=>'post','name'=>'create-report','files'=>'true','class'=>'needs-validation','novalidate'))}}
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">Drug Name</label>
                                {{Form::file('excel',array('class'=>'form-control','id'=>'validationCustom01','required'))}}

                            </div>
                        </div>

                    </div>


                    {{Form::submit('Submit',array('class'=>'btn btn-primary'))}}
                    <a href="{{URL::to('burrard-admin/report')}}" class="btn btn-danger" >Cancel</a>
                    {{Form::close()}}

                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
@endsection

@section('plugin')
    <!-- Plugin js-->
    <script src="{{URL::to('storage/app/public/Adminassets/libs/parsleyjs/parsley.min.js')}}"></script>
@endsection
@section('js')
    <!-- Validation init js-->
    <script src="{{URL::to('storage/app/public/Adminassets/js/pages/form-validation.init.js')}}"></script>

@endsection

