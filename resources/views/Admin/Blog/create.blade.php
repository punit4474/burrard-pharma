@extends('layouts.admin')
@section('title')
    Create Blog
@endsection
@section('css')
    <script src="https://cdn.ckeditor.com/ckeditor5/21.0.0/classic/ckeditor.js"></script>
    <style>
        span .invalid-feedback {
            display: block !important;
        }

    </style>
@endsection
@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18">Create Blog</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="{{URL::to('burrard-admin/blog')}}">Blogs</a></li>
                                <li class="breadcrumb-item active">Create Blog</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            {{Form::open(array('url'=>'burrard-admin/blog','method'=>'post','name'=>'create-blog','class'=>'needs-validation','files'=>'true','novalidate'))}}

                            <div class="form-group row ">
                                <div class="col-sm-4">
                                    <label>Title</label>
                                    {{Form::text('title','',array('class'=>$errors->has('title') ?'form-control is-invalid' : 'form-control','placeholder'=>'Enter Title','required'))}}
                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="col-sm-4">
                                    <label>Image</label>
                                    <div class="custom-file">
                                        {{Form::file('image',array('class'=>$errors->has('image') ?'custom-file-input is-invalid' : 'custom-file-input','id'=>'customFile'))}}
                                        <label class="custom-file-label" for="customFile">Choose file</label>

                                    </div>
                                    @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <div class="form-group mb-3">
                                        <label for="validationCustom01">Description</label>

                                        {{Form::textarea('description','',array('class'=>'form-control','id'=>'editor','placeholder'=>'Description','required','novalidate','rows'=>'8'))}}
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">

                                    <div class="form-group mb-3">
                                        <label for="validationCustom01">Date</label>
                                        {{Form::date('date','',array('class'=>'form-control','id'=>'validationCustom04','required'))}}

                                    </div>
                                </div>
                            </div>


                            <div class="form-group mb-0">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                        Submit
                                    </button>
                                    <a href="{{URL::to('burrard-admin/blog')}}" type="reset"
                                       class="btn btn-secondary waves-effect">
                                        Cancel
                                    </a>
                                </div>
                            </div>
                            {{Form::close()}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('plugin')
    <!-- Plugin js-->
    <script src="{{URL::to('storage/app/public/Adminassets/libs/parsleyjs/parsley.min.js')}}"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'), {
                toolbar: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote']
            })
            .catch(error => {
                console.log(error);
            });
    </script>
    <script>
        $('input[type="file"]').change(function (e) {

            var fileName = e.target.files[0].name;
            $('.custom-file-label').text(fileName);

        });

    </script>
@endsection
@section('js')


    <script src="{{URL::to('storage/app/public/Adminassets/js/pages/form-validation.init.js')}}"></script>

@endsection
