@extends('layouts.admin')
@section('title')
    Blogs
@endsection
@section('css')
    <!-- plugin css -->
    <link href="{{URL::to('storage/app/public/Adminassets/libs/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL::to('storage/app/public/Adminassets/libs/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between mb-4">
                        <h4 class="mb-0 font-size-18">Blogs List</h4>

{{--                        <div class="page-title-right">--}}
                            <nav aria-label="breadcrumb" class="float-right">
                                <a href="{{URL::to('burrard-admin/blog/create')}}" class="btn btn-primary text-white">+ Create Blog</a>
                            </nav>
{{--                        </div>--}}

                    </div>
                </div>
            </div>
            <!-- end page title -->
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('message'))
                        {!! Session::get('message') !!}
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">


                            <table id="datatable-buttons"
                                   class="table table-striped table-bordered dt-responsive nowrap"
                                   style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                <tr>
                                    <th>#ID</th>
                                    <th>Title</th>
                                    <th>Image</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>


                                <tbody>
                                @foreach($data as $row)
                                <tr>
                                    <td>{{$row->id}}</td>
                                    <td>{{substr($row->title,0,100)}}..</td>
                                    <td>
                                        @if(file_exists(storage_path('app/public/blog/'.$row->image)) && $row->image != '')
                                            <img src="{{URL::to('storage/app/public/blog/'.$row->image)}}" class="rounded avatar-sm"
                                                 alt="blog">
                                        @else
                                            <img src="{{URL::to('storage/app/public/default/default_blog.jpg')}}" class="rounded avatar-sm"
                                                 alt="blog">
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{URL::to('burrard-admin/blog/'.$row->id.'/status')}}">
                                        <span class="badge badge-pill  {{$row->status == 'active' ? 'badge-success' : 'badge-danger'}}">{{$row->status}}</span>
                                        </a>
                                    </td>
                                    <td>
                                        <a class="" href="{{URL::to('burrard-admin/blog/'.$row->id.'/edit')}}" ><i class="uil-pen"></i></a>
                                        <a class="" href="{{URL::to('burrard-admin/blog/'.$row->id.'/destroy')}}" ><i class="uil-trash-alt"></i></a>

                                    </td>
                                </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->

        </div> <!-- container-fluid -->
    </div>
@endsection
@section('plugin')
    <!-- Required datatable js -->
    <script src="{{URL::to('storage/app/public/Adminassets/libs/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::to('storage/app/public/Adminassets/libs/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{URL::to('storage/app/public/Adminassets/libs/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{URL::to('storage/app/public/Adminassets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
@endsection
@section('js')
    <!-- Datatables init -->
    <script src="{{URL::to('storage/app/public/Adminassets/js/pages/datatables.init.js')}}"></script>
@endsection
