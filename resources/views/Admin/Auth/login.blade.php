<!DOCTYPE html>
<html lang="en">


<head>
        <meta charset="utf-8" />
        <title>Login - Burrard Pharma</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Burrard Pharma" name="description" />
        <meta content="Burrard Pharma" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="{{URL::to('storage/app/public/Adminassets/images/favicon.ico')}}">

        <!-- App css -->
        <link href="{{URL::to('storage/app/public/Adminassets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::to('storage/app/public/Adminassets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{URL::to('storage/app/public/Adminassets/css/app.min.css')}}" rel="stylesheet" type="text/css" />

    </head>

    <body class="authentication-bg">

        <div class="account-pages my-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-10">
                        <div class="card">
                            <div class="card-body p-0">
                                <div class="row">
                                    <div class="col-md-6 p-5">
                                        <div class="mx-auto mb-5">
                                            <a href="{{URL::to('burrard-admin')}}">
                                                <img src="{{URL::to('storage/app/public/Frontassets/logo.svg')}}" alt="" height="40" />
                                            </a>
                                        </div>

                                        <h6 class="h5 mb-0 mt-4">Welcome back!</h6>
                                        <p class="text-muted mt-1 mb-4">Enter your email address and password to
                                            access admin panel.</p>
                                        {!! Form::open(array('url'=>'burrard-admin/login','method'=>'post','name'=>'login','class'=>'authentication-form')) !!}

                                            <div class="form-group">
                                                <label class="form-control-label">Email Address</label>
                                                <div class="input-group input-group-merge">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="icon-dual" data-feather="mail"></i>
                                                        </span>
                                                    </div>
                                                    {!! Form::email('email', '', ['class'=>'form-control','id'=>'email','placeholder'=>"hello@email.com",'required']) !!}
                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group mt-4">
                                                <label class="form-control-label">Password</label>

                                                <div class="input-group input-group-merge">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="icon-dual" data-feather="lock"></i>
                                                        </span>
                                                    </div>
                                                    {!! Form::password('password', ['class'=>'form-control','id'=>'password','required','placeholder'=>'Enter your Password']) !!}
                                                    @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>


                                            <div class="form-group mb-0 text-center">
                                                <button class="btn btn-primary btn-block" type="submit"> Log In
                                                </button>
                                            </div>
                                        {!! Form::close() !!}

                                    </div>
                                    <div class="col-lg-6 d-none d-md-inline-block">
                                        <div class="auth-page-sidebar" style="background-image: url({{URL::to('storage/app/public/Frontassets/media/about-img.jpg')}})">
                                            <div class="overlay"></div>
{{--                                            <div class="auth-user-testimonial">--}}
{{--                                                <p class="font-size-24 font-weight-bold text-white mb-1">I simply love it!</p>--}}
{{--                                                <p class="lead">"It's a elegent templete. I love it very much!"</p>--}}
{{--                                                <p>- Admin User</p>--}}
{{--                                            </div>--}}
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->


                        <!-- end row -->

                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->

        <!-- Vendor js -->
        <script src="{{URL::to('storage/app/public/Adminassets/js/vendor.min.js')}}"></script>

        <!-- App js -->
        <script src="{{URL::to('storage/app/public/Adminassets/js/app.min.js')}}"></script>

    </body>
</html>
