<!-- footer -->
<div id="footer-wrapper">
    <div class="footer-container">
        <div class="footer-top">
            <div class="footer-left">
                <a class="footer-logo" href="{{URL::to('/')}}">
                    <img src="{{URL::to('storage/app/public/Frontassets/media/logo-white')}}.svg">
                </a><br/>
                <div class="footer-icons">
                    <p>Follow us</p>
                    <div>
                        <a href="https://www.linkedin.com/company/burrardpharmaceuticals" target="_blank"><img src="{{URL::to('storage/app/public/Frontassets/media/icons8-linkedin.svg')}}"></a>
                        <a href="https://twitter.com/burrardpharma" target="_blank"><img src="{{URL::to('storage/app/public/Frontassets/media/twitter.svg')}}"></a>
                        <a href="https://www.instagram.com/burrardpharmaceuticals/" target="_blank"><img src="{{URL::to('storage/app/public/Frontassets/media/instagram.svg')}}"></a>
                    </div>
                </div>
            </div>
            <div class="footer-right">
                <div class="footer-items">
                    <p>Services</p>
                    <a href="{{URL::to('service')}}#drug-formulation">Drug Formulation</a>
                    <a href="{{URL::to('service')}}#know-how">Know How</a>
                    <a href="{{URL::to('service')}}#drug-manufacturing">Drug Manufacturing</a>
                    <a href="{{URL::to('service')}}#technology-transfer">Technology Transter</a>
                </div>

                <div class="footer-items">
                    <p>Company</p>
                    <a href="{{URL::to('company')}}#about-wrapper">About Us</a>
                    <a href="{{URL::to('company')}}#center-wrapper">Our Team</a>
                    <a href="{{URL::to('company')}}#career-wrapper">Careers</a>
                    <a href="{{URL::to('company')}}#contact-wrapper">Contact Us</a>
                </div>

            </div>
        </div>
        {{-- <div class="footer-center">
            <div class="footer-left"></div>
            <div class="footer-right">
                <div class="twitter-items">
                    <blockquote class="twitter-tweet" data-theme="dark"><p lang="en" dir="ltr">The <a href="https://twitter.com/hashtag/Covid?src=hash&amp;ref_src=twsrc%5Etfw">#Covid</a>-19 <a href="https://twitter.com/hashtag/pandemic?src=hash&amp;ref_src=twsrc%5Etfw">#pandemic</a> is one of the greatest challenges modern medicine has ever faced. Doctors and scientists are scrambling to find treatments and drugs that can save the lives of infected people and perhaps even prevent infection. <a href="https://t.co/DY9E9d6lfO">https://t.co/DY9E9d6lfO</a></p>&mdash; Burrard Pharma (@BurrardPharma) <a href="https://twitter.com/BurrardPharma/status/1283981560350302208?ref_src=twsrc%5Etfw">July 17, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
            </div>
        </div> --}}
        <div class="footer-bottom">
            <div class="footer-left">
                <div class="footer-items">
                    <p>Website By <a href="https://www.hoomanstudio.com" target="_blank">Hooman Studio</a></p>
                </div>
            </div>
            <div class="footer-right">
                <div class="footer-items">
                    <p>&copy;<span id="year"></span> All Rights Reserved <span>&mdash;</span> <a href="{{URL::to('privacy-policy')}}">Privacy & Policy</a> &nbsp; & &nbsp;<a href="{{URL::to('terms-and-conditions')}}">Terms and Condition</a></p>
                </div>
                @yield('footer_sc')

            </div>
        </div>
    </div>
</div>

<!-- footer -->
