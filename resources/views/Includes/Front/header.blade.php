<!-- navbar -->
<div class="navbar">

    @if(Request::url() == URL::to('/'))
    <div id="nav-hidden"></div>
    @else
    <div id="nav-shown"></div>
    @endif
    <div class="nav-pc">
        <a class="logo" href="{{URL::to('/')}}">
            <svg fill="currentcolor" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 250 36" style="enable-background:new 0 0 250 36;" xml:space="preserve">
            <g>
            <path d="M231.95,18c0-4.99,4.04-9,9.02-9c4.99,0,9.02,4.01,9.02,9s-4.04,9-9.02,9C235.99,27,231.95,22.99,231.95,18z M249.33,18
            c0-4.66-3.74-8.4-8.35-8.4s-8.37,3.74-8.37,8.4s3.76,8.4,8.37,8.4S249.33,22.66,249.33,18z M236.22,18c0-2.67,2.07-4.79,4.84-4.79
            c1.79,0,3.29,0.92,4.06,2.32l-1.05,0.6c-0.55-1.12-1.75-1.74-3.02-1.74c-2.17,0-3.59,1.7-3.59,3.62s1.42,3.61,3.59,3.61
            c1.3,0,2.49-0.62,3.02-1.77l1.07,0.62c-0.77,1.4-2.29,2.32-4.09,2.32C238.28,22.79,236.22,20.67,236.22,18z"/>
            <g>
            <path d="M130.92,26.71h4.5V16.5c0-1.66,1.35-3,3-3c1.66,0,3,1.34,3,3v10.21h4.5V16.5c0-4.14-3.36-7.5-7.5-7.5
            c-1.07,0-2.08,0.23-3,0.64V0h-4.5V26.71z"/>
            <path d="M198.21,9c-2.04,0-3.9,0.83-5.25,2.16c-1.35-1.33-3.2-2.16-5.25-2.16c-4.14,0-7.5,3.36-7.5,7.5v10.21h4.5V16.5
            c0-1.66,1.35-3,3-3c1.66,0,3,1.34,3,3v10.21h4.5V16.5c0-1.66,1.34-3,3-3s3,1.34,3,3v10.21h4.5V16.5
            C205.71,12.36,202.35,9,198.21,9z"/>
            <path d="M169.72,15.99v10.73h4.5V15.99c0-1.37,1.12-2.49,2.49-2.49H178V9h-1.29C172.85,9,169.72,12.13,169.72,15.99z"/>
            <path d="M128.21,18c0-4.96-4.04-9-9-9s-9,4.04-9,9v18h4.5V25.79c1.32,0.77,2.86,1.21,4.5,1.21C124.18,27,128.21,22.96,128.21,18z
            M123.71,18c0,2.48-2.02,4.5-4.5,4.5s-4.5-2.02-4.5-4.5s2.02-4.5,4.5-4.5C121.7,13.5,123.71,15.52,123.71,18z"/>
            <path d="M161.71,26.7h4.5v-8.71c0-4.96-4.04-8.99-9-8.99s-9,4.03-9,8.99s4.04,9.25,9,9c2.08-0.1,3.53-0.96,4.5-2.17V26.7z
            M157.21,22.49c-2.48,0-4.5-2.02-4.5-4.5s2.02-4.5,4.5-4.5s4.5,2.02,4.5,4.5S159.69,22.49,157.21,22.49z"/>
            <path d="M221.5,26.7h4.5v-8.71c0-4.96-4.04-8.99-9-8.99s-9,4.03-9,8.99s4.04,9.25,9,9c2.08-0.1,3.53-0.96,4.5-2.17V26.7z
            M217,22.49c-2.48,0-4.5-2.02-4.5-4.5s2.02-4.5,4.5-4.5s4.5,2.02,4.5,4.5S219.48,22.49,217,22.49z"/>
            <path d="M30.82,19.5c0,1.66-1.35,3-3,3s-3-1.34-3-3V9.29h-4.5V19.5c0,4.14,3.36,7.5,7.5,7.5s7.5-3.36,7.5-7.5V9.29h-4.5V19.5z"/>
            <path d="M38.68,15.99v10.73h4.5V15.99c0-1.37,1.11-2.49,2.49-2.49h1.29V9h-1.29C41.82,9,38.68,12.13,38.68,15.99z"/>
            <path d="M49.18,15.99v10.73h4.5V15.99c0-1.37,1.11-2.49,2.49-2.49h1.29V9h-1.29C52.31,9,49.18,12.13,49.18,15.99z"/>
            <path d="M79.79,15.99v10.73h4.5V15.99c0-1.37,1.11-2.49,2.48-2.49h1.29V9h-1.29C82.92,9,79.79,12.13,79.79,15.99z"/>
            <path d="M71.78,26.7h4.5v-8.71c0-4.96-4.04-8.99-9-8.99s-9,4.03-9,8.99s4.04,9.25,9,9c2.08-0.1,3.53-0.96,4.5-2.17V26.7z
            M67.28,22.49c-2.48,0-4.5-2.02-4.5-4.5s2.02-4.5,4.5-4.5s4.5,2.02,4.5,4.5S69.76,22.49,67.28,22.49z"/>
            <path d="M9,9c-1.64,0-3.18,0.44-4.5,1.21V0H0v18c0,4.96,4.04,9,9,9s9-4.04,9-9S13.96,9,9,9z M9,22.5c-2.48,0-4.5-2.02-4.5-4.5
            s2.02-4.5,4.5-4.5s4.5,2.02,4.5,4.5S11.48,22.5,9,22.5z"/>
            <path d="M88.88,18c0,4.96,4.04,9,9,9s9-4.04,9-9V0h-4.5v10.21C101.06,9.44,99.52,9,97.88,9C92.92,9,88.88,13.04,88.88,18z
            M93.38,18c0-2.48,2.02-4.5,4.5-4.5s4.5,2.02,4.5,4.5s-2.02,4.5-4.5,4.5C95.4,22.5,93.38,20.48,93.38,18z"/>
            </g>
            </g>
            </svg>
        </a>
        <div class="menu-btn"><span></span></div>
        <div class="nav-links">
            <div class="drop-box">
                <div class="nav-link"><a href="{{URL::to('service')}}">Services</a></div>
                <div class="space"></div>
                <div class="drop-items">
                    <a href="{{URL::to('service')}}#drug-formulation">Drug Formulation</a>
                    <a href="{{URL::to('service')}}#know-how">Know How</a>
                    <a href="{{URL::to('service')}}#drug-manufacturing">Drug manufacturing</a>
                    <a href="{{URL::to('service')}}#technology-transfer">Technology transter</a>
                </div>
            </div>

            <div class="drop-box">
                <div class="nav-link"><a href="{{URL::to('company')}}">Company</a></div>
                <div class="space"></div>
                <div class="drop-items">
                    <a href="{{URL::to('company')}}#about-wrapper">About us</a>
                    <a href="{{URL::to('company')}}#center-wrapper">Our Team</a>
                    <a href="{{URL::to('company')}}#career-wrapper">Careers</a>
                    <a href="{{URL::to('company')}}#contact-wrapper">Contact Us</a>
                </div>
            </div>
            <div class="nav-link"><a href="{{URL::to('pricing')}}">Pricing</a></div>
            <div class="nav-link"><a href="{{URL::to('press')}}">Press</a></div>
            @if(Auth::check())
                <div class="drop-box">
                    <div class="nav-link"><a href="{{URL::to('user-profile')}}">{{Auth::user()->first_name}} {{Auth::user()->last_name}}</a></div>
                    <div class="space"></div>
                    <div class="drop-items">
                        <a href="{{URL::to('logout')}}">Logout</a>
                    </div>
                </div>
                <div class="logout-btn">
                    <a href="{{URL::to('logout')}}">Logout</a>
                </div>
            @else
                <div class="nav-link">
                    <a href="{{URL::to('/login')}}">Login</a>
                </div>
               
            @endif
            <div class="nav-link m-block">
                    <a href="{{URL::to('company')}}#contact-wrapper">Contact Us</a>
                </div>
            <div class="nav-btn">
                <div class="generator"><a href="{{URL::to('/generator')}}">Generator</a></div>
            </div>
        </div>
    </div>
</div>
<!-- navbar -->
