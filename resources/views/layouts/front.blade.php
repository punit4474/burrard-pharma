<!DOCTYPE html>
<html>
<head>
	<title>@yield('front_title') - Burrard Pharma</title>
    <script src="{{URL::to('storage/app/public/Frontassets/js/jquery-3.4.1.min.js')}}"></script>
	<link rel="stylesheet" type="text/css" href="{{URL::to('storage/app/public/Frontassets/style.css?ver=1.1')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::to('storage/app/public/Frontassets/responsive.css')}}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @yield('meta')

    <meta name="theme-color" content="#fdfbf3">
    <meta name="msapplication-navbutton-color" content="#fdfbf3">
    <meta name="apple-mobile-web-app-status-bar-style" content="#fdfbf3">
    <link rel="apple-touch-icon" sizes="57x57" href="{{URL::to('storage/app/public/Frontassets/media/favs/57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{URL::to('storage/app/public/Frontassets/media/favs/60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{URL::to('storage/app/public/Frontassets/media/favs/72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::to('storage/app/public/Frontassets/media/favs/76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{URL::to('storage/app/public/Frontassets/media/favs/114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{URL::to('storage/app/public/Frontassets/media/favs/120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{URL::to('storage/app/public/Frontassets/media/favs/144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{URL::to('storage/app/public/Frontassets/media/favs/152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{URL::to('storage/app/public/Frontassets/media/favs/180x180.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{URL::to('storage/app/public/Frontassets/media/favs/32x32.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{URL::to('storage/app/public/Frontassets/media/favs/192x192.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{URL::to('storage/app/public/Frontassets/media/favs/16x16.png')}}">
    @yield('front_css')
</head>
<body>

    @include('Includes.Front.header')

    @yield('front_content')

    @include('Includes.Front.footer')

<!-- jquery -->


@yield('front_js')
<!-- Start of LiveChat (www.livechatinc.com) code -->
<script>
    window.__lc = window.__lc || {};
    window.__lc.license = 12127452;
    ;(function(n,t,c){function i(n){return e._h?e._h.apply(null,n):e._q.push(n)}var e={_q:[],_h:null,_v:"2.0",on:function(){i(["on",c.call(arguments)])},once:function(){i(["once",c.call(arguments)])},off:function(){i(["off",c.call(arguments)])},get:function(){if(!e._h)throw new Error("[LiveChatWidget] You can't use getters before load.");return i(["get",c.call(arguments)])},call:function(){i(["call",c.call(arguments)])},init:function(){var n=t.createElement("script");n.async=!0,n.type="text/javascript",n.src="https://cdn.livechatinc.com/tracking.js",t.head.appendChild(n)}};!n.__lc.asyncInit&&e.init(),n.LiveChatWidget=n.LiveChatWidget||e}(window,document,[].slice))
</script>

<script>

$(".menu-btn").click(function () {
  $('html').toggleClass('show-menu');
});

</script>

<noscript><a href="https://www.livechatinc.com/chat-with/12127452/" rel="nofollow">Chat with us</a>, powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a></noscript>
<!-- End of LiveChat code -->

</body>

</html>
