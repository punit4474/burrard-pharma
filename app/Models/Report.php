<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = [
        'name','description','status','dosage_form','strengh','route_of_admin','atc_code'
    ];

    protected $table = 'reports';
}
