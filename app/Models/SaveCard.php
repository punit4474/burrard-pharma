<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaveCard extends Model
{
    protected $fillable = [
        'user_id','card_id','card_brand','exp_month','exp_year','fingerprint','last4','funding'
    ];

    protected $table = 'save_card';
}
