<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'order_id','payment_id','amount','payment_date','status','payment_method'
    ];

    protected $table = 'payments';
}
