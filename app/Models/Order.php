<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'order_id','name','email','report_id','payment_amount','order_date','order_status','payment_id','user_id','billing_address','city','country','postalcode','phone_number'
    ];

    protected $table = 'orders';
}
