<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = [
        'user_id','name','email','subscription_id','amount','start_date','end_date','status','billing_address','city','country','postalcode','phone_number'
    ];

    protected $table = 'subscription';
}
