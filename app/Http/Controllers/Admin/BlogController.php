<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;
use Session;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Blog::get();
        return view('Admin.Blog.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required',
            'image'=>'required|mimes:jpeg,bmp,png,gif',
            'description'=>'required',
            'date'=>'required'
        ]);

        $data = $request->all();

        if ($request->file('image')) {

            $file = $request->file('image');
            $filename = 'blog-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move(storage_path('app/public/blog/'), $filename);
            $data['image'] = $filename;
        }

        $data['date'] = \Carbon\Carbon::parse($request['date'])->format('Y-m-d');
        $data['slug']= strtolower($request['title']);
        $data['slug']=  preg_replace('/[^A-Za-z0-9\-]/', '',preg_replace("/\s+/", "-",  $data['slug']));
        $blog = new Blog();
        $blog->fill($data);
        if($blog->save()){
            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Blog added successfully. </div>');
            return redirect('burrard-admin/blog');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Blog::findorFail($id);
        return view('Admin.Blog.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required',
            'image'=>'mimes:jpeg,bmp,png,gif',
            'description'=>'required',
            'status'=>'required',
        ]);

        $data = $request->all();
        $data = $request->except('_token', '_method');

        if ($request->file('image')) {

            $oldimage = Blog::where('id', $id)->value('image');

            if (!empty($oldimage)) {

                File::delete('storage/app/public/blog/' . $oldimage);
            }

            $file = $request->file('image');
            $filename = 'blog-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move('storage/app/public/blog', $filename);
            $data['image'] = $filename;
        }

        $data['date'] = \Carbon\Carbon::parse($request['date'])->format('Y-m-d');
        $data['slug']= strtolower($request['title']);
        $data['slug']= preg_replace('/[^A-Za-z0-9\-]/', '',preg_replace("/\s+/", "-",  $data['slug']));

        $blog = Blog::where('id',$id)->update($data);
        if($blog){
            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Blog updated successfully. </div>');
            return redirect('burrard-admin/blog');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $oldimage = Blog::where('id', $id)->value('image');

        if (!empty($oldimage)) {

            File::delete('storage/app/public/blog/' . $oldimage);
        }

        $delete = Blog::where('id',$id)->delete();

        if ($delete) {
            Session::flash('message', '<div class="alert alert-danger"><strong>Success!</strong> Blogs delete successfully. </div>');

            return redirect('burrard-admin/blog');
        }
    }

    /*
       Change user Status
   */
    public function statusChange($id){
        $users  = Blog::findorFail($id);

        if($users['status'] == 'active'){
            $newStatus = 'inactive';
        } else {
            $newStatus = 'active';
        }

        $update = Blog::where('id',$id)->update(['status'=>$newStatus]);

        if($update){
            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Blogs status updated successfully. </div>');

            return redirect('burrard-admin/blog');
        }
    }
}
