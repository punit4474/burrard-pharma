<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = user::get();
        return view('Admin.User.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.User.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:8',
            'roles'=>'required',
            'phone_number'=>'required',
            'organization'=>'required',
        ]);

        $data = $request->all();
        $data['password'] = Hash::make($request['password']);

        $user = new User();
        $user->fill($data);
        if($user->save()){

            $clientrole = Role::where('id', $request['roles'])->first();
            $user->attachRole($clientrole);


            Session::flash('message','<div class="alert alert-success">User Created Successfully.!! </div>');
            return redirect('burrard-admin/user');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::findorFail($id);
        return view('Admin.User.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'first_name'=>'required',
            'last_name'=>'required',
            'status'=>'required',
            'email'=>'required|email|unique:users,email,'.$id,
            'roles'=>'required',
            'phone_number'=>'required',
            'organization'=>'required',
        ]);

        $role = $request['roles'];
        $data = $request->all();
        $data = $request->except('_token', '_method','roles');

        $update = User::where('id',$id)->update($data);
        if($update){
            $userRole = DB::table('role_user')->where('user_id',$id)->update(['role_id'=>$role]);

            Session::flash('message','<div class="alert alert-success">User Updated Successfully.!! </div>');
            return redirect('burrard-admin/user');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = User::where('id',$id)->delete();

        if($delete){
            Session::flash('message', '<div class="alert alert-danger"><strong>Alert!</strong> User Deleted successfully. </div>');

            return redirect('burrard-admin/user');
        }

    }

    /*
        Change user Status
    */
    public function statusChange($id){
        $users  = User::findorFail($id);

        if($users['status'] == 'active'){
            $newStatus = 'inactive';
        } else {
            $newStatus = 'active';
        }

        $update = User::where('id',$id)->update(['status'=>$newStatus]);

        if($update){
            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> User status updated successfully. </div>');

            return redirect('burrard-admin/user');
        }
    }

}
