<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;

class OrderController extends Controller
{
    public function index(){
        $data = Order::get();
        foreach($data as $row){
            $report = [];
            $rep = explode(',',$row->report_id);
            foreach($rep as $i){
                $report[] = $this->getReport($i);
            }
            $row->report = implode(',',$report);
        }
        return view('Admin.Order.index',compact('data'));
    }

    public function getReport($id){
        $data = Report::where('id',$id)->value('name');
        return $data;
    }

    public function getReportData($id){
        $data = Report::where('id',$id)->first();
        return $data;
    }
    public function edit($id){
        $data = Order::findorFail($id);

        $rep = explode(',',$data['report_id']);
        foreach($rep as $i){
            $report[] = $this->getReportData($i);
        }

        $data['report'] = $report;

        return view("Admin.Order.edit",compact('data'));
    }

    public function update(Request $request,$id){
        $data['order_status'] = $request['order_status'];

        $update = Order::where('id',$id)->update($data);

        if($update){
            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Order status updated successfully. </div>');

            return redirect('burrard-admin/order');
        }
    }

}
