<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ContactUs;
use Illuminate\Http\Request;
use Session;

class ContactController extends Controller
{
    public function index(){
        $data = ContactUs::get();
        return view('Admin.Contact.index',compact('data'));
    }


     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $delete = ContactUs::where('id',$id)->delete();

        if($delete){
            Session::flash('message', '<div class="alert alert-danger"><strong>Alert!</strong> Contact Deleted successfully. </div>');

            return redirect('burrard-admin/contact-us');
        }

    }
}
