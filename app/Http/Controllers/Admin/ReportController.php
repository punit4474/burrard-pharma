<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use PDO;
use Session;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Report::get();
        return view('Admin.Report.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.Report.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'dosage_form'=>'required',
            'strengh'=>'required',
            'route_of_admin'=>'required',
            'atc_code'=>'required',
        ]);

        $data = $request->all();

        $report = new Report();
        $report->fill($data);
        if($report->save()){
            Session::flash('message','<div class="alert alert-success">Report Added Successfully.!! </div>');
            return redirect('burrard-admin/report');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Report::findorFail($id);
        return view('Admin.Report.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',
            'dosage_form'=>'required',
            'strengh'=>'required',
            'route_of_admin'=>'required',
            'atc_code'=>'required',
            'status'=>'required'
        ]);

        $data = $request->all();
        $data = $request->except('_token', '_method');

        $update = Report::where('id',$id)->update($data);

        if($update){
            Session::flash('message','<div class="alert alert-success">Report Updated Successfully.!! </div>');
            return redirect('burrard-admin/report');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Report::where('id',$id)->delete();

        if($delete){
            Session::flash('message', '<div class="alert alert-danger"><strong>Alert!</strong> Report Deleted successfully. </div>');

            return redirect('burrard-admin/report');
        }

    }

     /*
        Change user Status
    */
    public function statusChange($id){
        $users  = Report::findorFail($id);

        if($users['status'] == 'active'){
            $newStatus = 'inactive';
        } else {
            $newStatus = 'active';
        }

        $update = Report::where('id',$id)->update(['status'=>$newStatus]);

        if($update){
            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Report status updated successfully. </div>');

            return redirect('burrard-admin/report');
        }
    }

    public function bulk(){
        return view('Admin.Report.bulk');
    }

    public function storeBulk(Request $request){

            $data =$request->file('excel');
            if ( $xlsx = \SimpleXLSX::parse( $request->file('excel') ) ) {
                foreach($xlsx->rows() as $value=>$item){

                    if($value != 0){
                        $valueData['name'] = $item[0];
                        $valueData['dosage_form'] = $item[1];
                        $valueData['strengh'] = $item[2];
                        $valueData['route_of_admin'] = $item[3];
                        $valueData['atc_code'] = $item[4];

                        $store = new Report();
                        $store->fill($valueData);
                        $store->save();
                    }


                }
            } else {
                echo \SimpleXLSX::parseError();
            }
            Session::flash('message','<div class="alert alert-success">Report Added Successfully.!! </div>');
            return redirect('burrard-admin/report');
    }

}
