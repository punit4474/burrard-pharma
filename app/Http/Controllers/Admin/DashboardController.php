<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Report;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        $payment = Payment::sum('amount');
        $report = Report::count();
        $order = Order::count();
        $user = User::count();
        return view('Admin.dashboard',compact('payment','report','order','user'));
    }
}
