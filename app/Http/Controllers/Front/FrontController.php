<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\ContactUs;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Payment as Payments;
use App\Models\User;
use Illuminate\Support\Facades\Redirect;
use App\Models\Report;
use App\Models\SaveCard;
use App\Models\Subscription;
use Illuminate\Http\Request;
use Cartalyst\Stripe\Stripe;
use Auth;
use Mail;
use Session;
use Validator;


use function Config;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class FrontController extends Controller
{

    public function index(){
        return view('Front.index');
    }

    public function service(){
        return view('Front.service');
    }

    public function company(){
        return view('Front.company');
    }

    public function pricing(){
        return view('Front.pricing');
    }

    public function generator()
    {
        $search = 'a';
        $data = Report::where('name','LIKE',$search.'%')->where('status', 'active')->get();


        return view('Front.generator', compact('data'));
    }

    public function generatorStore(Request $request)
    {
        $data = $request->all();
        $data['report'] = explode(',',$request['report_data']);
        $report = [];
        foreach ($data['report'] as $row) {
            $report[] = $this->getReport($row);
        }
        $data['reportData'] = $report;
        $userCard = SaveCard::where('user_id',Auth::user()->id)->orderBy('id','DESC')->first();

        return view('Front.checkout', compact('data','userCard'));
    }

    public function paymentStore(Request $request)
    {

        $data = $request->all();


        if($data['payment'] == 'stripe'){
            $stripe = new \Stripe\StripeClient('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');
            // $stripe = new \Stripe\StripeClient('sk_test_51BGb01GpPRVu83dOE7jzJ1XCgueVNdsVvYcrnS6hxGnQT4KM0lglv4Gj6YpIJHU5XROSaFTfY3Afkbfuu0Jlhtes00J53frgOD');

            if(Auth::user()->customer_id == ''){
                $customer = $stripe->customers->create([
                    'name' => $request['name'],
                    'email'=>Auth::user()->email,
                    'address' => [
                        'line1' => $request['address']
                    ]
                ]);
                $update = User::where('id',Auth::user()->id)->update(['customer_id'=>$customer['id']]);
                $c_id = $customer['id'];
            } else {
                $c_id = Auth::user()->customer_id;
            }

            if(isset($request['last_card'])){
                $customer = $stripe->customers->update( $c_id , [
                    'default_source' => $request['last_card']
                ]);

            } else {
                $card = $stripe->customers->createSource(
                    $c_id,
                    ['source' => $request['stripeToken']]
                );

                if(isset($request['savecard'])){

                    if($request['savecard'] == 'on'){

                        $cardData['user_id'] = Auth::user()->id;
                        $cardData['card_id'] = $card['id'];
                        $cardData['card_brand'] = $card['brand'];
                        $cardData['exp_month'] = $card['exp_month'];
                        $cardData['exp_year'] = $card['exp_year'];
                        $cardData['fingerprint'] = $card['fingerprint'];
                        $cardData['last4'] = $card['last4'];
                        $cardData['funding'] = $card['funding'];

                        $saveCard = new SaveCard();
                        $saveCard->fill($cardData);
                        $saveCard->save();
                    }
                }

            }


            $charge = $stripe->charges->create([
                'amount' => $request['amount']*100,
                'currency' => 'USD',
                'customer' =>$c_id,
                'description' => 'Generate Grug Report Request',
            ]);
            $charge_id  = $charge['id'];
            $payment_method = 'stripe';
        } else {
            $charge_id = '';
            $payment_method = 'paypal';
        }

        $orderData['order_id'] = 'Order-'.uniqid();
        $orderData['name'] = $request['name'];
        $orderData['email'] = $request['email'];
        $orderData['report_id'] = $request['report'];
        $orderData['billing_address'] = $request['address'];
        $orderData['city'] = $request['city'];
        $orderData['country'] = $request['country'];
        $orderData['postalcode'] = $request['postalcode'];
        $orderData['phone_number'] = $request['phone_number'];
        $orderData['payment_amount'] = $request['amount'];
        $orderData['user_id'] = Auth::user()->id;
        $orderData['order_date'] = \Carbon\Carbon::now()->format("Y-m-d");
        $orderData['order_status'] = 'processing';

        $order = new Order();
        $order->fill($orderData);
        if($order->save()){

            $paymentData['order_id'] = $orderData['order_id'];
            $paymentData['payment_id'] = $charge_id;
            $paymentData['amount'] =$request['amount'];
            $paymentData['payment_method'] =$payment_method;
            $paymentData['payment_date'] = \Carbon\Carbon::now()->format("Y-m-d");
            $paymentData['status'] = 'success';
            $payments = new Payments();
            $payments->fill($paymentData);
            $payments->save();

            $getReportData = Report::whereIn('id',explode(',',$request['report']))->get();

            $updateUserAddress = User::where('id',Auth::user()->id)->update(['billing_address'=>$order['billing_address'],'city'=>$order['city'],'country'=>$order['country'],'postalcode'=>$order['postalcode'],'phone_number'=>$order['phone_number']]);

            if($data['payment'] == 'paypal'){

                foreach($getReportData as $key =>$item) {
                    $pdata[$key] = new Item();
                    $pdata[$key]->setName($item->name)/** item name **/
                    ->setCurrency('USD')
                        ->setQuantity(1)
                        ->setPrice('5000');
                }

                $this->_api_context = new ApiContext(new OAuthTokenCredential(
                    'AVEa0VAKKumO5TG018XnNYBAb4OJEecQlWZwQEUix2TPBcpTwEAmnv-wj1RAeTpooS484rlEk-_YhMIZ',
                    'ELuOuJdRa3WaSdhXmOB6ZE4SuORC6XRSLq1iEi0RTlOIXwH5VrA2hQvJYAR58AtXyKjHuXsGsupDNAlB'));


                 $this->_api_context->setConfig(
                    array(
                       'log.LogEnabled' => true,
                       'log.FileName' => 'PayPal.log',
                       'log.LogLevel' => 'DEBUG',
                      'mode' => 'live'
                    )
              );

                 $item_list = new ItemList();
                 $item_list->setItems($pdata);

                 $payer = new Payer();
                 $payer->setPaymentMethod('paypal');

                 $amount = new Amount();
                 $amount->setTotal($data['amount']);
//                 $amount->setTotal(2);
                 $amount->setCurrency('USD');

                 $transaction = new Transaction();
                 $transaction->setAmount($amount)
                     ->setItemList($item_list)
                     ->setDescription('Item Description');

                 $redirectUrls = new RedirectUrls();
                 $redirectUrls->setReturnUrl(route('payment.success'))
                     ->setCancelUrl(route('payment.cancel'));

                 $payment = new Payment();
                 $payment->setIntent('sale')
                     ->setPayer($payer)
                     ->setTransactions(array($transaction))
                     ->setRedirectUrls($redirectUrls);


                 try {
                     $title = 'New Report Order Received';
                     $title1 = 'Your order has been received successfully';
                     $data_mail = ['title' => $title,'title1' => $title1, 'email' => $request['email'], 'name' => $request['name'], 'report_data' => $getReportData];

//                     $payment->create($apiContext);
                     $payment->create($this->_api_context);
                     Session::put('paypal_payment_id', $payment->getId());
                     Session::put('orderNumber', $order->order_id);
                     Session::put('datamail', $data_mail);
                     Payments::where('id', $payments->id)->update(['payment_id' => $payment->getId()]);

                     return redirect($payment->getApprovalLink());
                 } catch (PayPalConnectionException $ex) {
                     Session::flash('Something went wrong.');
                     return redirect('checkout');
                 }

             }else{
                $title = 'New Report Order Received';
                $title1 = 'Your order has been received successfully';
                $data_mail = ['title' => $title,'title1' => $title1, 'email' => $request['email'], 'name' => $request['name'], 'report_data' => $getReportData];

                try {
                    Mail::send('email.report', $data_mail, function ($message) use ($data_mail) {
                        $message->from('info@burrardpharma.com', "Burrard Pharma")->subject($data_mail['title']);
                        $message->to('info@burrardpharma.com');
                    });
                Mail::send('email.report_user', $data_mail, function ($message) use ($data_mail) {
                    $message->from('info@burrardpharma.com', "Burrard Pharma")->subject($data_mail['title1']);
                    $message->to($data_mail['email']);
                });

                    return view('Front.success');
                } catch (\Swift_TransportException $e) {
                    \Log::debug($e);
                    return view('Front.success');
                }
             }
        }


    }

    public function getReport($id)
    {
        $data = Report::where('id', $id)->first();
        return $data;
    }

    public function storeContact(Request $request){

        $validator = Validator::make($request->all(), [
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required|email',
            'phone_number'=>'required|numeric',
            'message'=>'required',
            'g-recaptcha-response'=>'required',
        ],[
            'g-recaptcha-response.required'=>'Google Captcha is Required'
        ]);

        if($validator->fails()) {

            return redirect('company#contact-form')
                ->withErrors($validator)
                ->withInput();

        }

        $data = $request->all();
        $data['name'] = $request['first_name'].' '.$request['last_name'];
        $contact =  new ContactUs();
        $contact->fill($data);
        if($contact->save()){

            $title = 'Contact Inquiry Recieved';
            $data_mail = ['title' => $title, 'email' => $request['email'], 'name' => $request['name'], 'phone' => $request['phone_number'], 'messageData' => $request['message']];

            try {
                Mail::send('email.contact', $data_mail, function ($message) use ($data_mail) {
                    $message->from('info@burrardpharma.com', "Burrard Pharma")->subject($data_mail['title']);
                    //$message->to('info@hoomanstudio.com');

                    $message->to('info@burrardpharma.com');
                });

            } catch (\Swift_TransportException $e) {
                \Log::debug($e);

            }

            Session::flash('message','<div class="alert alert-success">Contact Inquiry Send Successfully.!! </div>');
            return \redirect('company#contact-wrapper');
        }
    }

    public function userProfile(){
        $order = Order::where('user_id',Auth::user()->id)->orderBy('id','DESC')->get();
        foreach($order as $row){
            $row->orderDetails = Report::whereIn('id',explode(',',$row->report_id))->get();
        }
        $subscription = Subscription::where('user_id',Auth::user()->id)->orderBy('id','DESC')->get();
        return view('Front.profile',compact('order','subscription'));
    }

    public function updateProfile(Request $request){
        $data = $request->all();
        $data = $request->except('_token');

        $update = User::where('id',Auth::user()->id)->update($data);
        if($update){
            return \redirect('user-profile');
        }
    }

    public function success(Request $request)
    {
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            'ARdVKvuVxad4DjDUqGYHCvSOVkq2IMgRqH28By3DKIZT-Dyh7o8H9JM_oXQy7dX2h2B3YqDDLZsmAb4b',
                 'EF83hlQLetIyBQeg8xQyKA7VupzFK-xBAeVkezeonuCmb1OPlIe7qk45j6Qub8MtPEyekHeC7PaP2YHj'));

        $payment_id = Session::get('paypal_payment_id');
        $oredr_number = Session::get('orderNumber');
        $data_mail = Session::get('datamail');

        Session::forget('paypal_payment_id');
        Session::forget('orderNumber');
        Session::forget('datamail');

        if (empty($request->PayerID) || empty($request->token) || empty($payment_id) || empty($oredr_number)) {
            return back()->with('errors', ['Invalid request.']);
        }

        $payment = Payment::get($payment_id, $this->_api_context);

        $execution = new PaymentExecution();
        $execution->setPayerId($request->PayerID);

        $result = $payment->execute($execution, $this->_api_context);

        if ($result->getState() == 'approved') {
            Payments::where('order_id',$oredr_number)->where('payment_id',$payment_id)->update(['status'=>'success']);
        } else {
            Payments::where('order_id',$oredr_number)->where('payment_id',$payment_id)->update(['status'=>'failed']);
        }

        try {
            Mail::send('email.report_user', $data_mail, function ($message) use ($data_mail) {
                $message->from('info@burrardpharma.com', "Burrard Pharma")->subject($data_mail['title1']);
                $message->to($data_mail['email']);
            });

            return redirect('payment-sucess')->with('success', ['Payment Successful.']);
        } catch (\Swift_TransportException $e) {
            \Log::debug($e);
            return redirect('payment-sucess')->with('success', ['Payment Successful.']);
        }



    }

    public function cancel()
    {
        $payment_id = Session::get('paypal_payment_id');
        $oredr_number = Session::get('orderNumber');
        $data_mail = Session::get('datamail');

        Session::forget('paypal_payment_id');
        Session::forget('orderNumber');
        Session::forget('datamail');

        Payments::where('order_id',$oredr_number)->where('payment_id',$payment_id)->update(['status'=>'failed']);

//        try {
//            Mail::send('email.report_user', $data_mail, function ($message) use ($data_mail) {
//                $message->from('info@burrardpharma.com', "Burrard Pharma")->subject($data_mail['title1']);
//                $message->to($data_mail['email']);
//            });
//
//            return redirect('payment-failed')->with('error', ['Payment Failed.']);
//        } catch (\Swift_TransportException $e) {
//            \Log::debug($e);
//            return redirect('payment-failed')->with('error', ['Payment Failed.']);
//        }

        return redirect('payment-failed')->with('error', ['Payment Failed.']);

    }

    public function subscription(Request $request){

        $stripe = new Stripe('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');

        if(Auth::user()->customer_id == ''){
            $customer = $stripe->customers()->create([
                'name' => $request['name'],
                'email'=>Auth::user()->email,
            ]);
            $update = User::where('id',Auth::user()->id)->update(['customer_id'=>$customer['id']]);
            $c_id = $customer['id'];
        } else {
            $c_id = Auth::user()->customer_id;
        }

        $card = $stripe->cards()->create( $c_id, $request['stripeToken']);

        $subscription = $stripe->subscriptions()->create($c_id, [
            'plan' => 'yearly',
        ]);


        $subscribeData['user_id'] = Auth::user()->id;
        $subscribeData['name'] = $request['name'];
        $subscribeData['email'] = $request['email'];
        $subscribeData['subscription_id'] = $subscription['id'];
        $subscribeData['start_date'] =  date("Y-m-d", $subscription['current_period_start']);
        $subscribeData['end_date'] =  date("Y-m-d", $subscription['current_period_end']);
        $subscribeData['status'] = 'success';
        $subscribeData['amount'] = 250;
        $subscribeData['billing_address'] = $request['address'];
        $subscribeData['city'] = $request['city'];
        $subscribeData['country'] = $request['country'];
        $subscribeData['postalcode'] = $request['postalcode'];
        $subscribeData['phone_number'] = $request['phone_number'];

        $subscribe = new Subscription();
        $subscribe->fill($subscribeData);
        if($subscribe->save()){

            $title = 'Thank you for the subscribe Our service';
            $data_mail = ['title' => $title, 'email' => $request['email'], 'name' => $request['name'], 'subscription_id' => $subscription['id']];

            try {
                Mail::send('email.subscribe', $data_mail, function ($message) use ($data_mail) {
                    $message->from('info@burrardpharma.com', "Burrard Pharma")->subject($data_mail['title']);
                    $message->to($data_mail['email']);
                });


            } catch (\Swift_TransportException $e) {
                \Log::debug($e);

            }
            Session::flash('message','<div class="alert alert-success">Subscription subscribe Successfully.!! </div>');
            return redirect('user-profile');
        }

    }

    public function generatorAjax(Request $request)
    {

        $data = Report::where('name','LIKE',$request->page.'%')->where('status', 'active')->get();

        return $data;
    }

    /**
     * Blog Functions
     */

    public function blog(){
        $data = Blog::orderBy('id','DESC')->get();
        return view('Front.blog',compact('data'));
    }

    public function blogDetails($slug){

        $data = Blog::where('slug',$slug)->first();

        return view('Front.blog_details',compact('data'));
    }

    /**
     * Notification
     */
    public function notification(Request $request){
        $this->validate($request,[
           'email'=>"required"
        ]);

        $data = $request->all();

        $notification = new Notification();
        $notification->fill($data);
        if($notification->save()){

            $title = 'One Newsletter Subscribe';
            $data_mail = ['title' => $title, 'email' => $request['email']];

            try {
                Mail::send('email.notification', $data_mail, function ($message) use ($data_mail) {
                    $message->from('info@burrardpharma.com', "Burrard Pharma")->subject($data_mail['title']);
                    $message->to('info@burrardpharma.com');
                });


            } catch (\Swift_TransportException $e) {
                \Log::debug($e);

            }

            $va = [
                'status'=>'1',
                'data'=>'okay'
            ];
            return $va;
        }
    }

//    public function mediumBlog(){
//        $stripe = new Stripe('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');
//        $plan = $stripe->plans()->create([
//            'id'                   => 'test-plan',
//            'name'                 => 'Testing Plan',
//            'amount'               => 1,
//            'currency'             => 'USD',
//            'interval'             => 'year',
//            'statement_descriptor' => 'Testing Plan',
//        ]);
//
//        dd($plan);
//    }

}
